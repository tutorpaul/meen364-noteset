# MEEN 364 Noteset

There is already too much to read here, so I'll keep this brief despite superfluous sentences like this one.
This repo was from the time before I knew about repos, as such it is a hot mess with a bunch of unneeded files.
I noticed an omission from the text and wanted to correct that, but now all my projects are version controlled and CI pipeline'd and all that jazz, so here we are.
I'm hoping to get the CI pipeline to typeset the releases so that I don't have to maintain a LaTeX installation for the once a year I might typeset this thing.
If everything goes well I might start migrating figures to TiKz and making them look all pretty.

## Contributing

If you see something wrong in the text or think that something is missing then I welcome you to create an issue on this repo and we can work to make the change.
