\section{Laplace Transform Property Proofs}\label{sec:laplace_props_proof}
Several properties of the Laplace Transform given are proved below.

Before we start let's remember the definition of the Laplace Transform:
$$F (s) = {\cal L} \left\{ f(t)\right\} = \int _0 ^\infty f(t)e^{-st}\, dt\text{.}$$

So anytime we see the right hand side of that equation we are free to replace it with the left hand side or vice-versa as needed.

Likewise if we see some different element on one side of the equation then a corresponding element on the other side should change to match, for example:
$$G (\beta) = {\cal L} \left\{ g(p)\right\} = \int _0 ^\infty g(p)e^{-\beta p}\, dp\text{,}$$
is exactly equivalent to the definition of the Laplace Transform with some symbols changed.

\subsection{Linearity}

We will attempt to prove the following, all that should be required is basic algebraic manipulation and basic understanding of the properties of the integral:
$${\cal L} \left\{ \alpha f(t) + \beta g(t) \right\} = \alpha F(s)+ \beta G(s)$$

\begin{proof}
\begin{eqnarray*}
{\cal L} \left\{ \alpha f(t) + \beta g(t) \right\} &=& \int _0 ^\infty \left(\alpha f(t) + \beta g(t)\right)e^{-st}\, dt \\
&=&\int _0 ^\infty \alpha f(t)e^{-st} + \beta g(t)e^{-st}\, dt \\
&=&\int _0 ^\infty \alpha f(t)e^{-st}\, dt + \int _0 ^\infty\beta g(t)e^{-st}\, dt \\
&=&\alpha\int _0 ^\infty  f(t)e^{-st}\, dt + \beta\int _0 ^\infty g(t)e^{-st}\, dt \\
&=&\alpha{\cal L}\left\{f(t)\right\}+\beta{\cal L}\left\{g(t)\right\} \\
&=&\alpha F(s)+\beta G(s)
\end{eqnarray*}
\end{proof}

\subsection{Frequency Shift}

Now we will try to prove the following.
$${\cal L} \left\{ f(t)e^{-at}\right\} = F(s+a)$$
We will need to remember how to manipulate exponents.

\begin{proof}
\begin{eqnarray*}
{\cal L} \left\{ f(t)e^{-at}\right\} &=& \int _0 ^\infty f(t)e^{-at}e^{-st}\, dt \\
&=&\int _0 ^\infty f(t)e^{-st-at}\, dt \\
&=&\int _0 ^\infty f(t)e^{-(s+a)t}\, dt \\
&=& F(s+a)
\end{eqnarray*}
\end{proof}

\subsection{Time Shift}

$${\cal L} \left\{ f(t-a) \right\} = e^{-as}F(s)$$

To prove this we will use the concept of $u$-substitution.

\begin{proof}
$${\cal L} \left\{ f(t-a) \right\} = \int _0 ^\infty f(t-a)e^{-st}\, dt $$
Take $u=t-a$, making $t=u+a$ and $du=dt$.  Making this substitution gives the following.
\begin{eqnarray*}
\int _0 ^\infty f(t-a)e^{-st}\, dt &=& \int _0 ^\infty f(u)e^{-s(u+a)}\, du \\
&=&\int _0 ^\infty f(u)e^{-su-as}\, du \\
&=&\int _0 ^\infty f(u)e^{-su}e^{-as}\, du
\end{eqnarray*}

Since $e^{-as}$ is not a function of $u$ it can be removed from the integral as a constant.
\begin{eqnarray*}
\int _0 ^\infty f(u)e^{-su}e^{-as}\, du&=&e^{-as}\int _0 ^\infty f(u)e^{-su}\, du \\
&=&e^{-as}F(s) \\
\end{eqnarray*}
\end{proof}

\subsection{Frequency Differentiation}

$${\cal L}\left\{tf(t)\right\}=\frac{-d}{ds}F(s)$$

\begin{proof}
\begin{eqnarray*}
{\cal L}\left\{tf(t)\right\} &=& \int _0 ^\infty tf(t)e^{-st}\, dt \\
&=& \int _0 ^\infty f(t)te^{-st}\, dt 
\end{eqnarray*}
Now consider the term $e^{-st}$, when it is differentiated with respect to $s$ we see something useful.
$$\frac{d e^{-st}}{ds} = -te^{-st}$$
Re-arranging this gives us a useful substitution.
$$te^{-st}=\frac{-d e^{-st}}{ds}$$
Making this substitution give us the following.
$$\int _0 ^\infty f(t)te^{-st}\, dt = \int _0 ^\infty f(t)\frac{-d e^{-st}}{ds}\, dt $$
Since an $s$-derivative is independent of $t$ the $\frac{-d}{ds}$ can be removed from the integral.
\begin{eqnarray*}
\int _0 ^\infty f(t)\frac{-d e^{-st}}{ds}\, dt &=& \frac{-d }{ds}\int _0 ^\infty f(t)e^{-st}\, dt \\
&=&\frac{-d}{ds}F(s)
\end{eqnarray*}
\end{proof}

\subsection{Differentiation}
$${\cal L} \left\{ \frac{df(t)}{dt}  \right\} =sF(s)-f(0)$$

To prove this we will need to use integration by parts.

\begin{proof}
\begin{eqnarray*}
{\cal L} \left\{ \frac{df(t)}{dt}  \right\} &=& \int _0 ^\infty \frac{df(t)}{dt}e^{-st}\, dt \\
&=&\int _0 ^\infty  e^{-st}\, df(t)
\end{eqnarray*}
Now we can take $u=e^{-st}$ and $dv=df(t)$, making $du=-se^{-st}\,dt$ and $v=f(t)$.
\begin{eqnarray*}
\int _0 ^\infty  e^{-st}\, df(t) &=& \int _0 ^\infty  u\, dv \\
&=&uv|_0 ^\infty -\int _0 ^\infty v\,du \\
&=&f(t)e^{-st}|_0 ^\infty - \int _0 ^\infty f(t) (-s)e^{-st}\,dt \\
&=&f(t)e^{-st}|_0 ^\infty - (-s)\int _0 ^\infty f(t) e^{-st}\,dt \\
&=&\left(f(\infty)e^{-\infty} - f(0)e^{0}\right)+s\int _0 ^\infty f(t) e^{-st}\,dt
\end{eqnarray*}
Remember: $e^{-\infty}=0$ and $e^0=1$.
\begin{eqnarray*}
\left(f(\infty)e^{-\infty} - f(0)e^{0}\right)+s\int _0 ^\infty f(t) e^{-st}\,dt &=& s\int _0 ^\infty f(t) e^{-st}\,dt -f(0) \\
&=&sF(s) - f(0)
\end{eqnarray*}
\end{proof}

\subsection{Integration}

$${\cal L} \left\{ \int _0 ^t f(\tau)\,d\tau \right\} =\frac{F(s)}{s}$$

We will need to use integration by parts again to prove this property.  It will also be useful to remember that the derivative is the inverse operation of the integral.

\begin{proof}
$${\cal L} \left\{ \int _0 ^t f(\tau)\,d\tau \right\} = \int _0 ^\infty \int _0 ^t f(\tau)\,d\tau e^{-st}\, dt$$
Taking $u=\int _0 ^t f(\tau)\,d\tau$ and $dv=e^{-st}\,dt$, makes $du=f(t)\,dt$ and $v=\frac{-1}{s}e^{-st}$.
\begin{eqnarray*}
\int _0 ^\infty \int _0 ^t f(\tau)\,d\tau e^{-st}\, dt &=& \int _0 ^\infty u\, dv \\
&=&uv|_0 ^\infty -\int _0 ^\infty v\,du \\
&=&\left.\left(\int _0 ^t f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{-st}\right)\right|_0 ^\infty -\int _0 ^\infty \frac{-1}{s}e^{-st}f(t)\,dt
\end{eqnarray*}

Expanding this gives the following function.
$$\left[\left(\int _0 ^\infty f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{-\infty}\right)-\left(\int _0 ^0 f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{0}\right)\right] -\frac{-1}{s}\int _0 ^\infty e^{-st}f(t)\,dt $$
If we remember that the value of any definite integral whose upper and lower bounds are the same is, by definition, zero then we will see that:
$$\int _0 ^0 f(\tau)\,d\tau=0\text{,}$$
further we should remember that $e^{-\infty}=0$.  So the above function is simplified down to the following.
$$\frac{1}{s}\int _0 ^\infty e^{-st}f(t)\,dt $$
Which is equivalent to:
$$\frac{F(s)}{s}$$

\end{proof}