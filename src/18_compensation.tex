\section{Motivation}
Now that we know how to determine the stability of a system by looking at its Bode plot we can think about how to modify the system to improve its stability or \newterm{robustness}.  Compensators are simple controllers that change a system's Bode plot in a specific range of frequencies in order to improve the steady-state error, phase margin, or both.  There are three types of compensators we will study in this topic: \newterms{lead}{lead compensator}, \newterms{lag}{lag compensator}, and \newterms{lead-lag compensators}{lead-lag compensator}.

\section{Lead Compensation}
Adding a lead compensator to a system is an easy way to improve (increase) the phase margin (which is roughly equivalent to adding damping).  Essentially you are adding derivative control without the negative effect of noise amplification.  This is done by adding a zero to your system at a lower frequency than an added pole.  \eqn{lead_comp} gives the general form of a lead compensator, and in general the Bode plot looks like the one given in \fig{lead_comp}.  Adding a lead compensator will also increase your system's bandwidth, but it can degrade the system's steady state performance.

\begin{equation}
D(s)=K\frac{Ts+1}{\alpha Ts+1} \text{\quad where $\alpha < 1$} 
\label{eqn:lead_comp}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{18_compensation_figs/lead_comp.pdf}
\caption{Typical Lead Compensator}
\label{fig:lead_comp}
\end{center}
\end{figure}

\subsection{Designing a Lead Compensator}
Complete the following steps, given the open loop transfer function of the system to be compensated, $G(j\omega )$; a required phase margin, $\phi _\text{req}$; and a required steady state error.  Note that I'm using different nomenclature than that found in the class notes, but the procedure is the same.  Quite often the uncompensated system will have a variable gain, $K_1$, already present (as part of $G(j\omega)$), and for the lead design procedure we will incorporate the two gains together as $K_t=KK_1$; after removing $K_1$, $G(j\omega)$ can be called $G^*(j\omega)$.
\begin{enumerate}
\item Using the steady state error requirement, determine the needed gain, $K_t$.
\item Determine the phase margin of the system with gain $K_t$, this phase margin will herein be known as $\phi _\text{uncomp}$.
\item Find the needed phase increase, $\phi _\text{comp}$, to get the required phase margin, $\phi _\text{req}$, using \eqn{phase_req} where $\varepsilon$ is a safety margin ($5^\circ$ to $12^\circ$).

\begin{equation}
\phi _\text{comp}=\phi _\text{req}-\phi _\text{uncomp}+\varepsilon
\label{eqn:phase_req}
\end{equation}

\item Using \eqn{find_alpha}, find $\alpha$.

\begin{equation}
\alpha = \frac{1-\sin\left(\phi _\text{comp}\right)}{1+\sin\left(\phi _\text{comp}\right)}
\label{eqn:find_alpha}
\end{equation}

\item Find the compensated system's \index{crossover frequency}crossover frequency, $\omega _\text{comp}$, by applying \eqn{find_crossover}.

\begin{equation}
20\log|K_t G^*(j\omega _\text{comp})|=-10\log\left(\frac{1}{\alpha}\right)
\label{eqn:find_crossover}
\end{equation}

\item Compute the compensator's break frequencies, $\omega _1$ and $\omega _2$ (and, by proxy, $T$) using Equations~\ref{eqn:break_freq_1}~and~\ref{eqn:break_freq_2}.

\begin{equation}
\omega _1 = \frac{1}{T} =\omega _\text{comp}\sqrt\alpha
\label{eqn:break_freq_1}
\end{equation}

\begin{equation}
\omega _2 = \frac{1}{\alpha T} =\frac{\omega _\text{comp}}{\sqrt\alpha}
\label{eqn:break_freq_2}
\end{equation}

\item Sketch the compensated system's Bode plot to ensure the design requirements are met.  If the phase margin isn't high enough, repeat the previous steps with a larger $\varepsilon$.
\end{enumerate}

The maximum increase in phase margin that a lead compensator can provide is $60^\circ$, if you need more than that try using the compensator given in \eqn{double_lead_comp}.  If using the double lead compensator then you should halve $\phi _\text{comp}$ in the design procedure.

\begin{equation}
D(s)=K\left(\frac{Ts+1}{\alpha Ts+1}\right)^2 \text{\quad where $\alpha < 1$} 
\label{eqn:double_lead_comp}
\end{equation}

\section{Lag Compensation}
A lag compensator is similar to a lead compensator, the primary difference being that the pole occurs at a lower frequency than the zero.  This arrangement roughly approximates an integral controller, giving improved steady state performance without a sharp negative effect on the phase margin.  The generic transfer function of a lag compensator is given as \eqn{lag_comp}, and its Bode plot is given as \fig{lag_comp}.

\begin{equation}
D(s)=\alpha\frac{Ts+1}{\alpha Ts+1} \text{\quad where $\alpha > 1$} 
\label{eqn:lag_comp}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{18_compensation_figs/lag_comp.pdf}
\caption{Typical Lag Compensator}
\label{fig:lag_comp}
\end{center}
\end{figure}

The low frequency gain of the system will be increased by $20\log(\alpha)$, which will yield a proportional improvement in steady state error.

\subsection{Design of a Lag Compensator}
Complete the following steps given a system's open loop transfer function, a desired steady state error (the appropriate desired error constant: $K_p$, $K_v$, or $K_a$; which will be known as $K_\text{des}$), and a minimum allowable phase margin.  The open loop transfer function will typically have a built in variable gain, $K_1$ (just as before).
\begin{enumerate}
\item Pick the system gain, $K_1$, so the minimum phase margin (plus some factor of safety, $\varepsilon$) is met.
\item Find the system's crossover frequency, $\omega _c$ with the gain $K_1$.
\item From the adjusted system ($K_1$ included), find the system's steady state error constant ($K_p$, $K_v$, or $K_a$, depending on system type will be called $K_\text{act}$).
\item Choose a value for $\alpha$ such that $K_\text{des}=\alpha K_\text{act}$.
\item Compute $T$ using \eqn{lag_break_freq}; where $d$ is some value between 2 and 10.  Usually  you will set $d=10$, which puts the zero's break frequency a decade before the system's crossover frequency.

\begin{equation}
\frac{1}{T}=\frac{\omega_c}{d}
\label{eqn:lag_break_freq}
\end{equation}
\item Sketch the compensated system and find the error constant to ensure the design specifications were met.
\end{enumerate}

\section{Lead-Lag Compensation}
The most robust compensator is the lead-lag compensator, which is an approximation of a PID controller.  This should be used when you need to simultaneously meet requirements in steady state error, bandwidth, and phase margin.  A lead-lag compensator is just a lead and lag compensator placed in series with one another, but still designed separately: lag first, lead second.
