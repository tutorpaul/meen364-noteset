set terminal fig
set output 'disc.fig'
set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Sample'
set xrange [1:46]
set nokey
set sample 46
set xtics (1,6,11,16,21,26,31,36,41,46)
f(x) = sin((x-1)/45*3.14)+3*sin(5*(x-1)/45*3.14)-4*sin(10*(x-1)/45*3.14) 
plot f(x) w points pt 2 
