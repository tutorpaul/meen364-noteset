set terminal fig
set output 'cont.fig'
set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Time'
set xrange [0:3.14]
set nokey
set samples 200
f(x) = sin(x)+3*sin(5*x)-4*sin(10*x) 
plot f(x) 