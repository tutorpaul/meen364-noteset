set terminal fig

set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Time (sec)'


set output 'pid_ex_P.fig'
plot "pid_ex.dat" u 1:2 title 'P=1' w lines 1,\
     "pid_ex.dat" u 1:3 title 'P=2' w lines 3,\
     1 notitle w lines 2


set output 'pid_ex_I.fig'
plot "pid_ex.dat" u 1:3 title 'I=0' w lines 1,\
     "pid_ex.dat" u 1:4 title 'I=5.5' w lines 3,\
     1 notitle w lines 2


set output 'pid_ex_D.fig'
plot "pid_ex.dat" u 1:4 title 'D=0' w lines 1,\
     "pid_ex.dat" u 1:5 title 'D=0.15' w lines 3,\
     1 notitle w lines 2
     
set output 'pid_ex_comb.fig'
plot "pid_ex.dat" u 1:2 title 'Uncontrolled' w lines 4,\
     "pid_ex.dat" u 1:3 title 'P Control' w lines 3,\
     "pid_ex.dat" u 1:4 title 'PI Control' w lines 2,\
     "pid_ex.dat" u 1:5 title 'PID Control' w lines 1,\
     1 notitle w lines 5