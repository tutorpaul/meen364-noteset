% global n
Tfin=4;

kp=1;
ki=0;
kd=0;

G=tf(25,[1 3 25]);

C=tf([kd kp ki],[1 0]);

% figure(1);
% rlocus(D*G);

T=feedback(C*G,1);
% n=n+1;
% figure(n)
[o,t]=step(T,Tfin);

step(T,t);

kp=2;
P=tf([kd kp ki],[1 0]);
[p,t]=step(feedback(P*G,1),t);

ki=5.5;
PI=tf([kd kp ki],[1 0]);
[pI,t]=step(feedback(PI*G,1),t);

kd=0.15;
PID=tf([kd kp ki],[1 0]);
[pID,t]=step(feedback(PID*G,1),t);


fid= fopen('pid_ex.dat','wt');

fprintf(fid,'#  frequency  magnitude phase \n');
for i=1:length(t)
    fprintf(fid,'%12.4E  %12.4E  %12.4E  %12.4E  %12.4E  ', ...
        t(i), o(i), p(i), pI(i), pID(i));
    
    fprintf(fid,'\n');
end
fclose(fid);
