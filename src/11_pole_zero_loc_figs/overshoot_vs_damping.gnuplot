set terminal fig
set output 'overshoot_vs_damping.fig'
set size 1,1
set lmargin 8
set rmargin 1
set xlabel "Damping Ratio"
set ylabel "Overshoot"
set xrange [0:1]
set nokey
set xtics 0.05
set ytics 0.05
set grid
set samples 200
f(x) = exp(-3.14*x / sqrt(1-x**2))
plot f(x) 