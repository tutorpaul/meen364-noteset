set terminal fig fontsize 8;
#set output 'pole_locations_polar.fig';

set xlabel 'Time' offset 0,1.5
set noylabel
set autoscale y
set xrange [0:3]
set size .25,.3
set border ls 1
set xtics 3
set ytics 1
set samples 50
set nokey
set xzeroaxis lt 4

set output 'envelope1.fig'
plot exp(-0.8*x) lt 2,\
    -exp(-0.8*x) lt 2 

set output 'envelope2.fig'
plot exp(-0.4*x) lt 2,\
    -exp(-0.4*x) lt 2
    
set ytics 4
set output 'envelope4.fig'
plot exp(0.4*x) lt 2,\
    -exp(0.4*x) lt 2

set ytics 12
set output 'envelope5.fig'
plot exp(0.8*x) lt 2,\
    -exp(0.8*x) lt 2

set yrange [-1.2:1.2]
set ytics 1
set output 'envelope3.fig'
plot exp(0*x) lt 2,\
    -exp(-0*x) lt 2


set xrange [0:3];
set samples 200;
set ytics 1;
set output 'curve1.fig'
plot sin(2*x) lt 1

set output 'curve2.fig'
plot sin(4*x) lt 1

set output 'curve3.fig'
plot sin(6*x) lt 1

set output 'curve4.fig'
plot sin(8*x) lt 1
