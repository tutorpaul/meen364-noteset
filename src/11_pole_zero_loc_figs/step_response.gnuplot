set terminal fig

set size 1,1
set lmargin 5
set rmargin 1
#set ylabel 'Magnitude(dB)'
set ylabel 'Amplitude'
set xlabel 'Time'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set yrange [0:1.6]
set xrange [0:3.25]
#set autoscale y
#set xzeroaxis
#set yzeroaxis
#set noxlabel
#set noylabel
set samples 250

k(x) = 1- exp(-1.5*x)*cos(sqrt(91)/2*x)-(3/sqrt(91))*exp(-1.5*x)*sin(sqrt(91)/2*x)
set output 'step_response.fig'
plot k(x) w lines 1,\
     1.0 w lines 2,\
     0.1 w lines 3,\
     0.9 w lines 3,\
     1.05 w lines 1,\
     0.95 w lines 1
  
  

set key right bottom
f(x) = exp(-3/2*x)
h(x) = exp(3/2*x)
wd = 5*sqrt(1-(.3**2))
g(x) = (sin(wd*x))
set samples 250
set yrange [-1.2:1.2]
set xzeroaxis lt 4
set output 'impulse_response.fig'
plot f(x) w lines 2 ,\
    -f(x) w lines 2,\
     f(x)*g(x) w lines 1,\
     g(x) w lines 5
     
set autoscale y
set output 'impulse_response_negdamp.fig'
plot h(x) w lines 2,\
    -h(x) w lines 2,\
     h(x)*g(x) w lines 1,\
     g(x) w lines 5
    


