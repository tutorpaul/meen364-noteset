set terminal fig

set size 1.2,1.2
set lmargin 5
set rmargin 1
#set ylabel 'Magnitude(dB)'
set ylabel 'Imaginary Axis'
set xlabel 'Real Axis'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set yrange [-4:15]
set xrange [-3:3]
set xzeroaxis
set yzeroaxis

set output 'p-z_loc.fig'
plot "pole_locations.dat" using 1:2 w points 2

set noxlabel
set noylabel
set autoscale y
set xrange [0:3]
set size .25,.3
unset yzeroaxis
unset xtics
unset ytics

set output 'response5.fig'
plot "responses.dat" using 1:6 w lines 1

set output 'response6.fig'
plot "responses.dat" using 1:7 w lines 1

set output 'response7.fig'
plot "responses.dat" using 1:8 w lines 1

set output 'response8.fig'
plot "responses.dat" using 1:9 w lines 1

set yrange [-1:2]

set output 'response2.fig'
plot "responses.dat" using 1:3 w lines 1

set output 'response3.fig'
plot "responses.dat" using 1:4 w lines 1

set yrange [-0.35:0.35]

set output 'response4.fig'
plot "responses.dat" using 1:5 w lines 1

set yrange [-1:10]

set output 'response1.fig'
plot "responses.dat" using 1:2 w lines 1

set output 'response9.fig'
plot "responses.dat" using 1:10 w lines 1
