K_0=tf(3,1);

a=0.25;
T=5;

z=tf([T 1],1);
p=tf(1,[a*T 1]);

lead=K_0*z*p;

[mag_lead,phase_lead,w]=bode(lead);

figure(1)
bode(lead);


a=1.25;
T=5;

z=tf([T 1],1);
p=tf(1,[a*T 1]);

lag=a*z*p;

[mag_lag,phase_lag]=bode(lag,w);

figure(2)
bode(lag,w);

fid= fopen('lead_lag.dat','wt');

fprintf(fid,'#    freq    gain(lead)   phase(lead)    gain(lag)    phase(lag) \n');
for i=1:length(w)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E\n',...
        w(i),20*log10(mag_lead(i)),phase_lead(i),20*log10(mag_lag(i)),phase_lag(i));
end
fclose(fid);