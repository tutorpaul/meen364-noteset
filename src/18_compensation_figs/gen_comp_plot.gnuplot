set terminal fig
set output 'lag_phase.fig'
set size 1,0.5
set lmargin 6
set rmargin 1
#set ylabel 'Magnitude (dB)'
set ylabel 'Phase (degree)'
set xlabel 'Frequency (rad/sec)'
set logscale x
set format x "10^%L"
#set format x ""
set nokey
#set yrange [-0.2:2]
#set yzeroaxis

plot "lead_lag.dat" u 1:5 w lines 1
