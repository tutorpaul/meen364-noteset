% elements
\section{Electrical Elements}
All circuits that you will encounter in this class will be composed of the following elements: \newterms{resistors}{resistor}, \newterms{capacitors}{capacitor}, \newterms{inductors}{inductor}, \newterms{transformers}{transformer}, \newterms{sources}{source}, and \newterms{electric motors}{electric motor}.  Each of these is treated much like an element in a mechanical system, where the voltage across the element is similar to the velocity difference in a translational system, likewise the current is similar to the force.  Each element will be addressed in the following sub-sections.

% > resistors
\subsection{Resistors}
Resistors in an electrical system are similar in nature to a damper in a translational system.  They are modeled using \eqn{resistor}.
\begin{equation}
I_R=\frac{1}{R}V_R\quad\Rightarrow\quad V_R=RI_R
\label{eqn:resistor}
\end{equation}
$I_R$ represents the current (force), $V_R$ is the voltage (velocity), and $R$ is the resistance.  The resistor tends to draw energy from the system, thus it is non-conservative.

% > capacitors
\subsection{Capacitors}
A capacitor's sister element is a mass, as such it is modeled in much the same way; see \eqn{capacitor}.
\begin{equation}
I_C=C\frac{dV_C}{dt}
\label{eqn:capacitor}
\end{equation}
Once again, $I_C$ is the current (force), $V_C$ is the voltage (velocity), and $C$ is the capacitance.  You should remember (from \course{Principles of Electrical Engineering}) that capacitors are used to store electric energy in the form of voltage, while a mass stores energy in the form of velocity (kinetic energy).

% > inductors
\subsection{Inductors}
The inductor acts much like a spring, in that it stores energy in the form of current (springs store force).  An inductor is modeled using \eqn{inductor}.
\begin{equation}
V_L=L\frac{dI_L}{dt} \quad\Rightarrow\quad \frac{1}{L}\int V_L\,dt=I_L
\label{eqn:inductor}
\end{equation}
The first equation (in \eqn{inductor}) is what you will use to model inductors, while the second is just to illustrate how it is similar to Hooke's law (for springs).\footnote{Remember that the integral of velocity is position} $L$ is referred to as inductance.

% > transformers (lol robots in disguise)
\subsection{Transformers}
Transformers allow you to couple two otherwise independent circuits, in much the same way a set of gears allow you to link two rotational systems together.  I'm not going to go into the construction of a transformer, suffice it to say that there are a couple of wire coils.  Transformers are modeled using \eqn{transformer} for relating current, and \eqn{transformer_voltage} for voltage.
\begin{equation}
I_b=\frac{n_a}{n_b}I_a
\label{eqn:transformer}
\end{equation}

\begin{equation}
V_b=\frac{n_b}{n_a}V_a
\label{eqn:transformer_voltage}
\end{equation}

$I_a$ is the current going into the transformer on one end (circuit $a$), $I_b$ is the current coming out of the other end (circuit $b$), $n_a$ is the number of turns in the coil in circuit $a$, while $n_b$ is the number of turns in the coil in circuit $b$.  The coil ratio is parallel to the ratio of the radii in a gear system.


\subsection{Sources}
The primary drivers of an electric circuit are voltage \index{sources!current} and current sources\index{sources!voltage}.  The former provides a specified voltage without any restriction on current while the latter provides a specified current regardless of the voltage.

% > electric motors
\subsection{Electric Motors}
Electric Motors---also known as \newterms{electric machines}{electric machine}---don't have a mechanical counterpart, because they are used to convert from electric energy to mechanical (typically rotational) energy.  We aren't going to go into how they are modeled yet, but it is appropriate that they be introduced here.

% Kirchoff's Laws
\section{Kirchoff's Laws}
There are two fundamental laws that make the modeling of electrical systems possible, namely \newterm{Kirchoff's voltage law} (KVL), and \newterm{Kirchoff's current law} (KCL).  The first law---Kirchoff's voltage law---states that the sum of the voltages, contributions from sources as well as losses from other elements, must equal zero (see \fig{kvl} and \eqn{kvl}).  Kirchoff's current law stipulates that the sum of the currents entering a node is equal to the sum of currents leaving a node, it is also known as conservation of current (see \fig{kcl} and \eqn{kcl}). 

\begin{figure}[ht]
\begin{center}
\includegraphics{06_modeling_elec_figs/kirchoffs_voltage.pdf}
\caption{Kirchoff's Voltage Law Representation}
\label{fig:kvl}
\end{center}
\end{figure}

\begin{equation}
V_a+V_b+V_c=0
\label{eqn:kvl}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{06_modeling_elec_figs/kirchoffs_current.pdf}
\caption{Kirchoff's Current Law Representation}
\label{fig:kcl}
\end{center}
\end{figure}

\begin{equation}
I_1+I_3=I_2+I_4
\label{eqn:kcl}
\end{equation}

\section{Sign Conventions}
When considering an electric circuit it is best to define a convention for the direction of current flow.  We can either say that current flows from high voltage to low, or low voltage to high.  It doesn't matter which assumption is used, but once a decision is made it should be adhered to.  Sometimes the problem will have a convention defined, pay careful attention when examining the given circuit to determine if a convention is given and use it.  

If we assume that current flows from low voltage to high then we would update our figure to reflect that for every normal element in the circuit, source elements behave differently though, for a source element we will reverse this assumption and say that current is flowing from high voltage to low.  The appropriate labeling is shown in \fig{example_markedup} for this assumption.

\section{Understanding States}
As we add new elements to our repertoire we need to know how to define their states, as before the energy state of our elements is of interest.  In electrical systems we no longer deal with kinetic, gravitational potential, or spring potential energy; but we do have to account for electromagnetic energy.  A capacitor stores energy in the form of an electric field, and its energy can be calculated thusly:
$$U = \frac{1}{2}CV^2\text{.}$$
So, knowing the physical constant of the capacitor (namely the capacitance), and the voltage drop across the capacitor will allow us to know the energy stored in that capacitor.  Now, we don't consider physical constants as states, only variables.  So, the state variable for a capacitor should be its \emph{voltage drop}

An inductor also stores energy, as current passes through the coiled wire it generates a magnetic field.\footnote{While a capacitor can store energy like a battery an inductor cannot.  If an inductor is removed from a circuit the magnetic field will collapse, because the current stopped.}  This phenomenon is modeled by the following:
$$U = \frac{1}{2}LI^2\text{.}$$
So, the critical variable for understanding the state of an inductor is \emph{current flow}.

Resistors do not store energy, so they do not have a state variable.

% modeling strategy
\section{Modeling Strategy}
Now that we know how each element is modeled independently, and we know what laws will be used to make a model, we are ready to look at \emph{how} we will actually model an electrical system.  Let's take the example shown in \fig{example_problem}.  After examining the circuit you will want to define your state variables, the selection of state variables is as follows: \emph{Voltage across a capacitor, and current through an inductor}.  So, the state variables are $V_C$ and $I_L$ in this case; furthermore, the input to the system is $V_\text{in}$.   Next, we define our voltage loops and current nodes; it will also be helpful to add variables for each of the currents and define voltages.  \fig{example_markedup} shows how the circuit should be marked up if we assume current is flowing from low voltage to high.  
\begin{figure}[ht]
\begin{center}
\includegraphics{06_modeling_elec_figs/example_problem.pdf}
\caption{Example Circuit}
\label{fig:example_problem}
\end{center}
\end{figure}
\begin{figure}[ht]
\begin{center}
\includegraphics{06_modeling_elec_figs/example_markedup.pdf}
\caption{Marked-up Example Circuit}
\label{fig:example_markedup}
\end{center}
\end{figure}

We want each voltage loop to contain exactly one inductor, and each node to branch to exactly one capacitor (the gray loop doesn't contain an inductor, but we will need it later).  Next, we will write each of the \newterm{elemental equations} as is done in Equations~\ref{eqn:example_res}--\ref{eqn:example_cap}.  

\begin{equation}
V_R=RI_R
\label{eqn:example_res}
\end{equation}

\begin{equation}
V_L=L\dot I_L
\label{eqn:example_ind}
\end{equation}

\begin{equation}
I_C=C\dot V_C
\label{eqn:example_cap}
\end{equation}

Notice that each of the previous equations relate a non-state variable ($V_L$ or $I_C$) to the derivative of a state variable ($\dot I_L$ or $\dot V_C$).

Next we will want to apply Kirchoff's voltage law for each loop, the KVL for Loop 1 is shown in \eqn{example_kvl1} (Loop 2 hasn't been introduced yet).  The signs are as such because we are going clockwise around Loop 1.

\begin{equation}
V_C-V_L=0
\label{eqn:example_kvl1}
\end{equation}

Now perform Kirchoff's current law on each important node (the other node, Node B, is unimportant because it will yield the same equation as Node A).  The KCL for Node A is shown in \eqn{example_kcl1}.

\begin{equation}
I_1=I_L+I_C
\label{eqn:example_kcl1}
\end{equation}

The next step is to replace any non-state variables in Kirchoff's equations with state variables, this should be done by substituting elemental equations.  Substituting \eqn{example_ind} into \eqn{example_kvl1} yields \eqn{example_replace1}. Likewise, substituting \eqn{example_cap} into \eqn{example_kcl1} yields \eqn{example_replace2}.

\begin{equation}
V_C-L\dot I_L=0
\label{eqn:example_replace1}
\end{equation}

\begin{equation}
I_1=I_L+C\dot V_C
\label{eqn:example_replace2}
\end{equation}

We're almost done now, all that's left is to remove any non state variables that are left over.  We do this by defining more KVL loops or KCL nodes, and putting the equations into state space form.  \eqn{example_replace1} is already free of non-state variables, so it's fine.  However, we need to get rid of the $I_1$ in \eqn{example_replace2}, which can be done using \eqn{example_res} and by defining Loop 2 with the KVL given in \eqn{example_kvl2}. After dong the algebra we get \eqn{example_remove1}.

\begin{equation}
V_\text{in}-V_R-V_C=0
\label{eqn:example_kvl2}
\end{equation}

\begin{equation}
V_\text{in}-R\left(I_L+C\dot V_C\right)-V_C=0
\label{eqn:example_remove1}
\end{equation}

Re-arranging Equations~\ref{eqn:example_replace1}~and~\ref{eqn:example_remove1} into state space form yeilds \eqn{example_statespace}.

\begin{equation}
\left\{\begin{array}{c} \dot V_C \\ \dot I_L \\ \end{array}\right\} = \left[ \begin{array}{cc} \nicefrac{-1}{RC} & \nicefrac{-1}{C} \\ \nicefrac{1}{L} & 0 \\ \end{array}\right] \left\{\begin{array}{c} V_C \\  I_L \\ \end{array}\right\} + \left[\begin{array}{c} \nicefrac{1}{RC} \\ 0 \\ \end{array}\right]V_\text{in}
\label{eqn:example_statespace}
\end{equation}

To complete the state space form we must form an output equation.  For example, if we wanted the output to be the voltage across the resistor, $V_R$, we would use \eqn{example_output} (which was derived from \eqn{example_kvl2}).

\begin{equation}
y(t)=V_R=\left[\begin{array}{cc} -1 & 0 \\ \end{array}\right]\left\{\begin{array}{c} V_C \\  I_L \\ \end{array}\right\}+ 1V_\text{in} = V_\text{in} - V_C
\label{eqn:example_output}
\end{equation}
