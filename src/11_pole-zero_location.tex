% Why are poles and zeros useful?
\section{Motivation}
Now that we know how to find a system's transfer function, and how to break it down into its poles and zeros, some of you might be saying, ``Big deal, how does this help me?'' These notes should clarify the utility of a transfer function's pole-zero form. 

By looking at the pole and zero locations we can tell quite a bit about the system's unforced response to initial conditions.  We can determine stability, how much damping is present, and the natural frequencies of a system.  Essentially, once you know where the poles and zeros are located you can tell how an \emph{unforced} system will respond under most circumstances.\footnote{We will discuss forced response at a later date}  You might be thinking, ``But we could figure all that stuff by looking at the equations of motion.''  Your observation is correct, but in many cases the equations of motion are too complex to analyze directly.  Furthermore, you won't always have the equations of motion for a system, you will have to find the pole and zero locations---and thus the transfer function---approximately by looking at a system's experimental response (which is the reverse of what we will be doing here).

% How do we plot the poles and zeros
\section{Plotting Poles and Zeros}
Plotting points is not a new concept, but usually we are plotting on an $x$-$y$ plane, and thus need an ordered pair of values; however, when plotting poles and zeros we will be using the $\Re $-$\Im $ plane (also known as the $s$-plane).\footnote{$\Re$ stands for real, and $\Im$ stands for imaginary}  This means we will need to break down the poles and zeros into their real and imaginary parts then plot them as you would on a regular $x$-$y$ plane (see \fig{s_plane}).\footnote{Poles are plotted as an $\times$, and zeros are plotted as a $\bigcirc$}


% After plotting 
\section{Interpreting the Plot}
Now that the poles and zeros are plotted you can easily find several things about the system.  \fig{s_plane} shows the significant measurements you can get from pole locations.  You can get (much of) the same data by re-arranging the transfer function into the form of \eqn{gen_tf}; however, this is only possible for second order systems.  An approximation for higher order systems is discussed below.

\begin{equation}
H(s)=\frac{\omega _n^2}{s^2+2\zeta \omega _ns +\omega _n^2}
\label{eqn:gen_tf}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/s_plane.pdf}
\caption{Example Pole Locations}
\label{fig:s_plane}
\end{center}
\end{figure}

Now, to figure out the properties of the response you can use Equations~\ref{eqn:rise_time}--\ref{eqn:peak_time}, \fig{time_response} will show you the physical representation of those values.

\begin{equation}
t_r \approx \frac{1.8}{\omega _n}
\label{eqn:rise_time}
\end{equation}

\begin{equation}
t_{s,5\%}=\frac{3}{\sigma }
\label{eqn:settling_time}
\end{equation}

\begin{equation}
M_p=e^{\left(\nicefrac{-\pi\zeta }{\sqrt{1-\zeta ^2}}\right)}
\label{eqn:overshoot}
\end{equation}

\begin{equation}
t_p = \frac{\pi}{\omega _d}
\label{eqn:peak_time}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/step_response.pdf}
\caption{System Response to Step Input (Showing Important Measurements)}
\label{fig:time_response}
\end{center}
\end{figure}

Equations~\ref{eqn:rise_time}--\ref{eqn:peak_time} give a few important quantities.  \eqn{rise_time} expresses the \newterm{rise time} of the system, generally the rise time is measured as the time required for the system to move from 10\% to 90\% of its steady-state value.  The \newterm{settling time}, specifically the 5\% settling time, is shown in \eqn{settling_time}, this time represents the time it takes for the bounding envelope of the system response to decay to within 5\% of the steady state value.  The 2\% settling time as well as the 1\% settling time are also useful quantities, they can be calculated as follows:
$$t_{s,2\%}=\frac{3.9}{\sigma }\text{,}$$
$$t_{s,1\%}=\frac{4.6}{\sigma }\text{.}$$
The \newterm{percent overshoot} tells us how far our response will exceed the stead-state value, it can be found analytically using \eqn{overshoot} or graphically with \fig{overshoot_vs_damping}.  The \newterm{peak time} tells us when the maximum value will occur, \eqn{peak_time} allows us to find that value.

\begin{sidewaysfigure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/overshoot_vs_damping.pdf}
\caption{Overshoot as a Function of Damping Ratio}
\label{fig:overshoot_vs_damping}
\end{center}
\end{sidewaysfigure}


% Second Order Dominance
As you can see in \fig{s_plane} there is a third pole which is unaccounted for in \eqn{gen_tf}.  This is perfectly fine because the pole is much farther from the imaginary axis than the poles nearest to the imaginary axis.  This assumption is known as \newterm{second order dominance} and it is valid if the higher order poles are more than 10 times farther away from the $\Im$-axis than the second order poles.

\fig{s_plane} alludes to the fact that pole location is related to damping and natural frequency, when we know our system's natural frequency and damping ratio we can estimate its response to initial conditions or other stimuli with a high degree of accuracy.  \fig{pole_locations_polar} gives a graphical insight into finding the response: First find your damping envelope (radial lines on the figure), then find your undamped response (frequency related to distance from origin), finally, combine the two to show the response.  Figures~\ref{fig:impulse_response} \& \ref{fig:impulse_response_negdamp} show the results of this process, \fig{impulse_response} illustrates a system with positive damping while \fig{impulse_response_negdamp} represents a system with negative damping.

% \fig{pole_locations} shows this in a different manner; each pole (or pole pair if it is complex) is pointing to a subplot that represents its response to an impulse.\footnote{Complex means that the pole has a real and imaginary part}  

%\begin{sidewaysfigure}[ht]
%\begin{center}
%\includegraphics{11_pole_zero_loc_figs/pole_locations.pdf}
%\caption{Impulse Responses for Poles at Several Locations}
%\label{fig:pole_locations}
%\end{center}
%\end{sidewaysfigure}

\begin{sidewaysfigure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/pole_locations_polar.pdf}
\caption{Damping Envelopes and Undamped Responses for Several Segments of the s-Plane}
\label{fig:pole_locations_polar}
\end{center}
\end{sidewaysfigure}

\begin{figure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/impulse_response.pdf}
\caption{Impulse Response of an Underdamped System}
\label{fig:impulse_response}
\end{center}
\end{figure}

\begin{figure}[ht]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/impulse_response_negdamp.pdf}
\caption{Impulse Response of a System with Negative Damping}
\label{fig:impulse_response_negdamp}
\end{center}
\end{figure}

\section{Example}
Let's examine the block diagram given as \fig{pole_placement_block_diagram} in the hopes of finding values for $a$, $b$, and $k$ which will place the closed loop system poles at \unitfrac[10]{rad}{sec} from the origin, with a damping ratio of 0.5.  The third pole should be placed eight times further from the imaginary axis as the second order poles.  

\begin{figure}[h!]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/example_block.pdf}
\caption{Block Diagram With Unknown Terms}
\label{fig:pole_placement_block_diagram}
\end{center}
\end{figure} 

To begin we will need to find the transfer function implied by the block diagram, since this is a simple block diagram we can reduce it easily.

\begin{align}
T_A &= \frac{\frac{k}{s+a}\frac{1}{s+b}\frac{1}{s+8}}{1+\frac{k}{s+a}\frac{1}{s+b}\frac{1}{s+8}} \nonumber \\
&= \frac{k}{(s+a)(s+b)(s+8)+k} \nonumber \\
&= \frac{k}{s^3+(a+b+8)s^2 + (ab+8b+8a)s+8ab+k} \label{eqn:pole_placement_actual_tf}
\end{align}

Now we can figure out what the transfer function should be to place the poles in the desired locations.  Note that the natural frequency is \unitfrac[10]{rad}{sec} and that $\arcsin(0.5) = 30^\circ$.  The pole locations required are depicted in \fig{pole_placement_locations}.

\begin{figure}[h!]
\begin{center}
\includegraphics{11_pole_zero_loc_figs/example_s_plane.pdf}
\caption{Desired Pole Locations}
\label{fig:pole_placement_locations}
\end{center}
\end{figure}

From this we can write our desired system transfer function, recalling that $\sigma = \omega_n\zeta$.

\begin{align}
T_D &= \left(\frac{{\omega_n}^2}{s^2+2\zeta\omega_n s+ \omega_n^2}\right)\left(\frac{8\sigma}{s+8\sigma}\right) \nonumber \\
&= \left(\frac{{\omega_n}^2}{s^2+2\zeta\omega_n s+ \omega_n^2}\right)\left(\frac{8\zeta\omega_n}{s+8\zeta\omega_n}\right) \nonumber \\
&= \frac{8\zeta\omega_n^3}{s^3+(2\zeta\omega_n+8\zeta\omega_n)s^2+(16\zeta^2\omega_n^2 + \omega_n^2)s + 8\zeta\omega_n^3} \label{eqn:pole_placement_desired_tf}
\end{align}

By setting the denominators of Equations~\ref{eqn:pole_placement_actual_tf}~\&~\ref{eqn:pole_placement_desired_tf} equal to one another and comparing the coefficients we can find the following equations, which must be satisfied for the poles to be in the desired locations.
$$a + b + 8 = 10\zeta\omega_n$$
$$ab + 8b + 8a = 16\zeta^2\omega_n^2+\omega_n^2$$
$$8ab+k = 8\omega_n^3\zeta$$
Then substitute the known values of $\omega_n$ and $\zeta$, and solve for the unknowns.

$$\left.\begin{array}{c} a + b + 8 = 50 \\ ab + 8b + 8a = 500 \\ 8ab+k = 4000 \end{array}\right\} \begin{array}{c} a = 4.36 \text{ ---or--- } 37.64 \\ b = 37.64 \text{ ---or--- } 4.36 \\ k = 2688 \end{array}$$

