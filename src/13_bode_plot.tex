\section{Motivation}
Earlier in the course we discussed the response of a system to an initial condition or unit step, and now we will discuss the more general case of a periodic system input.

When you excite a linear system with a sinusoidal input it will respond at the same frequency as the input but with a different amplitude and a specific phase shift (see \fig{ex_plot}). These notes will help you find the amplitude and phase shift of a general system for all frequencies by teaching you how to produce a \newterm{Bode plot}.  Other than showing how a system responds to a given input, a Bode plot is also useful for finding the stability of a system and in controller design (later in the course).  \fig{ex_plot} shows the output of the system in \fig{ex_sys} with an input of $f(t)=\sin (2\pi t)$.  For a general input of $f(t)=A\sin(\omega t+\phi)$ the output will take the form $y(t)=A\left|G(j\omega)\right|\sin\left(\omega t+\phi+\angle G(j\omega)\right)$.

It is important that I mention that a Bode plot is actually \emph{two} plots: a \newterm{gain plot} and a \newterm{phase plot} which are stacked together so the reader can easily find the gain and phase at a specified frequency.

\begin{figure}[ht]
\begin{center}
\includegraphics{13_bode_plot_figs/description/desc_plot.pdf}
\caption{Input and Output Signals of a Simple Second Order System}
\label{fig:ex_plot}
\end{center}
\end{figure}

\begin{figure}[ht]
\begin{center}
\includegraphics{13_bode_plot_figs/desc_sys.pdf}
\caption{Simple Second Order System}
\label{fig:ex_sys}
\end{center}
\end{figure}


\section{Preparing to Sketch a Bode Plot} \label{sec:bode_prep}
To create a Bode plot you will need the transfer function of your system.  First you will want to factor it down (both numerator and denominator) into its smallest real polynomials.\footnote{If factoring a second order polynomial would yield imaginary parts then leave it as a second order polynomial}  Once that is done the numerator and denominator should only be comprised of the product of the following four types of terms:
\begin{enumerate}
\item Constant gain, $k$;
\item Singular $s$ terms (free integrators), $s^n$;
\item First order terms, $(s+\alpha )^n$; and
\item Second order terms, $(s^2+2\zeta \omega _n s+\omega _n^2)^n$.
\end{enumerate}

Each of those four classes of terms is plotted in its own way, but this will be discussed later.  The next thing you should do is set $s=j\omega $, and solve your first and second order terms to the following form: $\alpha ^n(\nicefrac{j\omega}{\alpha} +1)^n$, and $\omega_n^{2n}((\nicefrac{j\omega}{\omega _n})^2+2\zeta (\nicefrac{j\omega}{\omega _n})+1)^n$.  You will now want to collect all your constant product terms ($\alpha ^n$, $\omega _n^{2n}$, and $k$s) and call it $k_0$, and you are ready to plot.

\section{Theory}
Before we progress I'm going to introduce the mathematical concepts behind Bode plotting.

As previously mentioned, the Bode plot is broken down into two parts: the gain plot, and the phase plot,  both of which are functions of the excitation frequency, $\omega$.  The gain is unitless, however the Bode plot gives the magnitude of the gain in decibels, \unit{dB}, and the phase is in degrees.  To calculate the gain of your transfer function you take the square root of the sum of the squares of the real and imaginary parts of the transfer function (\eqn{tf_gain}), the magnitude of the gain is found by taking the logarithm of the gain and multiplying by 20 (\eqn{tf_mag}).  To find the phase you take the inverse tangent of the quotient of the imaginary and real parts of the transfer function (\eqn{tf_phase}). 

\begin{equation}
\left| G(j\omega )\right| = \sqrt{\Re\left\{G(j\omega)\right\}^2+\Im\left\{G(j\omega)\right\}^2}
\label{eqn:tf_gain}
\end{equation}

\begin{equation}
\text{Magnitude} = 20\log \left| G(j\omega ) \right|
\label{eqn:tf_mag}
\end{equation}

\begin{equation}
\angle G(j\omega) = \tan ^{-1} \left( \frac{\Im \{G(j\omega )\}}{\Re \{G(j\omega )\}}\right)
\label{eqn:tf_phase}
\end{equation}

If we are trying to find the gain and phase of a system we would apply the previously given equations to each part of the transfer function individually, then take the product of the gains (gains from parts\footnote{When I say ``part'' I am referring to an individual term from the list in \S\ref{sec:bode_prep}} in the denominator remain in the denominator) and sum up the phases (phases from parts in the denominator are subtracted).  


There is a graphical correlation to the gain and phase of a term.  If we find the numerical values of the real and imaginary parts of a term at a specific frequency and plot them on an $s$-plane, then we can find the gain and phase graphically by looking at the length (and angle relative to the positive real axis) of the vector formed by the real and imaginary parts, as is shown in \fig{vector_explaination}.  If we do this twice---once for a very small value of $\omega$, and once for a large value of $\omega$---then we can track the change in phase angle over the spectrum of frequencies.

\begin{figure}[ht]
\begin{center}
\includegraphics{13_bode_plot_figs/vector_explaination/vector.pdf}
\caption{The Vector Approach to Finding Gain and Phase}
\label{fig:vector_explaination}
\end{center}
\end{figure}


We'll need to remember some properties of the logarithm, namely those given in Equations~\ref{eqn:log_frac}--\ref{eqn:log_inverse}.

\begin{equation}
\log\left(\frac{\alpha}{\beta}\right)=\log (\alpha)-\log(\beta)
\label{eqn:log_frac}
\end{equation}

\begin{equation}
\log\left(\alpha\beta\right)=\log (\alpha)+\log(\beta)
\label{eqn:log_product}
\end{equation}

\begin{equation}
\log\left(\alpha^\beta\right)=\beta\log\left(\alpha\right)
\label{eqn:log_power}
\end{equation}

\begin{equation}
\log\left(10^\alpha\right)=\alpha
\label{eqn:log_inverse}
\end{equation}

\section{Sketching a Bode Plot}
In \S\ref{sec:bode_prep} I gave a list of the four classes of transfer function terms which could have been present in your function.  We should have translated those into the following forms (following the direction in \S\ref{sec:bode_prep}):
\begin{enumerate}
\item Constant gain, $k_0$;
\item Singular $s$ terms, $(j\omega)^n$;
\item First order terms, $(\nicefrac{j\omega}{\alpha}+1 )^n$; and
\item Second order terms, $((\nicefrac{j\omega}{\omega _n})^2+2\zeta (\nicefrac{j\omega}{\omega _n})+1)^n$.
\end{enumerate}

As previously stated, these will each be plotted---individually---in there own way, and super-positioned (added together) to form the Bode plot.  We should also note that the Bode plot is drawn on a semi-log scale which puts $\omega =0$ at $-\infty$ on the horizontal axis.

\subsection{Constant Gains}
A gain, $k_0$, has a constant value for all frequencies, thus its gain plot is a flat line at $20\log|k_0|$, and is its phase is constant at $0^\circ$ since $\tan^{-1}(\nicefrac{0}{k})\equiv0$.  \fig{ex_bode}a shows how a constant gain should be plotted.
\subsection{Singular $s^n$ Term}
The $(j\omega)^n$ term is plotted as a straight line through $\omega =\unitfrac[1]{rad}{sec}$ with a slope of $\unitfrac[20n]{dB}{dec}$.\footnote{The unit \unitfrac{dB}{dec} indicates that the value changes by a single decibel in each decade, or order of magnitude, of frequency (e.g.\ 1 \unit{dB} change from $\omega=10$ to $\omega=100$)}  The phase is a constant  $90n^\circ $.  \fig{ex_bode}b shows the plot of an $s$ term.
\subsection{First Order Terms} 
First order terms, $(\nicefrac{j\omega}{\alpha} +1)^n$, have two regimes of operation that need to be sketched: 
\begin{itemize}
\item for $\nicefrac{\omega}{\alpha} < 1$, $\nicefrac{j\omega}{\alpha} +1 \approx 1$, and
\item for $\nicefrac{j\omega}{\alpha} > 1$, $\nicefrac{j\omega}{\alpha} +1 \approx \nicefrac{j\omega}{\alpha}$.
\end{itemize}

Thus for $\omega < \alpha$ the gain is about 1 (\unit[0]{dB}), and for $\omega > \nicefrac{1}{\tau}$ the gain acts like a singular $s$ term.  In reality there is a non-linear section around $\omega = \alpha$ (the \newterm{break point}, or \newterm{corner frequency}), but when sketching this can be ignored.  The phase is $0^\circ$ before the break point and $90n^\circ$ after the break point (once again, in reality there is a non-linear section around the break point).  \fig{ex_bode}c shows the plot of a first order term.
\subsection{Second Order Terms}
A second order term $((\nicefrac{j\omega}{\omega _n})^2+2\zeta (\nicefrac{j\omega}{\omega _n})+1)^n$ is plotted in much the same way as a first order term with the following differences.  The break point is $\omega = \omega _n$, the slope after the break point will be $\unitfrac[40n]{dB}{dec}$, and the phase after the break point will be $180n^\circ$.  Also, the non-linear section near the break point is significant, and is related to $\zeta$, namely, $|G(j\omega _n)|=2\zeta$, and a lower damping yields a steeper transition in the phase plot.  \fig{ex_bode}d shows the plot of a second order term.\footnote{The figure shows a negative slope and phase because $n=-1$}

\section{Poorly Behaved Systems}
A system with a pole or zero in the \newterm{right half plane}\footnote{A pole or zero that appears on the positive real side of the $s$-plane, abbreviated RHP} will have a phase plot that starts at an odd location, that is to say that its value as $\omega \rightarrow 0$ is not what it seems it should be.  When you are given a system you should check that each of the poles and zeros are on the \newterm{left half plane}\footnote{The opposite of the right half plane, abbreviated LHP}, and if they aren't you need to use \eqn{tf_phase} to determine how the phase behaves.  A system with a pole in the RHP is called \newterm{unstable}, and a system with a zero in the RHP is called \newterm{non-minimum phase}.

If you come across a poorly behaved system then the gain will be unaffected, but the phase is mirrored about the final phase value.  So if you would normally have a system that should vary from $0$ to $90$ it would vary from $180$ to $90$. 

\section{Interpreting a Bode Plot}
Sketching the Bode plot is only half the battle, in fact it is way less than half the battle; the real meat and potatoes of Bode plots is their interpretation.  When you are exploring a linear system it is important to note the relationship between the input and the output of the system.  Given a system, $G(s)$, with an input of $r(t)=A\sin(\omega_0t+\phi)$, the output will be $y(t)=|G(j\omega_0)|A\sin(\omega_0t+\phi+\angle G(j\omega_0))$ once the \newterm{transient response} dies out (at steady state).  This knowledge is what makes Bode plots useful, all that needs to be done to find the steady state response of a system is to look at the Bode gain and phase at the excitation frequency.  

For a system with a non-oscillatory input (e.g.\ a step input) you need only consider the gain at an operational frequency of zero ($\omega_0=0$).  Given a system, $G(s)$, with an input of $r(t)=Au(t)$, the output will be $y(t)=A|G(0j)|$ at steady state.  This is basically an application of the final value theorem.

\section{Example}
Let's sketch the Bode plot of the system given in \eqn{example_problem}, and find the system's steady state response to the following input: $r(s)=3\cos(2t)$. 
 
\begin{equation}
G(s)=\frac{1}{1000}\frac{(s+100)^2}{s^2(s^2+6s+50)}
\label{eqn:example_problem}
\end{equation}

The first thing we will want to do is substitute in $s =j\omega $:
$$G(j\omega)=\frac{1}{1000}\frac{(j\omega+100)^2}{(j\omega)^2((j\omega)^2+6j\omega+50)}\text{.}$$
Then we will get each of the polynomials into standard form:
$$G(j\omega)=\frac{100^2}{(1000)50}\frac{(\frac{j\omega}{100}+1)^2}{(j\omega)^2(\frac{(j\omega)^2}{50}+\frac{6}{50}j\omega+1)}\text{.}$$
Now we can define the Bode plots for each of the terms:
\begin{itemize}
\item $k_0=\nicefrac{10000}{1000(50)}=0.2$, $20\log|0.2|=-13.979$, and $\angle k_0=0^\circ$; see \fig{ex_bode}a.
\item $(j\omega )^{-2}$, the gain passes through $\omega =1$ and has a constant slope of $\unitfrac[-40]{dB}{dec}$, the phase is a constant $-180^\circ$ (since $n=-2$); see \fig{ex_bode}b.
\item $(\nicefrac{j\omega}{100}+1)^2$, the gain has a break point at $\omega =\unitfrac[100]{rad}{sec}$ and will increase at \unitfrac[40]{dB}{dec} (since $n=2$), the phase will move from $0^\circ$ to $180^\circ$ at the break point; see \fig{ex_bode}c.
\item $(\nicefrac{(j\omega)^2}{50}+\nicefrac{6}{50}j\omega+1)^{-1}$ has a break point at $\omega = \omega _n = \sqrt{50}$, and will have a slope of \unitfrac[-40]{dB}{dec} after the break point, the phase will move from $0^\circ$ to $-180^\circ$ at the break point; see \fig{ex_bode}d.
\end{itemize}

Now, when you super-position (add) all those individual Bode plots together you will get the Bode plot of the entire system, which is shown in \fig{ex_bode_complete}.

Aside: If you use the equations from the Theory Section then you can find a single continuous function that represents the gain and phase as a function of $\omega$ as such:

\begin{eqnarray*}
|G(j\omega)| &=&\left|\frac{1}{1000}\right|\frac{|(j\omega+100)^2|}{|(j\omega)^2||(j\omega)^2+6j\omega+50|}\\
&=&\left|\frac{1}{1000}\right|\frac{|(j\omega+100)|^2}{|(j\omega)|^2|(j\omega)^2+6j\omega+50|}\\
&=&\sqrt{\frac{1}{1000}}^2\frac{\sqrt{100^2+\omega^2}^2}{\sqrt{\omega^2}^2\sqrt{(-\omega^2+50)^2+(6\omega)^2}}\\
&=&\frac{1}{1000}\frac{100^2+\omega^2}{\omega^2\sqrt{(-\omega^2+50)^2+(6\omega)^2}}\\
20\log|G(j\omega)|&=&20\log\left(\frac{1}{1000}\right) + 20\log (\omega^2+100^2) \\
&&- 20\log (\omega^2) - 20\log \left(\sqrt{(50-\omega^2)^2+36\omega^2}\right)
\end{eqnarray*}

\begin{eqnarray*}
\angle G(j\omega) &=& 2\tan^{-1}\left(\frac{\omega}{100}\right)-2\tan^{-1}\left(\frac{\omega}{0}\right)-\tan^{-1}\left(\frac{6\omega}{50-\omega^2}\right)\\
&=& 2\tan^{-1}\left(\frac{\omega}{100}\right)-180^\circ-\tan^{-1}\left(\frac{6\omega}{50-\omega^2}\right)
\end{eqnarray*}
%G(j\omega)=\frac{1}{1000}\frac{(j\omega+100)^2}{(j\omega)^2((j\omega)^2+6j\omega+50)}

\begin{figure}[ht]
\begin{center}
\includegraphics{13_bode_plot_figs/example/ex_plot.pdf}
\caption{Individual Polynomial Bode Plots for Example Problem}
\label{fig:ex_bode}
\end{center}
\end{figure}

\begin{figure}[ht]
\begin{center}
\includegraphics{13_bode_plot_figs/example/ex_plot_comp.pdf}
\caption{Complete Bode Plot for Example Problem}
\label{fig:ex_bode_complete}
\end{center}
\end{figure}

To find the steady state response we can look at the Bode plot, or we can plug the excitation frequency, $\omega_0=2$ into the gain and phase relationships we defined earlier.
$$\frac{1}{1000}\frac{100^2+2^2}{2^2\sqrt{(-2^2+50)^2+(12)^2}}=0.0526$$
$$20 \log(0.0526)=-25.58$$
$$2\tan^{-1}\left(\frac{2}{100}\right)-180^\circ-\tan^{-1}\left(\frac{12}{50-2^2}\right)=-192.3^\circ$$
Thus the steady state response of our system is $y(t)=0.1578\sin(2t-192.3^\circ)$.
