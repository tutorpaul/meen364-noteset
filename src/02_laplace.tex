\section{Definition}
The \newterm{Laplace transformation} is a special case of the Fourier transformation, and it allows us to solve differential equations relatively easily.  Also, it allows us to model systems as simple transfer functions (which will come later in this course).  Strictly speaking, the definition of the Laplace transformation is given as \eqn{laplace_trans}, however it is quite common to use a lookup table like the one given as \tbl{laplace_trans}.\footnote{A more extensive Laplace transformation table can be found in the course textbook or online}  

\begin{equation}
F (s) = {\cal L} \left\{ f(t)\right\} = \int _{-\infty} ^\infty f(t)u(t)e^{-st}\, dt = \int _0 ^\infty f(t)e^{-st}\, dt
\label{eqn:laplace_trans}
\end{equation}
To explain, the script `L' ($\cal L$), known as the Laplace Operator, is used to indicate the Laplace transformation of the function enclosed in the braces.  Also, $u(t)$ is the \newterm{Heaviside} function (\eqn{heaviside})---which is identical to zero before time zero, and identically one at time zero and after---the function, $f(t)$, should be a continuous function, and $s$ is a general complex variable ($s=\sigma + j\omega$). 

\begin{equation}
u(t)=\left\{\begin{array}{cl} 0 & t<0 \\ 1 & t\geq 0 \end{array} \right. 
\label{eqn:heaviside}
\end{equation}

In general taking the Laplace transformation is pretty easy using the definition (\eqn{laplace_trans}), but performing the inverse Laplace transformation---which is defined in \eqn{inv_laplace_trans}---is typically quite difficult to execute, thus it is not used.  This makes the lookup table that much more valuable; however, you will want to brush up on \newterm{partial fraction decomposition} to facilitate the usage of the table for inverse Laplace transformations, for an overview of partial fraction decomposition see \S\ref{sec:pfd} (pg.\ \pageref{sec:pfd}).  

\begin{equation}
f(t)=  {\cal L}^{-1} \left\{ F(s) \right\} = \frac{1}{2\pi j} \int _{c-j\infty} ^{c+j\infty} F(s) e^{st}\, ds
\label{eqn:inv_laplace_trans}
\end{equation}


 % Properties
 % Transformations

\begin{table}[ht]
\begin{center}
\caption{Common Laplace Transformations}
\label{tbl:laplace_trans}
\begin{tabular}{lc}
\hline
Time Domain & Frequency Domain \\
\hline
$\delta(t)$ (Impulse) & $1$ \\ 
$u(t)$ (Step) & $\frac{1}{s}$ \\ 
$t$ (Ramp) & $\frac{1}{s^2}$ \\
$t^a$ & $\frac{a!}{s^{a+1}}$ \\ 
$e^{-at}$ & $\frac{1}{s+a}$ \\ 
$te^{-at}$ & $\frac{1}{(s+a)^2}$ \\ 
$\sin (\omega t)$ & $\frac{\omega}{s^2+\omega ^2}$ \\ 
$\cos (\omega t)$ & $\frac{s}{s^2+\omega ^2}$ \\
\hline 
\end{tabular}
\end{center}
\end{table}

\section{Properties}

There are some simple mathematical properties of the Laplace transform listed in \tbl{laplace_props}, these come in quite handy when you are trying to: solve a differential equation, form a transfer function (later in the course), or perform a Laplace transformation.  You will do well to remember them all, but the most important one is differentiation.  Also the \newterm{initial value theroem} and \newterm{final value theorem} (Equations~\ref{eqn:ivt}~and~\ref{eqn:fvt}, respectively) will certainly be used later in the course to (you guessed it) find the initial and steady state values of a function in the \newterm{frequency domain}.\footnote{A function that has been Laplaced is said to be in the frequency domain}  The proofs of these properties are given in \S\ref{sec:laplace_props_proof} (pg.\ \pageref{sec:laplace_props_proof}).

\begin{table}[ht]
\begin{center}
\caption{Properties of the Laplace Transformation}
\label{tbl:laplace_props}
\begin{tabular}{lc}
\hline
Name & Property \\
\hline
Integration & ${\cal L} \left\{ \int f(t)\,dt \right\} =\frac{F(s)}{s}$ \\
Differentiation & ${\cal L} \left\{ \frac{d}{dt} f(t) \right\} =sF(s)-f(0)$ \\
Linearity & ${\cal L} \left\{ \alpha f(t) + \beta g(t) \right\} = \alpha F(s)+ \beta G(s)$ \\
Time Shift & ${\cal L} \left\{ f(t-a) \right\} = e^{-as}F(s)$ \\
Frequency Shift & ${\cal L} \left\{ f(t)e^{-at}\right\} = F(s+a)$ \\
Frequency Differentiation & ${\cal L}\left\{tf(t)\right\}=\frac{-d}{ds}F(s)$ \\
\hline
\end{tabular}
\end{center}
\end{table}


\begin{equation}
f(0^+)=\lim _{s\to \infty}sF(s)
\label{eqn:ivt}
\end{equation}

\begin{equation}
\lim _{t\to \infty}f(t)=\lim _{s\to 0}sF(s)
\label{eqn:fvt}
\end{equation}

\section{Example}
Let's solve the following initial value problem using the Laplace transform.
\begin{eqnarray*}
\ddot x(t)+5\dot x(t)+4x(t)&=&e^{-3t} \\
x(0)&=&2 \\
\dot x(0)&=&0 
\end{eqnarray*}

First, we will take the Laplace transformation of the differential equation using \tbl{laplace_trans}, and the properties in \tbl{laplace_props}.
\begin{eqnarray*}
s^2X(s)-sx(0)-\dot x(0)+5(sX(s)-x(0))+4X(s) &=&\frac{1}{s+3} \\
s^2X(s)-2s+5sX(s)-10+4X(s) &=& \frac{1}{s+3} \\
(s^2+5s+4)X(s)-(2s+10)&=&\frac{1}{s+3}
\end{eqnarray*}
Then we will solve the transformed equation for the dependent variable, $X(s)$.
\begin{eqnarray*}
X(s)&=&\frac{1}{(s+3)(s^2+5s+4)}+\frac{2s+10}{s^2+5s+4} \\
 &=&\frac{1}{(s+3)(s^2+5s+4)}+\frac{(2s+10)(s+3)}{(s^2+5s+4)(s+3)} \\
 &=&\frac{2s^2+16s+31}{(s+3)(s^2+5s+4)}
\end{eqnarray*}
Next we will expand the denominator and perform partial fraction decomposition.
\begin{eqnarray*}
X(s)&=&\frac{2s^2+16s+31}{(s+3)(s+1)(s+4)} \\
&=&\frac{A}{s+3}+\frac{B}{s+1}+\frac{C}{s+4} \\
&=&\frac{A(s+1)(s+4)+B(s+3)(s+4)+C(s+3)(s+1)}{(s+3)(s+1)(s+4)} \\
\therefore2s^2+16s+31&=&A(s+1)(s+4)+B(s+3)(s+4)+C(s+3)(s+1)
\end{eqnarray*}
Solving for the constants gives $A=\nicefrac{-1}{2}$, $B=\nicefrac{17}{6}$, and $C=\nicefrac{-1}{3}$; thus:
$$X(s)=\frac{-1}{2(s+3)}+\frac{17}{6(s+1)}-\frac{1}{3(s+4)}. $$
All that is left to do is apply \tbl{laplace_trans} to take the inverse Laplace transform.
$$x(t)=\frac{-1}{2}e^{-3t}+\frac{17}{6}e^{-t}-\frac{1}{3}e^{-4t}$$