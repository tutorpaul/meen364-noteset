set terminal fig
set output 'ex_rlocus.fig'
set size 1,1
set lmargin 6
set rmargin 1
#set ylabel 'Magnitude(dB)'
set ylabel 'Imaginary Axis'
set xlabel 'Real Axis'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set yrange [-30:30]
set xrange [-16:4]
#set xzeroaxis
set yzeroaxis

plot "ex_rlocus.dat" u 2:3 w lines 3,\
     "ex_rlocus.dat" u 4:5 w lines 5,\
     "ex_rlocus.dat" u 6:7 w lines 9
