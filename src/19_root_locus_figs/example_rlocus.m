A=tf([1 -3],[1 6 25]);
B=tf(1,[1 15]);

[R,K]=rlocus(A*B)

fid= fopen('ex_rlocus.dat','wt');

fprintf(fid,'#    gain    real1    imag1   real2     imag2  real3   imag3\n');
for i=1:length(K)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E\n',...
        K(i),real(R(1,i)),imag(R(1,i)),real(R(2,i)),imag(R(2,i)),real(R(3,i)),imag(R(3,i)));
end
fclose(fid);