\section{Feedback}
You need to know how to manipulate block diagrams to find transfer functions with respect to inputs other than the reference (set point).  That aside you should understand how a \index{feedback}feedback loop can improve a system's response in the following ways:
\begin{enumerate}
\item Rejecting disturbances,
\item Reference tracking,
\item Noise Reduction, and
\item Reducing Sensitivity.
\end{enumerate}

\section{PID Control}
\fig{app_pid_control} shows a generic \index{PID controller}PID controller, the three gains---$k_p$, $k_i$, and $k_d$---correspond to the \index{proportional gain}proportional gain, the \index{integral gain}integral gain, and the \index{derivative gain}derivative gain, respectively.  The proportional gain represents the gain that is applied to the current error.  Increasing $k_p$ speeds up the response of the system and reduces the steady state error.  However, if you increase it too much the system could go unstable. Integral gain is applied to the integral of the error over time (a cumulative error).  Increasing integral gain reduces steady state error (to zero),\footnote{The higher the value of $k_i$, the faster the system will reach the steady state value} but will also increase overshoot.  Another side effect of integral control is increased settling time.  Derivative gain is applied to the derivative of the error over time (the slope of the error).  Increasing derivative gain will tend to reduce overshoot, but will make the system more sensitive to noise (usually not used for that reason).  \tbl{app_pid_gains} gives a tabular form of the data in this paragraph consider it when \index{tuning}tuning your gains.

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/desc_pid.pdf}
\caption{Generic PID Controller}
\label{fig:app_pid_control}
\end{center}
\end{figure}

\begin{table}[ht]
\begin{center}
\caption{Effects of Increasing PID Gain Values}
\label{tbl:app_pid_gains}
\begin{tabular}{c|ccccc}
\cline{1-5}
& $t_r$ & $t_s$ & $M_p$ & $e_\text{ss}$ & \\
\cline{1-5}
$\uparrow k_p $ & $ \downarrow\downarrow$ & $\times $ & $ \uparrow $ & $ \downarrow $ & \\
$\uparrow k_i $ & $\times$ & $\uparrow$ & $\uparrow$ & $\downarrow\downarrow$  &\\
$\uparrow k_d $ & $\downarrow$ & $\downarrow\downarrow$ & $\downarrow\downarrow$ & $\times$ & \\ 
\cline{1-5}
\multicolumn{6}{l}{$\times$: no change, $\uparrow$: increase}\\
\multicolumn{6}{l}{$\downarrow$: decrease, $\downarrow\downarrow$: large decrease}\\
\end{tabular}
\end{center}
\end{table}

\section{Steady State Error}
For a unity feedback system you can use Table~\ref{tbl:app_ss_err} to determine a system's steady state error if you know the \index{system type}system type.  Use Equations~\ref{eqn:app_pos_const}--\ref{eqn:app_acc_const} to find the constants associated with the steady state error.  Remember that system type corresponds to the number of free integrators ($\nicefrac{1}{s}$ terms that can be factored out of the transfer function) present in the system.

\let\origarraystretch\arraystretch
\renewcommand\arraystretch{1.75}
\begin{table}[ht]
\begin{center}
\caption{Steady State Errors for System Types and Inputs}
\label{tbl:app_ss_err}
\begin{tabular}{lccc}
\hline
& \multicolumn{3}{c}{Input} \\
\cline{2-4}
Type & Step ($R(s)=\frac{1}{s}$) & Ramp ($R(s)=\frac{1}{s^2}$) & Parabola ($R(s)=\frac{1}{s^3}$) \\
\hline
Type 0 & $\nicefrac{1}{1+K_p}$ & $\infty$ & $\infty$ \\
Type 1 & 0 & $\nicefrac{1}{K_v}$ & $\infty$ \\
Type 2 & 0 & 0 & $\nicefrac{1}{K_a}$ \\
\hline
\end{tabular}
\end{center}
\end{table}
\let\arraystretch\origarraystretch

\begin{equation}
K_p=\lim_{s\to 0}D(s)G(s)
\label{eqn:app_pos_const}
\end{equation}
\begin{equation}
K_v=\lim_{s\to 0}sD(s)G(s)
\label{eqn:app_vel_const}
\end{equation}
\begin{equation}
K_a=\lim_{s\to 0}s^2D(s)G(s)
\label{eqn:app_acc_const}
\end{equation}

In general, Equation~\ref{eqn:app_ss_err}, which is an application of the \index{final value theorem}final value theorem, can be used to find steady state error.

\begin{equation}
e_{ss}=\lim _{s\to 0}s\left(R(s)-Y(s)\right)
\label{eqn:app_ss_err}
\end{equation}


If you are dealing with a reference tracking problem ($R(s)\neq 0$), then Equation~\ref{eqn:app_ss_err} can be simplified to:
$$e_{ss}=\lim _{s\to 0}sR(s)\left(1-T(s)\right);$$
where $T(s)$ is the transfer function from $R(s)$ to $Y(s)$.  Algebra can be your best friend or your worst enemy in this situation, be cautious to prevent errors.  If there is no reference input ($R(s)=0$), then Equation~\ref{eqn:app_ss_err} will simplify to:
$$e_{ss}=\lim _{s\to 0}-sY(s).$$
Don't forget:
$$Y(s)=\frac{Y(s)}{W(s)}W(s);$$
where $W(s)$ is some other input (disturbance or noise).

\section{Routh Array}

If given a transfer function with an unknown gain constant, usually $k$, and asked to find the range of gains which make the system stable, simply use a \index{Routh array}Routh array.  Given the following transfer function:
$$T(s)=\frac{a_0s^m+a_1s^{m-1}+a_2s^{m-2}+\cdots+a_{m-1}s+a_m}{b_0s^n+b_1s^{n-1}+b_2s^{n-2}+\cdots+b_{n-1}s+b_n},$$
re-arrange the denominator to seed the Routh Array (fill in the first two rows):
\begin{center}
\begin{tabular}{ccccc}
$s^n$: & $b_0$ & $b_2$ & $b_4$ & $\cdots$ \\
$s^{n-1}$: & $b_1$ & $b_3$ & $b_5$ & $\cdots$ \\
$s^{n-2}$: & $\frac{b_1b_2-b_0b_3}{b_1}=c_1$ & $\frac{b_1b_4-b_0b_5}{b_1}=c_2$ & $c_3$ & $\cdots$ \\
$s^{n-3}$: & $\frac{c_1b_3-b_1c_2}{c_1}=d_1$ & $\frac{c_1b_5-b_1c_3}{c_1}=d_2$ & $d_3$ & $\cdots$ \\
$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\ 
$s^0$: & $q_1$ & 0 & 0 & 0 \\ 
\end{tabular}
\end{center}

The number of sign changes in the first column\footnote{The column with all the $s$ terms is \emph{not} the first column, the one to its right is.} of the table after all the elements in the array have been calculated is the number of poles on the RHP of the $s$-plane.  Only when all the elements in the first column are positive is the system stable.

\section{Bounded Input-Bounded Output Systems}
If a system is defined as \index{BIBO}\index{bounded input-bounded output}bounded input-bounded output then you know that the system only has poles in the LHP, thus the system is \index{stability}stable and the steady state response can be predicted for any input regardless of initial conditions.  The steady state response will look like this:
$$y(t)=r_0 \left|C(j\omega_0)\right| \sin\left(\omega _0 t + \phi + \angle C(j\omega_0)\right); $$ %try a \, if the spacing isn't right
where $C(j\omega )$ is the transfer function of the system, and the input is:
$$r(t)=r_0\sin \left(\omega _0 t +\phi\right) $$

\section{Phase and Gain}
To find the phase and gain of a system at a particular frequency, $\omega _0$, you must first break the system down into its fundamental parts just like when Bode plotting.  Find the gain and phase for each part of the transfer function by using Equations~\ref{eqn:app_phase} and \ref{eqn:app_gain}, respectively.  Find the total gain by taking the product of the gains of each individual part of the transfer function in the numerator, then divide by the gains of each part of the transfer function in the denominator.  Find the total phase by summing the phase of each transfer function part in the numerator, and subtract the phase of each part in the denominator.

\begin{equation}
\left|C(j\omega_0)\right|=\sqrt{\Re\left\{ C(j\omega_0 ) \right\}^2 +\Im\left\{ C(j\omega_0 ) \right\}^2}
\label{eqn:app_phase}
\end{equation}
\begin{equation}
\angle C(j\omega_0 ) =\tan ^{-1}\left(\frac{\Im\left\{ C(j\omega_0 ) \right\}}{\Re\left\{ C(j\omega_0 ) \right\}}\right)
\label{eqn:app_gain}
\end{equation}

The decibel gain (or magnitude as I will refer to it in these notes) is found using \eqn{app_magnitude}

\begin{equation}
20 \log\left|C(j\omega_0)\right|\equiv\text{Magnitude}
\label{eqn:app_magnitude}
\end{equation}

\section{Stability Margins}
The \index{stability margins}stability margins of a system are the \index{gain margin}\emph{gain margin} and \index{phase margin}\emph{phase margin}, when \emph{both} of them have positive values the system is stable.  The gain margin (GM) is a measure of how much the (open-loop) gain can be increased before the system goes unstable.  It is defined as the value of the magnitude below the $0$ magnitude line at the frequency when the phase crosses the $-180^\circ$ line.\footnote{A negative magnitude is a positive gain margin}  Phase margin (PM) is essentially a measure of the system's damping ratio, \eqn{app_phase_damping} gives the relationship between phase margin and damping.  The phase margin is the value of the phase above the $-180^\circ$ line at the frequency when the gain crosses the $0$ magnitude line.  \fig{app_stability_margins} shows how to measure the gain and phase margin from a Bode plot, if you do not have a Bode plot to work from you can use the phase and gain equations (Equations~\ref{eqn:app_phase}~and~\ref{eqn:app_gain}).

\begin{equation}
\zeta=\frac{\text{PM}}{100}
\label{eqn:app_phase_damping}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{17_stability_margin_figs/margin.pdf}
\caption{Gain and Phase Margin Measurements}
\label{fig:app_stability_margins}
\end{center}
\end{figure}

\section{Compensators}
You can add a compensator to a system to improve any specific realm of performance. To improve bandwidth (system speed) and damping you can add a lead compensator, and to improve steady state performance you can add a lag compensator.

\subsection{Lead Compensators}
Using \eqn{app_lead_comp} as the base \index{lead compensator}lead compensator, complete the following steps given an open loop transfer function, $G(s)$; a required phase margin, $\phi _\text{req}$; and a required steady state error.  

\begin{equation}
D(s)=K\frac{Ts+1}{\alpha Ts+1} \text{\quad where $\alpha < 1$} 
\label{eqn:app_lead_comp}
\end{equation}

\begin{enumerate}
\item Combine the system gain, $K_1$ (if there is one), with the lead gain, $K$, such that  $K_t=KK_1$.  This will leave the original system without its gain, we will call the gain-less system $G^*(s)$.
\item Using the steady state error requirement, determine the needed gain, $K_t$.
\item Determine the phase margin of the system with gain $K_t$, this phase margin will herein be known as $\phi _\text{uncomp}$.
\item Find the needed phase increase, $\phi _\text{comp}$, to get the required phase margin, $\phi _\text{req}$, using \eqn{app_phase_req} where $\varepsilon$ is a safety margin ($5^\circ$ to $12^\circ$); on the exam they will likely tell you what safety margin to use.

\begin{equation}
\phi _\text{comp}=\phi _\text{req}-\phi _\text{uncomp}+\varepsilon
\label{eqn:app_phase_req}
\end{equation}

\item Using \eqn{app_find_alpha}, find $\alpha$.

\begin{equation}
\alpha = \frac{1-\sin\left(\phi _\text{comp}\right)}{1+\sin\left(\phi _\text{comp}\right)}
\label{eqn:app_find_alpha}
\end{equation}

\item Find the compensated system's \index{crossover frequency}crossover frequency, $\omega _\text{comp}$, by applying \eqn{app_find_crossover}.

\begin{equation}
20\log|K_t G^*(j\omega _\text{comp})|=-10\log\left(\frac{1}{\alpha}\right)
\label{eqn:app_find_crossover}
\end{equation}

\item Compute the compensator's break frequencies, $\omega _1$ and $\omega _2$ (and, by proxy, $T$) using Equations~\ref{eqn:app_break_freq_1}~and~\ref{eqn:app_break_freq_2}.

\begin{equation}
\omega _1 = \frac{1}{T} =\omega _\text{comp}\sqrt\alpha
\label{eqn:app_break_freq_1}
\end{equation}

\begin{equation}
\omega _2 = \frac{1}{\alpha T} =\frac{\omega _\text{comp}}{\sqrt\alpha}
\label{eqn:app_break_freq_2}
\end{equation}

\item Sketch the compensated system's Bode plot to ensure the design requirements are met.  If the phase margin isn't high enough, repeat the previous steps with a larger $\varepsilon$.
\end{enumerate}

\subsection{Lag Compensators}
Use \eqn{app_lag_comp} as the standard \index{lag compensator}lag compensator, and perform the following steps to find $\alpha$ and $T$.   You will need a desired steady state error (the appropriate desired error constant: $K_p$, $K_v$, or $K_a$, will be known as $K_\text{des}$), and a minimum allowable phase margin.  The open loop transfer function will typically have a built in variable gain, $K_1$ (just as before).

\begin{equation}
D(s)=\alpha\frac{Ts+1}{\alpha Ts+1} \text{\quad where $\alpha > 1$} 
\label{eqn:app_lag_comp}
\end{equation}

\begin{enumerate}
\item Pick the system gain, $K_1$, so the minimum phase margin (plus some factor of safety, $\varepsilon$) is met.
\item Find the system's crossover frequency, $\omega _c$ with the gain $K_1$.
\item From the adjusted system ($K_1$ included), find the system's steady state error constant ($K_p$, $K_v$, or $K_a$, depending on system type will be called $K_\text{act}$).
\item Choose a value for $\alpha$ such that $K_\text{des}=\alpha K_\text{act}$.
\item Compute $T$ using \eqn{app_lag_break_freq}, where $d$ is some value between 2 and 10. If they tell you to place the zero's corner frequency a decade before the system's crossover frequency use $d=10$.

\begin{equation}
\frac{1}{T}=\frac{\omega_c}{d}
\label{eqn:app_lag_break_freq}
\end{equation}
\item Sketch the compensated system and find the error constant to ensure the design specifications were met.
\end{enumerate}

\subsection{Lead-Lag compensators}
To implement a \index{lead-lag compensator}lead-lag compensator simply design the lead, then design the lag (each to meet their respective properties).

\section{Root Locus}
The \index{root-locus}root locus technique allows you to see how poles move as a system variable changes.

\begin{enumerate}
\item Prepare the system for the root locus technique by arranging the denominator of the closed loop transfer function into the following form:
$$1+\gamma G^*(s);$$
where $\gamma$ is the changing variable, and $G^*(s)$ is the system on which you will apply the root-locus technique.
\item Plot the poles and zeros of $G^*(s)$ on an $s$-plane, and define $n$ as the number of poles, and $m$ as the number of zeros.
\item Shade the portion of the $\Re$-axis that belongs to the root-locus.  If the combined number of poles and zeros to the right of any point on the $\Re$-axis is odd then that point belongs to the root-locus.
\item Sketch the \index{asymptotes}asymptotes (there will be $n-m$ of them), they will intercept the $\Re$-axis at $\alpha$ and leave at the angles $\phi_1, \phi_2, \dots \phi_l$; you can find the angles and the intercept using Equations~\ref{eqn:app_asymptotes}~and~\ref{eqn:app_asymptote_intercept}.

\begin{equation}
\phi_l=\frac{180^\circ+360^\circ(l-1)}{n-m}\text{,\quad where }l=1,2,\dots,n-m
\label{eqn:app_asymptotes}
\end{equation}

\begin{equation}
\alpha=\frac{\sum _i \Re\{p_i\}-\sum _j \Re\{z_j\}}{n-m}
\label{eqn:app_asymptote_intercept}
\end{equation}

\item Find the \index{departure angle}departure angle \emph{for each pole} using \eqn{app_depart_angle}, and find the \index{arrival angle}arrival angle \emph{for each zero} using \eqn{app_arrive_angle}.

\begin{equation}
\phi_\text{dep}=\frac{\sum_{j=1}^m\psi_j-\sum_{i=1}^n\phi_i-180^\circ-360^\circ l}{q}\text{,\quad where } l=1,\dots, q
\label{eqn:app_depart_angle}
\end{equation}

\begin{equation}
\psi_\text{arr}=\frac{\sum_{i=1}^n\phi_i-\sum_{j=1}^m\psi_j+180^\circ+360^\circ l}{q}\text{,\quad where } l=1,\dots, q
\label{eqn:app_arrive_angle}
\end{equation}

In the previous equations, $q$ represents the \index{multiplicity}multiplicity of the pole or the zero, multiplicity is the number of poles or zeros that are co-located at a specific location.  Remember that the root-locus is symmetric about the $\Re$-axis, so finding the relevant angles might be redundant for some of the poles or zeros.

\item Find the \index{breakaway points}breakaway points by solving \eqn{app_breakaway_pts} for $s_0$, a strictly real $s_0$ is a valid breakaway point.

\begin{equation}
\frac{d}{ds}\left(-\frac{1}{G^*(s_0)}\right)=0
\label{eqn:app_breakaway_pts}
\end{equation}

\item Sketch the root locus using the information you learned about the system from the previous steps, use a Routh array to find when (at what gain) a pole will cross the $\Im$-axis (causing instability).

\end{enumerate}

\fig{app_complete_rlocus} should help clear up any nomenclature issues you might have.

\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus.pdf}
\caption{Complete Root-Locus}
\label{fig:app_complete_rlocus}
\end{center}
\end{figure}
