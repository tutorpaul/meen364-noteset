\section{Differentiation}
To begin we will discuss a few properties of differentiation.  For this section we will assume that $f$, $g$, and $h$ are all differentiable functions. 

\begin{equation}
\diff{x}(fg)= f\frac{dg}{dx}+g\frac{df}{dx}
\label{eqn:product_rule}
\end{equation}

\begin{equation}
\diff{x}\left(\frac{f(x)}{g(x)}\right)= \frac{g(x)\diff{x}f(x) - f(x) \diff{x}g(x)}{g(x)^2}
\label{eqn:quotient_rule}
\end{equation}

Above are the product rule (\eqn{product_rule}) and quotient rule (\eqn{quotient_rule}), and below is the chain rule.  For the chain rule we will assume that $h(x)=f(g(x))$.
\begin{equation}
\frac{dh(x)}{dx}= \frac{d\,f}{dx}(g(x))\frac{d\,g(x)}{dx}
\label{eqn:chain_rule}
\end{equation}

Practically speaking: if $f(x)=\sin(x)$ and $g(x)=x^2$, then $f(g(x))=\sin(x^2)$.  So to differentiate it is necessary to use chain rule as such:
$$\diff{x}f(x)= \cos(x) \text{, and}$$
$$\diff{x}g(x)=2x\text{; so}$$
$$\diff{x}f(g(x))=2x\cos(x^2)\text{.}$$

\section{Sinusoidal Functions}
There are many properties of sine and cosine that we should know.
\begin{align}
&\sin^2(\theta)+\cos^2(\theta)=1 \\
&\sin(\alpha\pm\beta)=\sin(\alpha)\cos(\beta)\pm\cos(\alpha)\sin(\beta) \\
&\cos(\alpha\pm\beta)=\cos(\alpha)\cos(\beta)\mp\sin(\alpha)\sin(\beta)
\end{align}

\subsection{Half-Angle Formulas}
\begin{align}
&\cos^2(\theta)=\frac{1}{2}(1+\cos(2\theta)) \\
&\sin^2(\theta)=\frac{1}{2}(1-\cos(2\theta))
\end{align}

\subsection{Double-Angle Formulas}
\begin{align}
&\sin(2\theta)=2\sin(\theta)\cos(\theta) \\
&\cos(2\theta)=\cos^2(\theta)-\sin^2(\theta)
\end{align}

\subsection{Product Formulas}
\begin{align}
&\sin(\alpha)\cos(\beta)=\frac{1}{2}\left(\sin(\alpha+\beta)+\sin(\alpha-\beta)\right) \\
&\cos(\alpha)\cos(\beta)=\frac{1}{2}\left(\cos(\alpha+\beta)+\cos(\alpha-\beta)\right) \\
&\sin(\alpha)\sin(\beta)=\frac{1}{2}\left(\cos(\alpha-\beta)-\cos(\alpha+\beta)\right) 
\end{align}

\subsection{Eulerian Formulas}
\newterm{Euler's relation} is given as:
\begin{equation}
e^{\pm j\theta}=\cos(\theta)\pm j\sin(\theta)
\end{equation}
From which we derive some special definitions of sine and cosine.
\begin{equation}
\cos(\theta)=\frac{e^{j\theta}+e^{-j\theta}}{2} 
\end{equation}
\begin{equation}
\sin(\theta)=\frac{e^{j\theta}-e^{-j\theta}}{2j}
\end{equation}


\section{Partial Fraction Decomposition}\label{sec:pfd}\index{partial fraction decomposition}
A proper quotient\footnote{For a quotient of polynomials to be \textsl{proper} the degree of the numerator needs to be less than the degree of the denominator.} of any polynomials can always be expressed as the sum of simpler quotients, the process of finding the simpler quotients is known as partial fraction decomposition.  The process is not difficult but it can be tedious depending on the problem.  In this section we will go through an example to illustrate the general method, and a few formulations to illustrate the special cases that might be encountered.

\subsection{General Method}
Let's consider the following equation.
$$Y(s)=\frac{4s+3}{s^3+4s^2+3s}$$

To perform partial fraction decomposition we first need to factor the denominator as far as we can:
$$Y(s)=\frac{4s+3}{s^3+4s^2+3s}=\frac{4s+3}{s(s+1)(s+3)}\text{.}$$
Once factored we will be able to express our summation of partial fractions with unknown numerators (The special cases discussed in \S\ref{sec:pfd_special} will only affect this step):
$$Y(s)=\frac{4s+3}{s(s+1)(s+3)}=\frac{A}{s}+\frac{B}{s+1}+\frac{C}{s+3}\text{.}$$

In order to find the unknown constants ($A$, $B$, and $C$) we need to find a common denominator for all terms.
$$\frac{4s+3}{s(s+1)(s+3)}=\frac{A(s+1)(s+3)+Bs(s+3)+Cs(s+1)}{s(s+1)(s+3)}$$
Then cancel the denominators on either side of the equal sign.
$$4s+3=A(s+1)(s+3)+Bs(s+3)+Cs(s+1)$$

Now, through algebraic manipulation, we can find the unknown constants.  This is the step that can be tricky, but as long as you expressed your summation of partial fractions correctly it should be entirely possible.  A standard technique for this step is to choose a value for $s$ that will cancel several of the terms.  For this example I will choose $s=0$ to find $A$, $s=-1$ to find $B$, and $s=-3$ to find $C$.
\begin{align*}
s=0 &&\\
4(0)+3&=A(0+1)(0+3)+\cancelto{0}{B(0)(0+3)}+\cancelto{0}{C(0)(0+1)} \\
3&=3A \\
\therefore A&=1
\end{align*}

\begin{align*}
s=-1 && \\
4(-1)+3&=\cancelto{0}{A(-1+1)(-1+3)}+B(-1)(-1+3)+\cancelto{0}{C(-1)(-1+1)}\\
-1&=-2B \\
\therefore B&=\frac{1}{2}
\end{align*}

\begin{align*}
s=-3 && \\
4(-3)+3&=\cancelto{0}{A(-3+1)(-3+3)}+\cancelto{0}{B(-3)(-3+3)}+C(-3)(-3+1)\\
-9&=6C \\
\therefore C&=\frac{-3}{2}
\end{align*}

So, the original expression is known to be equivalent to the following:
$$Y(s)=\frac{4s+3}{s^3+4s^2+3s}=\frac{1}{s}+\frac{1}{2}\frac{1}{s+1}-\frac{3}{2}\frac{1}{s+3}\text{.}$$

\subsection{Special Cases}\label{sec:pfd_special}
So, the previous example showed us how to deal with the simplest factors ($\alpha s+\beta$), but how do we deal with more complicated terms?  All the following examples show how to form the partial fraction for a more difficult polynomial denominator.

For a second order term that cannot be factored, the numerator of the partial fraction should be a first order term instead of the typical zero-order term.
$$Y(s)=\frac{\cdots}{s(s^2+2s+5)}=\frac{A}{s}+\frac{Bs+C}{s^2+2s+5}$$

A repeated term has to be dealt with in the partial fraction as such.
$$Y(s)=\frac{\cdots}{s(s+3)^3}=\frac{A}{s}+\frac{B}{s+3}+\frac{C}{(s+3)^2}+\frac{D}{(s+3)^3}$$

When necessary the above rules should be used together.
$$Y(s)=\frac{\cdots}{s(s^2+2s+5)^2}=\frac{A}{s}+\frac{Bs+C}{s^2+2s+5}+\frac{Ds+E}{(s^2+2s+5)^2}$$

The procedure for solving for constants is the same as the example given earlier.

In certain cases it may be expedient to use the following rule to determine coefficients when a system has repeated roots. Given the following rational expression and its decomposition:

$$Y(s)=\frac{N(s)}{D(s)}=\frac{N(s)}{(s-\alpha)^r(s-\beta)\cdots}=\left\{\frac{A_{\alpha_r}}{(s-\alpha)^r} + \frac{A_{\alpha_{(r-1)}}}{(s-\alpha)^{r-1}} + \cdots + \frac{A_{\alpha_1}}{(s-\alpha)}\right\} + \frac{B}{s-\beta} + \cdots$$

The sequence of coefficients for $A_\alpha$ can be found according to this expression:

$$A_{\alpha_{(r-k)}} = \left\{ \frac{1}{k!} \frac{d^k}{ds^k}\left[(s-\alpha)^r \frac{N(s)}{D(s)}\right] \right\}_{s=\alpha}$$

\section{Logarithms}
All of the properties are equally valid for the natural logarithm ($\ln$).
\begin{equation}
\log\left(\frac{\alpha}{\beta}\right)=\log (\alpha)-\log(\beta)
\end{equation}

\begin{equation}
\log\left(\alpha\beta\right)=\log (\alpha)+\log(\beta)
\end{equation}

\begin{equation}
\log\left(\alpha^\beta\right)=\beta\log\left(\alpha\right)
\end{equation}

\begin{equation}
\log\left(10^\alpha\right)=\alpha \quad\text{or}\quad \ln\left( e^\beta\right)=\beta
\end{equation}

\begin{equation}
10^{\log\left(\alpha\right)}=\alpha \quad\text{or}\quad e^{\ln\left(\beta\right)}=\beta
\end{equation}

\section{Laplace Transform Property Proofs}\label{sec:laplace_props_proof}
The properties of the Laplace Transform given in \tbl{laplace_props} (pg.\ \pageref{tbl:laplace_props}) are proved below.

Before we start let's remember the definition of the Laplace Transform:
$$F (s) = {\cal L} \left\{ f(t)\right\} = \int _0 ^\infty f(t)e^{-st}\, dt\text{.}$$

So anytime we see the right hand side of that equation we are free to replace it with the left hand side or vice-versa as needed.

Likewise if we see some different element on one side of the equation then a corresponding element on the other side should change to match, for example:
$$G (\beta) = {\cal L} \left\{ g(p)\right\} = \int _0 ^\infty g(p)e^{-\beta p}\, dp\text{,}$$
is exactly equivalent to the definition of the Laplace Transform with some symbols changed.

\subsection{Linearity}

We will attempt to prove the following, all that should be required is basic algebraic manipulation and basic understanding of the properties of the integral:
$${\cal L} \left\{ \alpha f(t) + \beta g(t) \right\} = \alpha F(s)+ \beta G(s)$$

\begin{proof}
\begin{align*}
{\cal L} \left\{ \alpha f(t) + \beta g(t) \right\} &= \int _0 ^\infty \left(\alpha f(t) + \beta g(t)\right)e^{-st}\, dt \\
&=\int _0 ^\infty \alpha f(t)e^{-st} + \beta g(t)e^{-st}\, dt \\
&=\int _0 ^\infty \alpha f(t)e^{-st}\, dt + \int _0 ^\infty\beta g(t)e^{-st}\, dt \\
&=\alpha\int _0 ^\infty  f(t)e^{-st}\, dt + \beta\int _0 ^\infty g(t)e^{-st}\, dt \\
&=\alpha{\cal L}\left\{f(t)\right\}+\beta{\cal L}\left\{g(t)\right\} \\
&=\alpha F(s)+\beta G(s)
\end{align*}
\end{proof}

\subsection{Frequency Shift}

Now we will try to prove the following.
$${\cal L} \left\{ f(t)e^{-at}\right\} = F(s+a)$$
We will need to remember how to manipulate exponents.

\begin{proof}
\begin{align*}
{\cal L} \left\{ f(t)e^{-at}\right\} &= \int _0 ^\infty f(t)e^{-at}e^{-st}\, dt \\
&=\int _0 ^\infty f(t)e^{-st-at}\, dt \\
&=\int _0 ^\infty f(t)e^{-(s+a)t}\, dt \\
&= F(s+a)
\end{align*}
\end{proof}

\subsection{Time Shift}

$${\cal L} \left\{ f(t-a) \right\} = e^{-as}F(s)$$

To prove this we will use the concept of $u$-substitution.

\begin{proof}
$${\cal L} \left\{ f(t-a) \right\} = \int _0 ^\infty f(t-a)e^{-st}\, dt $$
Take $u=t-a$, making $t=u+a$ and $du=dt$.  Making this substitution gives the following.
\begin{align*}
\int _0 ^\infty f(t-a)e^{-st}\, dt &= \int _0 ^\infty f(u)e^{-s(u+a)}\, du \\
&=\int _0 ^\infty f(u)e^{-su-as}\, du \\
&=\int _0 ^\infty f(u)e^{-su}e^{-as}\, du
\end{align*}

Since $e^{-as}$ is not a function of $u$ it can be removed from the integral as a constant.
\begin{align*}
\int _0 ^\infty f(u)e^{-su}e^{-as}\, du&=e^{-as}\int _0 ^\infty f(u)e^{-su}\, du \\
&=e^{-as}F(s) \\
\end{align*}
\end{proof}

\subsection{Frequency Differentiation}

$${\cal L}\left\{tf(t)\right\}=\frac{-d}{ds}F(s)$$

\begin{proof}
\begin{align*}
{\cal L}\left\{tf(t)\right\} &= \int _0 ^\infty tf(t)e^{-st}\, dt \\
&= \int _0 ^\infty f(t)te^{-st}\, dt 
\end{align*}
Now consider the term $e^{-st}$, when it is differentiated with respect to $s$ we see something useful.
$$\frac{d e^{-st}}{ds} = -te^{-st}$$
Re-arranging this gives us a useful substitution.
$$te^{-st}=\frac{-d e^{-st}}{ds}$$
Making this substitution give us the following.
$$\int _0 ^\infty f(t)te^{-st}\, dt = \int _0 ^\infty f(t)\frac{-d e^{-st}}{ds}\, dt $$
Since an $s$-derivative is independent of $t$ the $\frac{-d}{ds}$ can be removed from the integral.
\begin{align*}
\int _0 ^\infty f(t)\frac{-d e^{-st}}{ds}\, dt &= \frac{-d }{ds}\int _0 ^\infty f(t)e^{-st}\, dt \\
&=\frac{-d}{ds}F(s)
\end{align*}
\end{proof}

\subsection{Differentiation}
$${\cal L} \left\{ \frac{df(t)}{dt}  \right\} =sF(s)-f(0)$$

To prove this we will need to use integration by parts.

\begin{proof}
\begin{align*}
{\cal L} \left\{ \frac{df(t)}{dt}  \right\} &= \int _0 ^\infty \frac{df(t)}{dt}e^{-st}\, dt \\
&=\int _0 ^\infty  e^{-st}\, df(t)
\end{align*}
Now we can take $u=e^{-st}$ and $dv=df(t)$, making $du=-se^{-st}\,dt$ and $v=f(t)$.
\begin{align*}
\int _0 ^\infty  e^{-st}\, df(t) &= \int _0 ^\infty  u\, dv \\
&=uv|_0 ^\infty -\int _0 ^\infty v\,du \\
&=f(t)e^{-st}|_0 ^\infty - \int _0 ^\infty f(t) (-s)e^{-st}\,dt \\
&=f(t)e^{-st}|_0 ^\infty - (-s)\int _0 ^\infty f(t) e^{-st}\,dt \\
&=\left(f(\infty)e^{-\infty} - f(0)e^{0}\right)+s\int _0 ^\infty f(t) e^{-st}\,dt
\end{align*}
Remember: $e^{-\infty}=0$ and $e^0=1$.
\begin{align*}
\left(f(\infty)e^{-\infty} - f(0)e^{0}\right)+s\int _0 ^\infty f(t) e^{-st}\,dt &= s\int _0 ^\infty f(t) e^{-st}\,dt -f(0) \\
&=sF(s) - f(0)
\end{align*}
\end{proof}

\subsection{Integration}

$${\cal L} \left\{ \int _0 ^t f(\tau)\,d\tau \right\} =\frac{F(s)}{s}$$

We will need to use integration by parts again to prove this property.  It will also be useful to remember that the derivative is the inverse operation of the integral.

\begin{proof}
$${\cal L} \left\{ \int _0 ^t f(\tau)\,d\tau \right\} = \int _0 ^\infty \int _0 ^t f(\tau)\,d\tau e^{-st}\, dt$$
Taking $u=\int _0 ^t f(\tau)\,d\tau$ and $dv=e^{-st}\,dt$, makes $du=f(t)\,dt$ and $v=\frac{-1}{s}e^{-st}$.
\begin{align*}
\int _0 ^\infty \int _0 ^t f(\tau)\,d\tau e^{-st}\, dt &= \int _0 ^\infty u\, dv \\
&=uv|_0 ^\infty -\int _0 ^\infty v\,du \\
&=\left.\left(\int _0 ^t f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{-st}\right)\right|_0 ^\infty -\int _0 ^\infty \frac{-1}{s}e^{-st}f(t)\,dt
\end{align*}

Expanding this gives the following function.
$$\left[\left(\int _0 ^\infty f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{-\infty}\right)-\left(\int _0 ^0 f(\tau)\,d\tau\right)\left(\frac{-1}{s}e^{0}\right)\right] -\frac{-1}{s}\int _0 ^\infty e^{-st}f(t)\,dt $$
If we remember that the value of any definite integral whose upper and lower bounds are the same is, by definition, zero then we will see that:
$$\int _0 ^0 f(\tau)\,d\tau=0\text{,}$$
further we should remember that $e^{-\infty}=0$.  So the above function is simplified down to the following.
$$\frac{1}{s}\int _0 ^\infty e^{-st}f(t)\,dt $$
Which is equivalent to:
$$\frac{F(s)}{s}$$

\end{proof}

\section{Mass Properties}
While this isn't part of the course in general, these are useful things to have.  For the following equations we can reference \fig{rectangle}.

\begin{figure}
\begin{center}
\includegraphics{00_complete_figs/generic_solid.pdf}
\end{center}
\caption{Generic Solid}
\label{fig:rectangle}
\end{figure}

To find the mass of a solid you can use the following:
\begin{equation}
m = \int_V \rho(\vec r ) \,dV
\label{eqn:mass_of_solid}
\end{equation}
Where $\rho(\vec r )$ is the density of the solid at $\vec r$.

The centroid of the solid is expressed as such:
\begin{equation}
\bar x = \frac{1}{m}\int_V x \rho(\vec r )\, dV
\label{eqn:centroid_of_solid_x}
\end{equation}
and
\begin{equation}
\bar y = \frac{1}{m}\int_V y \rho(\vec r )\, dV\text{.}
\label{eqn:centroid_of_solid_y}
\end{equation}

Finally, the mass moment of inertia is found by the following:
\begin{equation}
I = \int_V \rho(\vec r ) |\vec r| ^2\, dV = \int_V \rho(\vec r ) \left(r_x^2  + r_y^2\right)\, dV 
\label{eqn:moment_of_inertia}
\end{equation}
Where $|\vec r |$ is the length of $\vec r$.

\section{Polar Coordinates}
Given the radial and angular positions as a function of time ($r(t)$ and $\theta(t)$, respectively) the velocity and acceleration for a particle in polar coordinates are as follows:
\begin{equation}
\vec{v}  = \dot r(t) \hat\epsilon_r + r(t)\dot\theta(t) \hat\epsilon_\theta
\label{eqn:polar_velocity_components}
\end{equation}

\begin{equation}
\vec{a} = \left(\ddot r(t) -r(t)\dot\theta(t)^2\right)\hat\epsilon_r + \left(r(t)\ddot\theta(t)+2\dot r(t)\dot\theta(t)\right)\hat\epsilon_\theta
\label{eqn:polar_acceleration_components}
\end{equation}






