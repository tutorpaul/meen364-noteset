set terminal pdf monochrome dashed
set output 'lin_approx.pdf'
set size 1,1
set lmargin 6
set rmargin 1
set xrange [-0.25:0.25]
set key top left box
set samples 200
f(x) = x*x*x-1.5*x*x+.75*x+.25
g(x) = .75*x+.25
plot f(x) t 'Actual', g(x) t 'Approximation'