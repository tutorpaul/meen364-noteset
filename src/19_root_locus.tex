\label{topic:rlocus}

\section{Motivation}
As we know, the response of a system is entirely dependent on where the poles and zeros are located; as such, any tool that allows you to easily determine where your poles are can be quite useful in determining how to apply controls.  The \newterm{root-locus} technique for plotting poles allows you to see where the poles of a closed-loop system are and how they will move due to some changing system parameter (usually open loop gain).  What makes the root-locus technique really handy is that you can find the closed-loop pole locations using the open-loop system transfer function.

\section{Sketching the Root-Locus}
Typically, you will be sketching the root-locus for a system like the one shown in \fig{generic_system}, and you will be sketching it with respect to changes in the system gain, $K$.  Given an open-loop system's transfer function, $G(s)$, complete the following steps to sketch its root-locus.

\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/generic_sys.pdf}
\caption{Generic Feedback System}
\label{fig:generic_system}
\end{center}
\end{figure}

\begin{enumerate}
\item The first step is to plot the open-loop poles and zeros of your system on the $s$-plane as was discussed earlier in the course. (\fig{poles_zeros})
\item Working from right to left on the real axis, you will want to shade any part of the axis that has an odd number of poles and zeros to its right.  This might seem odd, but place your pencil at $\Re\{s\}\rightarrow\infty$ and move it to the left.  When your pencil passes over a single pole or zero start (or stop) shading (as you are still moving left); should you pass over a double pole or zero \emph{do not} start (or stop) shading, the same rule applies to a complex pair that is not located on the real-axis.  The shaded area represents the portions of the real-axis where poles will have strictly real values (areas that belong to the root-locus).\label{itm:rlocus_region} (\fig{rlocus_region})
\item Sketch the \newterm{asymptotes} that the poles will follow as $K$ goes to $\infty$.  To find the angle of the asymptotes use \eqn{asymptotes}; where $n$ is the number of poles, $m$ is the number of zeros, and $l$ is an index.  Only $n-m$ of the poles will go to $\infty$, the other $m$ will move to the zeros as $K\rightarrow\infty$.

\begin{equation}
\phi_l=\frac{180^\circ+360^\circ(l-1)}{n-m}\text{,\quad where }l=1,2,\dots,n-m
\label{eqn:asymptotes}
\end{equation}
The asymptotes will intercept the real-axis at $\alpha$, which can be found using \eqn{asymptote_intercept}; where $p_i$ is the $i^\text{th}$ pole and $z_j$ is the $j^\text{th}$ zero. (\fig{rlocus_asymptotes})
\begin{equation}
\alpha=\frac{\sum _i \Re\{p_i\}-\sum _j \Re\{z_j\}}{n-m}
\label{eqn:asymptote_intercept}
\end{equation}

\item As $K$ changes the poles will move, and we need to define the initial angle at which they move, \eqn{depart_angle} is used to find each poles' \newterm{departure angle}.  Remember that as $K$ goes to $\infty$, $m$ of the poles will move toward and eventually intersect with the $m$ zeros (remember, $m$ is the number of zeros).  Thus, we will want to know the angle at which the pole enters the zero, \eqn{arrive_angle} is used to find the \newterm{arrival angle} for each zero.\label{itm:depart_angle}  

\begin{equation}
\phi_\text{dep}=\frac{\sum\psi_j-\sum\phi_i-180^\circ-360^\circ l}{q}\text{,\quad where } l=1,\dots, q
\label{eqn:depart_angle}
\end{equation}

\begin{equation}
\psi_\text{arr}=\frac{\sum\phi_i-\sum\psi_j+180^\circ+360^\circ l}{q}\text{,\quad where } l=1,\dots, q
\label{eqn:arrive_angle}
\end{equation}

In the previous equations, $q$ is the \newterm{multiplicity} of the pole or zero, or the number of poles that are stacked at the current pole's location.  When looking for angles you will start by selecting a pole or zero (you will do this for each pole or zero) and sum the angles from each \emph{other} pole and zero to the current pole (or zero), the $\sum\phi_i$ term represents the sum of the angles from the $i^\text{th}$ pole to the current pole (or zero), and the $\sum\psi_j$ term corresponds, in turn, to the  zeros.  Since the sketches are symmetric about the real-axis you don't need to calculate the arrival or departure angles of single poles or zeros on the real-axis; they will only have one direction they can move in since they can't leave the root-locus region defined in Step~\ref{itm:rlocus_region}. (\fig{rlocus_depart_angle})

\item When two imaginary poles approach the real-axis they will eventually become strictly real and separate from one another on the real-axis, the opposite is true for strictly real poles which approach one another along the real axis.  The points where they touch then separate are known as \newterm{breakaway points}, and they can be found by solving \eqn{breakaway_pts}.\label{itm:breakaway_pts} (\fig{rlocus_breakaway})

\begin{equation}
\frac{d}{ds}\left(\frac{1}{G(s)}\right)_{s=s_0}=0
\label{eqn:breakaway_pts}
\end{equation}

\item Finish the sketch by combining everything you learned about the system from the previous steps.  Then use a routh array to find the gain at which the poles cross the imaginary-axis. (\fig{rlocus_comp})
\end{enumerate}

Step~\ref{itm:breakaway_pts} can be computationally difficult, thus it is beneficial to have a computer handy (perhaps with Maple) to find the breakaway points.  To find the exact root-locus you can use the \texttt{rlocus}\marginpar{\texttt{rlocus}}\index{rlocus@\texttt{rlocus}} command in \matlab.

\section{Root-Locus with Respect to Plant Parameters}
Earlier, I mentioned a specific usage of the root-locus---namely to find the effects of varying the open-loop gain, $K$---however, the technique can be generalized to show the pole locations when changing any parameter.  Looking back at \fig{generic_system} we will see that the closed loop system has the transfer function given in \eqn{generic_sys_cl}.
\begin{equation}
T_\text{cl}(s)=\frac{KG(s)}{1+KG(s)}
\label{eqn:generic_sys_cl}
\end{equation}

Also, we know that the root-locus technique shows us how the closed-loop poles move, and that the poles are the roots of the denominator.  Thus, our root-locus pole locations will always satisfy \eqn{generic_sys_char}, which is known as the \newterm{characteristic equation}.

\begin{equation}
1+KG(s)=0
\label{eqn:generic_sys_char}
\end{equation}

Now, the root-locus is created using only the open-loop transfer function, $G(s)$, right?  So, without losing any generality we can say that any time we have a characteristic equation that looks like the one given in \eqn{generic_sys_char} we can create a root-locus for the gain-like variable. 

If we are given the system defined by \eqn{var_pole}, we can find the closed loop pole movements for $\alpha$ varying between 1 and $\infty$.
\begin{equation}
H(s)=\frac{1}{s(s+\alpha)}
\label{eqn:var_pole}
\end{equation}
This is done by finding the closed-loop transfer function, $T_\text{cl}(s)$:
$$T_\text{cl}(s)=\frac{H(s)}{1+H(s)}=\frac{\frac{1}{s(s+\alpha)}}{1+\frac{1}{s(s+\alpha)}}=\frac{1}{s^2+\alpha s+1}\text{;}$$
extracting the characteristic equation,
$$s^2+\alpha s+1=0\text{;}$$
solving it into the standard form of the characteristic equation:
$$1+\alpha \frac{s}{s^2+1}=1+\alpha H_\alpha(s)=0\text{;}$$
and sketching the root-locus of the new transfer function, $H_\alpha(s)$, which is given as \eqn{var_pole_sol}.
\begin{equation}
H_\alpha(S)=\frac{s}{s^2+1}
\label{eqn:var_pole_sol}
\end{equation}

\section{Example}
Let's sketch the root-locus for the system given as \eqn{example_sys}.
\begin{equation}
G(s)=\frac{s-3}{\left(s^2+6s+25\right)(s+15)}
\label{eqn:example_sys}
\end{equation}
We will start by plotting the poles and zeros on an $s$-plane, which is done in \fig{poles_zeros}.

\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_1.pdf}
\caption{Plot of Poles and Zeros for an Example Sytem}
\label{fig:poles_zeros}
\end{center}
\end{figure}
Next, we will define the region which belongs to the root-locus; in \fig{rlocus_region} this region is the solid black line.
\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_2.pdf}
\caption{Root-Locus Region}
\label{fig:rlocus_region}
\end{center}
\end{figure}
Then, we will find the asymptotes of the system.  In this case $n=3$ and $m=1$, so our asymptotes are $90^\circ$ and $270^\circ$, and by summing the real parts of our poles and zeros we find the intercept to be $-12$.  The asymptotes are shown in \fig{rlocus_asymptotes}
$$\alpha = \frac{-15+2(-3)-3}{2}=\frac{-24}{2}=-12$$
\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_3.pdf}
\caption{Root-Locus Asymptotes}
\label{fig:rlocus_asymptotes}
\end{center}
\end{figure}
The departure angles are found by summing the angle from each of the poles and zeros to the zero or pole in question.

For the pole at $s=-3+4j$:
$$ \overbrace{180^\circ -\tan^{-1}\left(\frac{4}{6}\right)}^{s=3}-\underbrace{90^\circ}_{s=-3-4j}-\underbrace{\tan^{-1}\left(\frac{4}{12}\right)}_{s=-15} -180^\circ +360^\circ=217.9^\circ\text{,}$$
and for the pole at $s=-3-4j$:
$$ 180^\circ +\tan^{-1}\left(\frac{4}{6}\right)+90^\circ-\tan^{-1}\left(\frac{-4}{12}\right) -180^\circ -360^\circ=-577.9^\circ=-217.9^\circ\text{.}$$
The departure angle for the pole at $s=-15$ is simply $0^\circ$, since it cannot go any other direction, and the arrival angle for the zero is $180^\circ$ for the same reason.  Sketching these yields \fig{rlocus_depart_angle}.
\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_4.pdf}
\caption{Root-Locus Departure Angles}
\label{fig:rlocus_depart_angle}
\end{center}
\end{figure}

When we use Maple to solve \eqn{breakaway_pts} for the given system I get two solutions: $s=-5.24$, and $s=-8.40$; which are shown in \fig{rlocus_breakaway}.
\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_5.pdf}
\caption{Root-Locus Breakaway Points}
\label{fig:rlocus_breakaway}
\end{center}
\end{figure}

Finally, we can sketch the complete root-locus (\fig{rlocus_comp}), and find the gain which causes instability.

\begin{center}
\begin{tabular}{ccc}
$s^3$: & $1$ & $115+K$ \\
$s^2$: & $21$ & $375-3K$ \\
$s^1$: & $\frac{21(115+K)-(375-3K)}{21}$ & $0$ \\
$s^0$: & $375-3K$ & $0$ \\
\end{tabular}
\end{center}
So, the limiting value of $K$ for stability will be $K=\frac{375}{3}=125$ (the $s^1$ rows limiting gain value is $K=-85$, which is invalid).

\begin{figure}[ht]
\begin{center}
\includegraphics{19_root_locus_figs/ex_rlocus_comp.pdf}
\caption{Complete Root-Locus for the Example System}
\label{fig:rlocus_comp}
\end{center}
\end{figure}
