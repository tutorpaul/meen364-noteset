\section{Motivation}
During an experiment it is vital that the data you collect is actually representative of what is actually occuring, and most of the time it isn't a problem; however, if your data acquisition system cannot sample fast enough, or you haven't set it up properly your data might experience \newterms{undersampling}{undersample}.  This effect---also known as \newterm{aliasing}---causes one or more frequencies from the source to be misrepresented in the data; for a graphical example of the effects of aliasing see \fig{alias_simple}.  A simple physical example can be seen by watching a car's tire as the car accelerates, the spokes on the wheel will appear to rotate in the reverse direction once the car reaches a certain speed.  This is because the eye (and brain) cannot decipher images moving faster than a certain frequency.
 
\section{Theory}
A logical question to ask is, ``What is the minimum frequency at which I can sample to avoid undersampling?''\ and the answer is easy: You must sample at twice the highest frequency found in the signal (see \eqn{sample_freq}).  The highest frequency in the original signal is referred to as $\omega_M$, the frequency you must exceed in your sampling is known as the \newterm{Nyquist rate}, $2\omega_M$, the frequency at which you actually sample is called the \newterm{sampling frequency}, $\omega_s$, and the highest frequency you can reproduce is the \newterm{Nyquist frequency}, $\nicefrac{\omega_s}{2}$.

\begin{equation}
\omega_s > 2\omega_M\,\,\text{(to avoid aliasing)}
\label{eqn:sample_freq}
\end{equation}


In \fig{alias_simple} a signal with a frequency of $\unit[200]{Hz}$ is sampled correctly ($\unit[600]{Hz}$), and incorrectly ($\unit[375]{Hz}$), notice how undersampling can negatively affect your data quality. The aliased samples can be used to (logically) reconstruct a signal which isn't present in the original signal.  The aliased signal has a frequency of $\unit[175]{Hz}$.  You can calculate the frequency of the aliased signal by dividing the frequencies that will be aliased by the sampling frequency and adding or subtracting the fraction from an integer until the resulting fraction is less than $\nicefrac{1}{2}$.  Note that more than one frequency can be aliased in a signal, thus it is important to apply the following techniques to all frequencies that may be aliased.

\begin{figure}[ht]
\begin{center}
\includegraphics{03_aliasing_figs/aliasing_example1.pdf}
\caption{Correctly and Incorrectly Sampled Signal (Simple)}
\label{fig:alias_simple}
\end{center}
\end{figure}

By now you might ask, ``What if I don't know the highest frequency for which I should be sampling?'' This is a common problem but no worries, you should sample the signal at two significantly different rates and compare frequencies you find---if the frequencies match, you have found the frequencies present in the signal, and can adjust your sampling rate accordingly.  If the frequencies don't match then you should be able to calculate the frequencies present.  You could also ask, ``Why not sample as quickly as possible and get the most accurate depiction of the signal?''  The answer is that doing so would require more expensive computing hardware, and eat up data storage space; furthermore, it isn't generally necessary.  That being said most monitoring systems use sampling frequencies on the order of 30--50 times the highest signal frequency.

\section{Example}
\fig{alias_complex} shows another, slightly more complex signal that is being undersampled.  The original signal is: 
$$f(t)=\sin \left( (2\pi )100 t\right) + \sin \left( (2\pi )450 t\right) \text{.}$$

\begin{figure}[ht]
\begin{center}
\includegraphics{03_aliasing_figs/aliasing_example2.pdf}
\caption{Correctly and Incorrectly Sampled Signal}
\label{fig:alias_complex}
\end{center}
\end{figure}


Let's assume we sampled the previously given signal at $\unit[500]{Hz}$, we know that the signal would experience aliasing because $\omega _M = 450$ and $\nicefrac{500}{2}=250\not > 450$.  We can determine the aliased signal by using \eqn{sample_time}, which expresses the time, $t$, at a given sample, $n \in \mathbb{Z}^+$ ($n$ is in the set of positive integers), based on the sampling frequency, $\omega _s$.  

\begin{equation}
t=\frac{n}{\omega _s}
\label{eqn:sample_time}
\end{equation}

If we sub \eqn{sample_time} into the original signal we will get the following: 
$$f(n)=\sin \left( 2\pi \frac{100}{500} n\right) + \sin \left( 2\pi \frac{450}{500}n\right) \text{.}$$
And, since we know that each of the fractions needs to be less than one half, we will expand the previous to the following.

\begin{eqnarray*}
f(n)&=&\sin \left( 2\pi \frac{1}{5} n\right) + \sin \left( 2\pi \left(1 - \frac{5}{50}\right) n\right) \\
&=&\sin \left( 2\pi \frac{1}{5} n\right) + \sin \left( 2\pi n - 2\pi\frac{1}{10}n\right) \\
&=&\sin \left( 2\pi \frac{1}{5} n\right) + \sin \left(2\pi n\right) \cos \left( 2\pi \frac{1}{10}n\right) -\cos \left( 2\pi n\right) \sin \left( 2\pi\frac{1}{10}n\right)
\end{eqnarray*}

If we think back to our trigonometric identities we will remember Equations~\ref{eqn:sum_angle_sin}~and~\ref{eqn:sum_angle_cos}, which allowed us to perform the last step.  It will also serve you to remember that cosine is even so $\cos(-\gamma)=\cos(\gamma)$, and sine is odd so $\sin(-\gamma)=-\sin(\gamma)$.
\begin{equation}
\sin ( \alpha + \beta ) = \sin ( \alpha ) \cos ( \beta ) + \cos ( \alpha ) \sin (\beta )
\label{eqn:sum_angle_sin}
\end{equation}
\begin{equation}
\cos ( \alpha + \beta ) = \cos ( \alpha ) \cos ( \beta ) - \sin ( \alpha ) \sin (\beta )
\label{eqn:sum_angle_cos}
\end{equation}

Also, we will remember that $\sin (2\pi n) =0$ and $\cos (2\pi n) =1$, since $n$ is an integer we can simplify the previous expression.
$$f(n)=\sin \left( 2\pi \frac{1}{5} n\right) -\sin \left( 2\pi\frac{1}{10}n\right)$$
If we re-use \eqn{sample_time} we can get the aliased continuous signal:
$$f(t)=\sin \left( (2\pi) 100 t\right) -\sin \left( (2\pi) 50 t\right)\text{.}$$ 
