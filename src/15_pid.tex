\section{Motivation}
Up until now we treated the controller as a black box while we discussed the advantages of feedback control.  In this topic we will analyze a common manifestation of a system controller, namely, the \newterm{PID controller}.  The PID controller is the superposition of three independent controllers: the proportional, integral, and derivative controllers.  \fig{desc_pid} shows a typical PID controller, and the remainder of this topic is devoted to promoting your understanding of the PID controller.

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/desc_pid.pdf}
\caption{Example PID Controller}
\label{fig:desc_pid}
\end{center}
\end{figure}

PID control is a simple and quite commonly used approach to stabilize an otherwise unstable system, or make a system more \newterm{robust}.  The primary difficulty in this method of control is in the tuning---or the selection of gains---but don't be discouraged, there are some guidelines to facilitate this process.

\section{Proportional Control}
To facilitate the explaination of controls I'm going to define some new variables on a block diagram, namely, the \newterm{control error}, $E(s)$, and the \newterm{control effort}, $U(s)$.  Both of these new variables can be seen in \fig{desc_p}, which shows a typical proportional controller.  Now let's examine a typical value of the control error: $E(s)=R(s)-Y(s)$.  This means that if the output of the system, $Y(s)$, is equal to the setpoint, $R(s)$, then the error is $0$, if the output is less than setpoint then the error is positive, and if the output is greater than the setpoint then the error is negative.  Further, we should all know that the goal of the controller is to make the output be equal to setpoint, or $E(s)=0$ as $t \rightarrow \infty$.  

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/desc_p.pdf}
\caption{Example Proportional Controller}
\label{fig:desc_p}
\end{center}
\end{figure}

Now let's look at the control effort, $U(s)$, in the case of the proportional controller $U(s)=k_pE(s)$.  If we notice that $Y(s)=U(s)G(s)$ and do some algebra we will find the closed loop transfer function of the system depicted, which is given as \eqn{desc_p}.  So, if we make the \newterm{proportional gain}, $k_p$, very large the control effort becomes quite large, likewise if we make $k_p$ small the control effort is reduced.  

\begin{equation}
\frac{Y(s)}{R(s)}=\frac{k_pG(s)}{1+k_pG(s)}
\label{eqn:desc_p}
\end{equation}

Proportional control will (generally) improve rise time, however this faster response causes a larger overshoot. Generally the error improves (reduces) as $k_p$ is increased, but it is impossible to eliminate error altogether using strictly proportional gain.  Beware: too high a gain can make the system unstable (the reasons for this will become apparent when we talk about the Root-Locus technique). 

\section{Integral Control}
An example proportional-integral (PI) controlled system is shown as \fig{desc_pi}---its transfer function is given as \eqn{desc_pi}. The control effort, $U(s)$, for a PI controller is given as $U(s)=(k_p+\nicefrac{k_i}{s})E(s)$.  What this means is the control effort is proportional to the current error plus a fraction of the integral of the past error, that fraction is defined as the \newterm{integral gain}.\footnote{Remember that the integral of a curve is defined as the area contained under that curve}  Thus, unless the error is steady at zero, the control effort will be changing. Therefore, adding integral control tends to push steady state error to zero, but also increases the settling time of the system as well as the overshoot.  

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/desc_pi.pdf}
\caption{Example Proportional-Integral Controller}
\label{fig:desc_pi}
\end{center}
\end{figure}

\begin{equation}
\frac{Y(s)}{R(s)}=\frac{(k_p+\nicefrac{k_i}{s})G(s)}{1+(k_p+\nicefrac{k_i}{s})G(s)}
\label{eqn:desc_pi}
\end{equation}

\section{Derivative Control}

A derivative (D) controlled system is shown as \fig{desc_d}, and its transfer function is given as \eqn{desc_d}.  Derivative control does just the opposite of integral control, rather than basing the control effort on what the system has done, it tries to predict what the system will do.\footnote{Remember that the derivative of a function represents the slope of that function}  This method of control adds damping to the system, thus improving settling time, and reducing overshoot.  However, derivative control tends to amplify any noise that might be present in a real system so you should be careful to keep the \newterm{derivative gain} low when adding it to real systems.

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/desc_d.pdf}
\caption{Example Derivative Controller}
\label{fig:desc_d}
\end{center}
\end{figure}

\begin{equation}
\frac{Y(s)}{R(s)}=\frac{k_dsG(s)}{1+k_dsG(s)}
\label{eqn:desc_d}
\end{equation}


\section{Proportional-Integral-Derivative Control}    

The full PID controller has the transfer function given as \eqn{desc_pid}.  When \newterm{tuning} your PID gains ($k_p$, $k_i$, and $k_d$) you should bear in mind that the gains are intimately related, and that changing one can have unforeseen effects on the system's overall performance.  \tbl{pid_gains} shows the effects of raising each of the controller gains

\begin{equation}
\frac{Y(s)}{R(s)}=\frac{(k_p+\nicefrac{k_i}{s}+k_ds)G(s)}{1+(k_p+\nicefrac{k_i}{s}+k_ds)G(s)}
\label{eqn:desc_pid}
\end{equation}

\begin{table}[ht]
\begin{center}
\caption{Effects of Increasing PID Gain Values}
\label{tbl:pid_gains}
\begin{tabular}{c|ccccc}
\cline{1-5}
& $t_r$ & $t_s$ & $M_p$ & $e_\text{ss}$ & \\
\cline{1-5}
$\uparrow k_p $ & $ \downarrow\downarrow$ & $\times $ & $ \uparrow $ & $ \downarrow $ & \\
$\uparrow k_i $ & $\times$ & $\uparrow$ & $\uparrow$ & $\downarrow\downarrow$  &\\
$\uparrow k_d $ & $\downarrow$ & $\downarrow\downarrow$ & $\downarrow\downarrow$ & $\times$ & \\ 
\cline{1-5}
\multicolumn{6}{l}{$\times$: no change, $\uparrow$: increase}\\
\multicolumn{6}{l}{$\downarrow$: decrease, $\downarrow\downarrow$: large decrease}\\
\end{tabular}
\end{center}
\end{table}

\section{Study Case}
Let's add a PID controller to the system given in \eqn{ex_sys}.  We will start by adding some proportional gain (increasing $k_p$), \fig{ex_p} shows the change in the response.
\begin{equation}
G(s)=\frac{25}{s^2+3s+25}
\label{eqn:ex_sys}
\end{equation}
\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/example/pid_ex_P.pdf}
\end{center}
\caption{Adding Proportional Control}
\label{fig:ex_p}
\end{figure}

Next we can add some integral gain, which makes the system oscillate much more, and it removes the steady state error (see \fig{ex_i}).
\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/example/pid_ex_I.pdf}
\caption{Adding Integral Control}
\label{fig:ex_i}
\end{center}
\end{figure}
Now, to counteract the integral gain oscillations, we will add some derivative control (damping) to the system, this is shown in \fig{ex_d}.
\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/example/pid_ex_D.pdf}
\caption{Adding Derivative Control}
\label{fig:ex_d}
\end{center}
\end{figure}

In an actual problem you would likely have to spend more time tweaking the gains, however for the sake of example this works.

\begin{figure}[ht]
\begin{center}
\includegraphics{15_pid_figs/example/pid_ex_comb.pdf}
\caption{Composite View of Figs. \ref{fig:ex_p}, \ref{fig:ex_i}, \& \ref{fig:ex_d}}
\label{fig:ex_comb}
\end{center}
\end{figure}