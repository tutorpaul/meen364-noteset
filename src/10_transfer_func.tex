
% What is a transfer function
\section{What is a Transfer Function}
When you excite a linear system at a specific frequency it will respond at that same frequency, but with a different amplitude and phase shift.  A \newterm{transfer function} expresses the relationship between the input and the output as a ratio and is expressed in the frequency, or $s$, domain.  This means that you will oftentimes have to perform a Laplace transformation to find a transfer function.  However, there are cases when you don't need to deal with Laplace transformations, namely when the linear system is in state space form.

% Computing Transfer functions
\section{Computing Transfer Functions}
As previously stated, most of the time you will have to perform a Laplace transform of your linear system of differential equations to find the transfer function.  As such you would do well to remember the properties of the Laplace transform (especially the differentiation property, \eqn{laplace_diff}).  Transfer functions are only able to express the effects of some input to an output, initial conditions of a system cannot be factored in, thus when we apply the Laplace transform we must neglect any initial condition terms. Once you have performed the transformation you will want to solve your equations for the output over the input:
$$H(s)=\frac{\text{output}}{\text{input}}=\frac{Y(s)}{U(s)}\text{.}$$

\begin{equation}
{\cal L} \left\{ \frac{d}{dt} f(t) \right\} =sF(s)-\cancel{f(0)}
\label{eqn:laplace_diff}
\end{equation}

This expression, $H(s)$, should be a function of only $s$, if it is not then you haven't utilized all of your equations properly, go back and simplify some more.

If you are given a system in state space form you can find the transfer function by using \eqn{ss2tf}.  You would also do well to remember that the state space form is defined as such: 
$$\frac{d\vec{x}(t)}{dt}=\mat{A}\vec{x}(t)+\mat{B}u(t)\text{;}$$
$$y(t)=\mat{C}\vec{x}(t)+du(t)\text{.}$$

\begin{equation}
H(s)=\frac{Y(s)}{U(s)}=\mat{C}\left( s\mat{I}-\mat{A}\right)^{-1}\mat{B}+d
\label{eqn:ss2tf}
\end{equation}

You will want to remember how to take the inverse of a $2\times2$ matrix by hand (\eqn{mat_inverse})---use \matlab\ or Maple for larger matrices.  The \texttt{tf} command in \matlab\ is used to generate a transfer function which you can use for analysis.

\begin{equation}
\left[\begin{array}{cc} \alpha & \beta \\ \gamma & \delta \end{array}\right] ^{-1} = \frac{1}{\alpha\delta - \beta\gamma}\left[\begin{array}{cc} \delta & -\beta \\ -\gamma & \alpha \end{array}\right]
\label{eqn:mat_inverse}
\end{equation}

\subsection{Derivation}
The above fact is interesting, but seemingly incomprehensible at first glance.  Let's go through the derivation of \eqn{ss2tf}, and we will be see some implications of this form.  Before we begin it should be noted that $u(t)$ and $y(t)$ take scalar values.
\begin{proof}
\begin{align*}
\frac{d\vec{x}(t)}{dt} &=\mat{A}\vec{x}(t)+\mat{B}u(t) \\ 
\laplace{\frac{d\vec{x}(t)}{dt}} &= \laplace{\mat{A}\vec{x}(t)+\mat{B}u(t)} \\
s\vec{X}(s) - x(0) &= \mat{A}\vec{X}(s) + \mat{B} U(s) \\
s\vec{X}(s) - \mat{A}\vec{X}(s) &=  \mat{B} U(s) + x(0) 
\end{align*}
At this point we are able to factor out the $\vec{X}(s)$ from the left hand side, when we do that we get a term like $\vec{X}(s)\left(s - \mat{A}\right)$ which isn't possible since $s$ is a scalar and $\mat{A}$ is a matrix.  However if we re-interpret the left hand side we can alleviate this issue, including an identity matrix, $\mat{I}$, we proceed.
\begin{align*}
s\mat{I}\vec{X}(s) - \mat{A}\vec{X}(s) &=  \mat{B} U(s) + x(0) \\
\left(s\mat{I} - \mat{A}\right)\vec{X}(s) &= \mat{B} U(s) + x(0) \\
\vec{X}(s) &= \left(s\mat{I} - \mat{A}\right)^{-1}\left(\mat{B}U(s) + x(0)\right)
\end{align*}
Remembering the output equation:
\begin{align}
y(t) &= \mat{C}\vec{x}(t) + d u(t) \nonumber \\
\laplace{y(t)} &=\laplace{\mat{C}\vec{x}(t) + d u(t)} \nonumber \\
Y(s) &= \mat{C}\vec{X}(s) + dU(s) \nonumber \\
&= \mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}\left(\mat{B}U(s) + x(0)\right) + dU(s) \nonumber \\
Y(s)&= \mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}  \mat{B}U(s) +dU(s) + \mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}x(0) \label{eqn:ss2tf_pivitol} 
\end{align}
If we now assume zero for our initial conditions:
\begin{align*}
Y(s)&= \mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}  \mat{B}U(s) +dU(s) + \mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}\cancelto{0}{x(0) } \\
&= \left(\mat{C}\left(s\mat{I} - \mat{A}\right)^{-1}  \mat{B} +d\right)U(s) \\
\frac{Y(s)}{U(s)} &= \mat{C}\left( s\mat{I}-\mat{A}\right)^{-1}\mat{B}+d
\end{align*}
\end{proof}

Should we need to calculate the system response to some input and initial conditions we can use \eqn{ss2tf_pivitol}.

% Finding Poles and Zeros
\section{Finding Poles and Zeros}
As was discussed earlier, a transfer function is expressed as a fraction of two polynomial functions of $s$, as shown in \eqn{std_tf}.
\begin{equation}
H(s)=\frac{b_ms^m+b_{m-1}s^{m-1}+\cdots +b_0}{s^n+a_{n-1}s^{n-1}+\cdots +a_0}
\label{eqn:std_tf}
\end{equation}

Now, if you factor the numerator and denominator you can put the transfer function into the form given as \eqn{pole_zero_form}, which is known as the \newterm{pole-zero form}.
\begin{equation}
H(s)=b_m\frac{\prod _{i=1}^m (s-z_i)}{\prod _{j=1}^n (s-p_j)}
\label{eqn:pole_zero_form}
\end{equation}

In \eqn{pole_zero_form} $z_i$ is the $i^\text{th}$ \newterm{zero}, and $p_j$ is the $j^\text{th}$ \newterm{pole}.  In summation, you can find the zeros by factoring the transfer function's numerator , and the poles by factoring the denominator.  In this course a transfer function will typically have more poles than zeros, that is $n>m$.

% Example
\section{Example}
Let's consider the following system of equations---which model the system given in \fig{example_sys_tf}---taking the output to be $V_C$, and the input to be $V_\text{in}$ 
$$V_\text{in}(t)-R_1I_L(t)-L\dot I_L(t) -V_C(t)=0\text{;}$$
$$I_L(t) -\frac{V_C(t)}{R_2}-C\dot V_C(t)=0\text{.}$$

\begin{figure}[ht]
\begin{center}
\includegraphics{10_transfer_func_figs/example_problem.pdf}
\caption{Example Electrical Systems}
\label{fig:example_sys_tf}
\end{center}
\end{figure}
The first thing we should do is take the Laplace transform of each equation, which should look like this
$$V_\text{in}(s)-R_1I_L(s)-L\left(sI_L(s)-\cancel{I_L(0)}\right)-V_C(s)=0\text{;}$$
$$I_L(s)-\frac{V_C(s)}{R_2}-C\left(sV_C(s)-\cancel{V_C(0)}\right)=0\text{.}$$
We want to get rid of all the terms that aren't the input or output.  To do that we will solve the second equation for $I_L(s)$
$$I_L(s)=\frac{V_C(s)}{R_2}+sCV_C(s)\text{.}$$
Then substitute it into the first equation, yielding
$$V_\text{in}(s)-R_1\left(\frac{V_C(s)}{R_2}+sCV_C(s)\right) -sL\left(\frac{V_C(s)}{R_2}+sCV_C(s)\right)-V_C(s)=0\text{.}$$
Okay, now we will factor out all the output terms, $V_C(s)$, and move the input term(s), $V_\text{in}(s)$, to the other side
$$\left[ R_1\left(\frac{1}{R_2}+sC\right) +sL\left(\frac{1}{R_2}+sC\right)+1\right] V_C(s)=V_\text{in}(s)\text{.}$$
This simplifies down to
$$\left[ LCs^2+\left(R_1C+\frac{L}{R_2}\right)s+\left(1+\frac{R_1}{R_2}\right) \right] V_C(s)=V_\text{in}(s)\text{.}$$
Which can be rearranged into the standard transfer function
$$H(s)=\frac{Y(s)}{U(s)}=\frac{V_C(s)}{V_\text{in}(s)}=\frac{1}{LCs^2+\left(R_1C+\frac{L}{R_2}\right)s+\left(1+\frac{R_1}{R_2}\right)} \text{.}  $$
