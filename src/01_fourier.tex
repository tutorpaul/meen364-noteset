%\chapter{Fourier Analysis}
\section{Motivation}

The tools used in Fourier analysis are a useful set of equations that allow you to visualize and reproduce a signal.  This lecture will show you how to perform Fourier analysis, but more importantly it will tell you why you would want to use Fourier analysis.

\section{Theory}
In vector geometry there are many ways to define a vector, polar notation requires the vector length and the angle from a known reference.  It is just as easy to define the same vector by giving the coordinates of its endpoint (see \fig{vec_def}), but for this definition to be meaningful we must all agree on a set of \newterm{unit vectors}, which are a set of orthogonal vectors with length 1 (together these conditions are referred to as orthonormality).  Through this method we are able to define vectors with an unlimited number of dimensions, all we have to do is define an orthonormal \newterm{basis}.  There is a similar method for defining functions which is the focus of this topic.

\begin{figure}
\begin{center}
\includegraphics{01_fourier_figs/vec_def}
\caption{Two Ways to Define a Vector on a Cartesian Plane.}
\label{fig:vec_def}
\end{center}
\end{figure}

Just as we can decompose a vector down to its components it is possible to do the same with a function, we just need to have an orthonormal basis.  This is where the sine and cosine function come in.  If we recognize that $\sin(x)$, $\sin(2x)$, $\cos(x)$, and $\cos(2x)$ are orthogonal with one another then we can take a bigger step and notice that $\sin(nx)$ is orthogonal with $\sin(mx)$ when $n\neq m$ (same is true for cosine).  Further we can note that the ``length'' of each of those functions is one.\footnote{To prove this one should use the vector space defined by all continuous functions on $[-\pi,\pi]$ with the following norm: $\langle f,g\rangle = \int_{-\pi}^{\pi} f(x)g(x)\,dx$}  Since the hard working mathematicians of the past have rigorously proven the above we are able to use the sine and cosine function as a basis for describing functions, in much the same was as $\hat{i}$, $\hat{j}$, and $\hat{k}$ are used to describe 3-dimensional vectors.

Fourier analysis fully exploits this idea in that it allows us to decompose a signal down to its constituent sinusoidal signals.

\section{Fourier Transforms}

\newterm{Fourier transform}\emph{s} are used to decompose a signal into its frequency components, an example of what this really means can be seen in \fig{ttf_example} where you will see a signal with a manifestation of its Fourier transform to the right.  Notice how the single sine wave, \fig{ttf_example}a, appears as a single spike on the FFT (\newterms{Fast Fourier Transform}{fast Fourier transform}) plot, the superposition (summation) of two sine waves, \fig{ttf_example}b, appears as two spikes, and the noisy superposition of 6 sine waves, \fig{ttf_example}c, appears as 6 large spikes and several smaller bumps (random noise).\footnote{Not all of the spikes can be seen because they are not large enough, or too high frequency}

\begin{figure}[ht]
\begin{center}
\includegraphics[width=\textwidth]{01_fourier_figs/fft_example.pdf}
\caption{Several Signals (left) and Their Fourier Transforms (right)}
\label{fig:ttf_example}
\end{center}
\end{figure}

To actually perform a Fourier transform you simply need to perform an integral or summation, depending on the type of signal to be analyzed.  If the signal is continuous use \eqn{ft_cont}, if it is discrete use \eqn{ft_disc}.  Note that the Fourier transformation is usually only meaningful for real (measured) signals.

\begin{equation}
F(\omega )= \int _{-\infty} ^{\infty} f(t)e^{-j \omega t}\,dt
\label{eqn:ft_cont}
\end{equation}

\begin{equation}
X(\omega)= \sum _{n=-\infty} ^{\infty} x(n) e^{-j \omega n}
\label{eqn:ft_disc}
\end{equation}

\subsection{Mathematical Aside}

It would behoove you to remember a few mathematical identities, namely the integral of an exponential, \eqn{exp_int}, and the equivalent value of a geometric series, \eqn{geometric_series} (which is only valid when $|r|<1$).

\begin{equation}
\int e^{\alpha x}\,dx=\frac{e^{\alpha x}}{\alpha}+c
\label{eqn:exp_int}
\end{equation}

\begin{equation}
\sum _{n=0} ^{\infty}ar^n=a+ar+ar^2+\cdots +ar^{\infty}=\frac{a}{1-r}
\label{eqn:geometric_series}
\end{equation}

Furthermore, it might be helpful to remember how to manipulate a geometric series, so pay close attention.

$$\sum _{n=0} ^{\infty}ar^{-n}=\frac{ar}{-1+r}$$

\begin{eqnarray*}
\sum _{n=1} ^{\infty}ar^n &=& ar+ar^2+\cdots +ar^{\infty}\\
&=& \sum _{n=0} ^{\infty}\left(ar^n\right)-a\\
&=& \frac{a}{1-r}-a\\
&=& \frac{a-a(1-r)}{1-r}\\
&=& \frac{ar}{1-r}
\end{eqnarray*}

$$\sum _{n=-\infty} ^{-1}ar^{-n}=ar^{\infty}+\cdots +ar^2+ar=\sum _{k=1} ^{\infty}ar^{k}$$

You will surely need to remember integration by parts---which is just the inverse of the product rule---as well, so it is given as \eqn{int_parts}.

\begin{equation}
\int_a^b u\,dv = uv|_a^b-\int_a^b v\,du
\label{eqn:int_parts}
\end{equation}

\section{Fourier Series Expansion}

Any periodic signal can be represented exactly by an infinite series of sine and cosine waves which are superpositioned together.  You can use \newterm{Fourier series expansion} to find the coefficients of each of the sine and cosine terms.  If you want a graphical depiction of what a Fourier series actually is, just take a look at \fig{fse}, where you will see a square wave and its first, third, fifth, and tenth order Fourier series. The curves are unlabeled, but the higher order series are the closer approximations to the original.  Like the Fourier transform, there are two methods to find the Fourier series coefficients, the continuous time (Equation~\ref{eqn:fse_cont}), and the discrete time (\eqn{fse_disc}).  Equations~\ref{eqn:fse_series_cont}~and~\ref{eqn:fse_series_disc} show you how to express the Fourier series for either the continuous or discrete, note that $x_\text{approx}$ will always be a continuous time function.

When performing Fourier analysis you should resist the urge to use \newterm{Euler's relation} (\eqn{eulers_relation}) to convert exponentials to sinusoids, as that will only make the problem more difficult.  If possible you should convert sinusoids to exponentials as that will make the calculus easier.  It is also common for students to want to assume that the imaginary part of an expression is meaningless, \emph{do not} fall victim to this blunder; the imaginary part is meaningful, do not eliminate it.

\begin{equation}
e^{jx}=\cos(x)+j\sin(x)
\label{eqn:eulers_relation}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{01_fourier_figs/fse_example.pdf}
\caption{Square Wave and Several Fourier Series Expansion Approximations}
\label{fig:fse}
\end{center}
\end{figure}

\begin{equation}
a_k=\frac{1}{T_0}\int_0^{T_0}x(t)e^{-jk\omega _0t}\,dt
\label{eqn:fse_cont}
\end{equation}

In the previous equation, $T_0$ represents the fundamental period of the signal (the time for a complete cycle), and $\omega_0=\frac{2\pi}{T_0}$.

\begin{equation}
x_\text{approx}(t)=\sum_{k=-\infty}^{\infty}a_ke^{jk\omega _0 t}
\label{eqn:fse_series_cont}
\end{equation}


\begin{equation}
a_k=\frac{1}{N}\sum_{n=0}^{N-1}x(n)e^{-2j\pi k \frac{n}{N}}
\label{eqn:fse_disc}
\end{equation}

In the previous equation, $N$ is the total number of data points required to complete a single period.

\begin{equation}
x_\text{approx}(t)=\sum_{k=-\infty}^{\infty}a_ke^{2j\pi k \frac{t}{N}}
\label{eqn:fse_series_disc}
\end{equation}

\section{Example}
Let's find the first five Fourier series coefficients for the signal given in \fig{fse_problem}.

\begin{figure}[ht]
\begin{center}
\includegraphics{01_fourier_figs/fse_problem.pdf}
\caption{Example Discrete Periodic Signal}
\label{fig:fse_problem}
\end{center}
\end{figure}

The first thing we need to do is determine the fundamental period, $N$, as we see there are $6$ samples which make up the full pattern thus $N=6$.  It will be useful to define a period of our signal numerically, as is done in \tbl{fse_problem}.
\begin{table}[ht]
\begin{center}
\caption{Definition of the Example Signal}
\label{tbl:fse_problem}
\begin{tabular}{cc}
\hline
Sample & Value \\
\hline
$0$ & $0$ \\
$1$ & $2$ \\
$2$ & $4$ \\
$3$ & $0$ \\
$4$ & $-4$ \\
$5$ & $-2$ \\
\hline
\end{tabular}
\end{center}
\end{table}

Then, we will apply \eqn{fse_disc}:
\begin{eqnarray*}
a_k &=& \frac{1}{6}\left(0e^{-2j\pi k\frac{0}{6}}+2e^{-2j\pi k\frac{1}{6}}+4e^{-2j\pi k\frac{2}{6}}+0e^{-2j\pi k\frac{3}{6}}-4e^{-2j\pi k\frac{4}{6}}-2e^{-2j\pi k\frac{5}{6}}\right) \\
&=& \frac{1}{3}\left(e^{-j\pi k\frac{1}{3}}+2e^{-j\pi k\frac{2}{3}}-2e^{-j\pi k\frac{4}{3}}-e^{-j\pi k\frac{5}{3}}\right). \\
\end{eqnarray*}

The problem asked us to find the first five Fourier series coefficients thus we must find $a_1$ through $a_5$, and for the sake of completeness we will find $a_0$.  To find the coefficients we will substitute the an integer (0--5) denoting the order for $k$.  \tbl{fse_problem_coefficients} shows the coefficients which were found using \matlab, all the coefficients are of the form $\alpha-\beta j$ where the $\alpha$ term is the coefficient for the cosine term and the $\beta$ is the coefficient of the sine term (if $\beta$ is negative then the sine coefficient is positive).

\begin{table}[ht]
\begin{center}
\caption{Fourier Series Coefficients for the Example Signal}
\label{tbl:fse_problem_coefficients}
\begin{tabular}{cc}
\hline
Order & Value \\
\hline
$0$ & $0$ \\
$1$ & $0-1.732j$ \\
$2$ & $0+0.577j$ \\
$3$ & $0$ \\
$4$ & $0-0.577j$ \\
$5$ & $0+1.732j$ \\
\hline
\end{tabular}
\end{center}
\end{table}

Using the coefficients to find the approximate signal yields the following:
$$x_\text{approx}(t)=1.732\sin(\frac{\pi t}{3})-0.577\sin(\frac{2\pi t}{3})+0.577\sin(\frac{4\pi t}{3})-1.732\sin(\frac{5\pi t}{3}).$$
Which, when plotted, looks like \fig{fse_problem_soln}; of course, if we were to increase the order of approximation then the fit would be closer.

\begin{figure}[ht]
\begin{center}
\includegraphics{01_fourier_figs/fse_problem_soln.pdf}
\caption{Fifth Order Approximation of the Example Signal}
\label{fig:fse_problem_soln}
\end{center}
\end{figure}
