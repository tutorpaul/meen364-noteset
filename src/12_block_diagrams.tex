\section{Motivation}
\newterms{Block diagrams}{block diagrams} are an easy way to represent \newterm{feedback} loops, which will be used for the remainder of the course.  \fig{block_examples} shows several example block diagrams which I will use in these notes to describe strategies to analyze and reduce block diagrams.

\begin{figure}[ht]
\begin{center}
\includegraphics{12_block_diagrams_figs/block_examples.pdf}
\caption{Example Block Diagrams}
\label{fig:block_examples}
\end{center}
\end{figure}

\section{Reducing Simple Blocks}
There are a set of tricks to reduce simple feedback loops.  \fig{block_examples}a represents the simplest negative feedback loop possible, and it can be reduced to the following simple transfer function:
$$\frac{Y(s)}{U(s)}=\frac{G_1}{1+G_1} \text{.}$$
The transfer function is defined in two parts, the numerator and the denominator.  The numerator is the product of everything in the direct line from input to output.  The denominator is one minus everything in the entire feedback loop (the minus sign on the summation block changes that to positive). So, using the same logic, \fig{block_examples}b  can be reduced to:
$$\frac{Y(s)}{U(s)}=\frac{G_1G_2}{1+G_1G_2G_3} \text{,}$$
and \fig{block_examples}c reduces to:
$$\frac{Y(s)}{U(s)}=\frac{KG_1G_2}{1-G_1G_2G_3} \text{.}$$

\section{Reducing Complex Blocks}
It is easiest to use the aforementioned method when you are only dealing with single feedback loops.  However, when you are dealing with a block diagram, like the one given in \fig{complex_block}, the simple method can become tedious to apply.  
\begin{figure}[ht]
\begin{center}
\includegraphics{12_block_diagrams_figs/complex_block.pdf}
\caption{A Complex Block Diagram}
\label{fig:complex_block}
\end{center}
\end{figure}
As such, it is often easier to apply an algebraic approach described presently.  We will start by naming several of the points within the block diagram, which I have done in \fig{complex_block_ann} ($A$ and $B$). 
\begin{figure}[ht]
\begin{center}
\includegraphics{12_block_diagrams_figs/complex_block_ann.pdf}
\caption{An Annotated Complex Block Diagram}
\label{fig:complex_block_ann}
\end{center}
\end{figure}
Then we will define a series of algebraic equations that define each of the named points (and the output) in terms of the input, output, or other named points; I have defined these as Equations~\ref{eqn:block_algA}--\ref{eqn:block_algY}. 

\begin{equation}
A=U(s)G_1-Y(s)G_6
\label{eqn:block_algA}
\end{equation}

\begin{equation}
B=A+AG_2-Y(s)G_5=A\left( 1+G_2 \right) -Y(s)G_5
\label{eqn:block_algB}
\end{equation}

\begin{equation}
Y(s)=AG_3+BG_4
\label{eqn:block_algY}
\end{equation}

Substituting \eqn{block_algB} into \eqn{block_algY} and simplifying should yield \eqn{block_algY2}.

\begin{equation}
Y(s)=A(G_2G_4+G_3+G_4)-Y(s)G_4G_5
\label{eqn:block_algY2}
\end{equation}

Substituting \eqn{block_algA} into this will give us \eqn{block_algY3}.

\begin{equation}
Y(s)=U(s)(G_1G_2G_4+G_1G_3+G_1G_4)-Y(s)(G_2G_4G_6+G_3G_6+G_4G_5+G_4G_6)
\label{eqn:block_algY3}
\end{equation}

Finally, you can solve this equation down to the standard form for transfer functions (\eqn{block_soln}).  

\begin{equation}
\frac{Y(s)}{U(s)}=\frac{G_1G_2G_4+G_1G_3+G_1G_4}{1+G_2G_4G_6+G_3G_6+G_4G_5+G_4G_6}
\label{eqn:block_soln}
\end{equation}
