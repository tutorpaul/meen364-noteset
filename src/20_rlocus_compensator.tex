\section{Motivation}
We know from a previous topic that pole and zero locations dictate the system response, and we learned how to predict pole location changes using root-locus techniques.  Now we will discuss a method that will allow us to change system (and therefore its response) to suit our needs using the graphical technique of the root-locus.

\section{Lead Compensators}
A lead compensator consists of a strategically placed zero, and a pole that is faster (farther from the $\Im$-axis) and also strategically placed.  Rather than looking at the form given in the compensation topic we will use the pole-zero form given as \eqn{pz_lead}.  The desired system gain will be found using the root-locus.
\begin{equation}
D(s)=\frac{s+z}{s+p}\text{,\quad where }p>z
\label{eqn:pz_lead}
\end{equation}

The idea is to plot the uncompensated system's root-locus then, after choosing where to place the compensator zero some geometry will tell us where to place the pole based on the performance criteria we desire.  Usually the compensator zero will be best placed near one of the system poles, and the compensator pole must be placed farther out on the negative $\Re$-axis.  \fig{lead_compensators}a shows a simple system which is defined in \eqn{example_sys_rlocus}, the compensators used in \fig{lead_compensators}b--e are given in \tbl{lead_compensators}.

\begin{equation}
G(s)=\frac{s+3}{s(s^2+2s+5)}
\label{eqn:example_sys_rlocus}
\end{equation}

\section{Lead Compensator Example}
Suppose we were given a transfer function like, $G(s)$, and told to design a lead compensator, $C(s)$, such that the closed loop system met some performance criteria, such as $\omega_n = \unitfrac[8]{rad}{sec}$ and $\zeta = 0.4$.
\[G(s) = \frac{1}{(2s+1)(s+4)}\]
Let's start by looking at the root locus of the uncompensated system. Since root loci are symmetric about the real axis we only need to look at the positive half of the imaginary axis, \fig{ex_rlocus_lead_uncomp} shows the root locus of $G(s)$.

\begin{figure}[h!]
\centering
\includegraphics{20_rlocus_compensator_figs/analytical/ex_rlocus_lead_uncomp.pdf}
\caption{Root Locus of Uncompensated System}
\label{fig:ex_rlocus_lead_uncomp}
\end{figure}

Now we can consider the constraints we want the compensated system to meet; we draw a circle about the origin of radius 8 to represent the desired natural frequency, and the radial line to represent the desired damping ratio. \fig{ex_rlocus_lead_const} shows these constraints and their intersection at $s=-3.2\pm 7.33j$. That intersection point is precisely where we want our compensated system poles to fall.

\begin{figure}[h!]
\centering
\includegraphics{20_rlocus_compensator_figs/analytical/ex_rlocus_lead_const.pdf}
\caption{Root Locus of Uncompensated System with Constraint Set Superimposed}
\label{fig:ex_rlocus_lead_const}
\end{figure}

The lead compensator that we are implementing is simply a pole and a zero. We can choose the location of zero and calculate the location of the pole by considering the so-called \newterm{subtended angles} of the poles and zeros of the system. For this example we will choose the location of the new zero as $z=5$, the new pole will be farther from the origin at a position labeled $p$. The extra pole and zero and the relevant angles are depicted in \fig{ex_rlocus_lead_subtend}.

\begin{figure}[h!]
\centering
\includegraphics{20_rlocus_compensator_figs/analytical/ex_rlocus_lead_subtend_mod.pdf}
\caption{A View of the Angles from Poles and Zeros to the Desired Pole Location}
\label{fig:ex_rlocus_lead_subtend}
\end{figure}

We must choose $\theta_p$ such that the following equation is satisfied: \[\sum \theta - \sum \phi = 180^\circ\]. 
\[\theta_1 = 180 - \arctan \left(\frac{3.2-0.5}{7.33}\right) = 159.8^\circ \]
\[\theta_2 = \arctan \left(\frac{4-3.2}{7.33}\right) = 6.2^\circ\]
\[\phi_z = \arctan \left(\frac{5-3.2}{7.33}\right) = 13.8^\circ\]

\begin{align*}
\theta_1 + \theta_2 + \theta_p - \phi_z &= 180^\circ \\
159.8^\circ + 6.2^\circ + \theta_p - 13.8^\circ &= 180^\circ \\
\theta_p &= 27.8^\circ
\end{align*}
\begin{align*}
\theta_p &= \arctan\left(\frac{p-3.2}{7.33}\right) \\
7.33\tan(\theta_p)+3.2 &= p \\
7.1 &= p
\end{align*}

When we place the pole of the compensator at $p = 7.1$, as was done in \fig{ex_rlocus_lead_comp}, we find that the trajectory of the dominant poles passes through the desired region. Now we have to determine the gain, $k$, that will place the dominant pole at the desired location.

\begin{figure}[h!]
\centering
\includegraphics{20_rlocus_compensator_figs/analytical/ex_rlocus_lead_comp.pdf}
\caption{Root Locus of Compensated System}
\label{fig:ex_rlocus_lead_comp}
\end{figure}

As we learned in a previous topic (Topic \ref{topic:rlocus}) the root locus shows where the poles fall for all positive gain values, in other words the plot shows the roots of the denominator of the closed loop system ($1+kG(s)C(s)=0$). If we substitute the desired pole location into that denominator as $s$ then we should be able to solve for $k$. This, however, is analyticially troublesome in many cases due to the fact that we don't know \emph{exactly} where the poles will go.\footnote{In the sense that $3.2 \pm 7.33j$ is distinctly different from $3.2001 \pm 7.3302j$} Instead we can use \matlab\ to narrow in on the appropriate gain value. In this case the value is $k=127$.

%\begin{align*}
%1+kG(s)C(s) &= 0 \\
%1+k\left(\frac{1}{(2s+1)(s+4)}\right)\left(\frac{s+5}{s+7.1}\right) &=0 \\
%k \frac{s+5}{(2s+1)(s+4)(s+7.1)} &= -1 \\
%k &= -\left.\frac{(2s+1)(s+4)(s+7.1)}{s+5}\right|_{s=3.2+7.33j} \\
%\end{align*}

\begin{figure}[ht]
\centering
\includegraphics{20_rlocus_compensator_figs/lead_rlocus.pdf}
\caption{Example System's Root Locus with and without Lead Compensation}
\label{fig:lead_compensators}
\end{figure}

\begin{table}[ht]
\begin{center}
\caption{Lead Compensators Corresponding to those in \fig{lead_compensators}}
\label{tbl:lead_compensators}
\begin{tabular}{cc}
\hline
Figure Number & Compensator \\
\hline
\ref{fig:lead_compensators}b & $D(s)=\frac{s+1}{s+10}$ \\
\ref{fig:lead_compensators}c & $D(s)=\frac{s+1}{s+20}$ \\
\ref{fig:lead_compensators}d & $D(s)=\frac{s+1}{s+30}$ \\
\ref{fig:lead_compensators}e & $D(s)=\frac{s+2}{s+30}$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\section{Lag Compensators}

After adding a lead compensator you can usually get a pretty good response, however sometimes the steady-state performance is lacking.  You can usually overcome these shortcomings by adding a lag compensator.  You will want to add the compensator near the $\Im$-axis so it won't affect the system's response significantly.  \fig{lag_compensator}a shows a system with a lead compensator (this is the same as \fig{lead_compensators}b), and \fig{lag_compensator}b shows the same system with a lag compensator added (there is a zoomed section to show the pole movement near the $\Im$-axis).  The lag compensator that was added is given as \eqn{lag_compensator_rlocus}.

\begin{equation}
D_\text{lag}=\frac{s+0.1}{s+0.01}
\label{eqn:lag_compensator_rlocus}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{20_rlocus_compensator_figs/lag_rlocus_tot.pdf}
\end{center}
\caption{Example System's Root Locus with and without Lag Compensation}
\label{fig:lag_compensator}
\end{figure}

Notice that the overall trajectory of the poles doesn't change much, that isn't what a lag compensator does. The effect of the lag compensator is seen in the time response of the system as it approaches steady state. 