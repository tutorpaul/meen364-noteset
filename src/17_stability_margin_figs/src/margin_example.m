K_0=tf(3,1);

A=tf([1],1);
B=tf(1,[1 0]);
C=tf(1,[1 2 5]);

tot=K_0*A*B*C;

%rlocus(tot)
[magT,phaseT,w]=bode(tot);

margin(tot);
%[magK,phaseK]=bode(K_0,w);
%[magA,phaseA]=bode(A,w);
%[magB,phaseB]=bode(B,w);
%[magC,phaseC]=bode(C,w);
%
%magK=squeeze(magK);
%magA=squeeze(magA);
%magB=squeeze(magB);
%magC=squeeze(magC);
%magT=squeeze(magT);
%
%phaseK=squeeze(phaseK);
%phaseA=squeeze(phaseA);
%phaseB=squeeze(phaseB);
%phaseC=squeeze(phaseC);
%phaseT=squeeze(phaseT);

fid= fopen('ex_bode.dat','wt');

fprintf(fid,'#    freq    gain(tot)   phase(tot)\n');
for i=1:length(w)
    fprintf(fid,'%12.4E %12.4E %12.4E\n',...
        w(i),20*log10(magT(i)),phaseT(i));
end
fclose(fid);