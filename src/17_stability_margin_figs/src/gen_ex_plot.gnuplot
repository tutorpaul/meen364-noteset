set terminal fig
set output 'ex_plot_phase.fig'
set size 1,0.5
set lmargin 6
set rmargin 1
set ylabel 'Phase (degree)'
set xlabel 'Frequency (rad/sec)'
set logscale x
set format x "10^%L"
set nokey

plot "ex_bode.dat" u 1:3 w lines 1
