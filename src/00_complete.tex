\documentclass[notes]{notes}
\usepackage[latin1]{inputenc}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}   %allows the use of \therefore among other things
\usepackage{amsfonts}  %use \mathbb{< R | Z | I | etc. >} for blackboard bold letters
\usepackage{graphicx}  %use \includegraphics[width=x in]{figure.pdf}
\usepackage{rotating}
%\usepackage{fancyhdr}
\usepackage[compact]{titlesec}
\usepackage{lastpage}
\usepackage{units}         %use \unit[qty]{unit}
\usepackage{nicefrac}   %use \nicefrac{num}{den}
\usepackage{mparhack}
\usepackage{cancel}
%\usepackage{showkeys}   %use to show ref names in document
\usepackage{makeidx}     % use `makeindex file.idx` in a shell to generate index
\usepackage{fancyvrb}
\usepackage{theorem}   % \begin{proof} ... \end{proof}
\makeindex

%change all the occurrences of \textit to \emph (including \newterm), it looks better
%\centering is preferred to \begin{center} ... \end{center}

\author{Paul Stiverson}
\title{Dynamic Systems and Control Notes}
\edition{Twelfth Release (draft)}

\renewcommand{\qedsymbol}{\ensuremath{\blacksquare}}
\renewcommand\arraystretch{1.35}  %use to change the row height of a table
\renewcommand\emph[1]{\textsl{#1}}

\newcommand{\newterm}[1]{\emph{#1}\marginpar{#1}\index{#1}}  %use me when introducing new terms
\newcommand{\newterms}[2]{\textsl{#1}\marginpar{#2}\index{#2}}  %use when introducing new terms, but you want a different entry in the index from the text.  
\setlength{\marginparwidth}{1.2in}
\let\oldmarginpar\marginpar
\renewcommand\marginpar[1]{\-\-\oldmarginpar[\raggedleft\footnotesize #1]%
{\raggedright\footnotesize #1}}

\newcommand{\fig}[1]{Figure~\ref{fig:#1}} %use \fig{picture} to access \ref{fig:picture} 
\newcommand{\eqn}[1]{Equation~\ref{eqn:#1}} %same as above
\newcommand{\tbl}[1]{Table~\ref{tbl:#1}}  %same as above
\newcommand{\mat}[1]{{\bf #1}}
\newcommand{\matlab}{{\sc Matlab}}
\newcommand{\simulink}{Simulink}
\newcommand{\course}[1]{``#1''}
\newcommand{\laplace}[1]{{\cal L}\left\{#1\right\}}
\newcommand{\diff}[1]{\ensuremath{\frac{d}{d#1}}}

%%%%%page layout%%%%%%
\evensidemargin .875in
\oddsidemargin 0.375in
\textwidth     5.25in
%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
\clearpage
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

Please visit TutorPaul.com if you are interested in making a contribution!
\clearpage
\frontmatter


\chapter*{Preface}
\addcontentsline{toc}{chapter}{Preface}
This course (at least at Texas A\&M) is typically considered quite difficult.  I cannot attribute this stereotyping to any particular difficulty in the course material, but rather to the quality of teaching materials.  The aim of these notes is to take steps to improve the student's understanding, and---when accompanied by a quality lecture---I feel they can be a great help.  However, reading these notes does not guarantee you will do well in the course, you must seek to comprehend the notes, then apply what you have learned by working through problems.  I cannot stress enough the importance of re-inforcing your understanding by working problems, the example problems are engineered to cover the topic, but are generally quite simple and will not be representative of what you will find on the exams.

There is something that is much more important than simply working problems in this course; to really \emph{understand} what a control system is, and knowing why we---as people, not engineers---should care about them.  This knowledge will serve you more faithfully than an encyclopedic knowledge of all the formul\ae\ in this book.  Simply put, a control system is anything which makes what you want to happen, happen.  The four requirements for a control system are listed below.
\begin{itemize}
\item Information about the desired state of the system,
\item the actual state of the system,
\item a way to compare the actual with the desired state, and
\item a way to affect change in the system.
\end{itemize}

Long before engineers were able to hammer out the mathematics of controls (like you will be studying in this course) people were using controls on their simple machinery, like the flushing toilet.  When a toilet is flushed, the tank is emptied, but it needs to be full for the next flush.  The desired state is full, the actual state is empty this difference between empty and full opens the valve and water begins to flood in.  As the water level rises it moves the float which closes the water valve.

A few millennia before that simple control system was conceived another---much more complex---system was allowing early humans to walk upright.  The mind knows it wants to be standing upright, as you start to fall the mind realizes the change, and seeks to correct it by firing muscles in the legs.  The brain receives \emph{feedback} from all the senses and it uses that information to determine what needs to be done to accomplish the tasks it requires.

For the next few days I want you to examine your surroundings and start thinking about the control systems that you encounter, try to identify how the four qualities required are embodied in each case.

\tableofcontents
\listoffigures{}
%\listoftables{}
\cleardoublepage


\mainmatter

\chapter{Introduction}
In this course, and subsequently in the notes, we will be discussing \newterm{signals} and \newterm{systems} quite extensively.  As such it is important that this volume starts with a definition of the two terms.

\section{Signals}
A signal is simply a function that expresses the state of one or more independent variables.  Throughout this course you will be required to manipulate both \newterm{continuous} and \newterm{discrete} signals.  We, as humans, experience every signal (temperature, sound, smell, etc.) in continuous time; however, computers cannot absorb information continuously, thus the need for discrete signals.  A discrete signal is comprised of a series of \newterm{sample}\emph{s}\footnote{A sample is the value of a signal at a chosen instant}---usually taken at a regular time interval---of a continuous signal.  \fig{continuous_discrete} shows a continuous signal and its discrete counterpart; $T_s$, represents the time between samples.  Each sample can be referenced by an integer which indicates its place in the signal.  

\begin{figure}[ht]
\begin{center}
\includegraphics{00_complete_figs/cont_disc.pdf}
\caption{A Continuous Signal and Its Matching Discrete Signal}
\label{fig:continuous_discrete}
\end{center}
\end{figure}

\section{Systems}
A system is defined (somewhat vaguely) as something that responds to or produces signals.  So, in a given system you can have one or more input signals (voltage, force, torque, etc.), and one or more output signals (position, velocity, temperature, etc.).  Just as there are continuous and discrete signals there are continuous and discrete systems.  Physical systems, like those you studied in \course{Dynamics and Vibrations}, are (most often) continuous systems, while computers are discrete systems. 

\chapter{Mechanical System Modeling}
\input{04_modeling_mech}

\chapter{State Space Representation}
% Maybe add some matlab code, or a simulink model.
\input{05_state_space}

\chapter{Electrical System Modeling}
\input{06_modeling_elec}

\chapter{Electromechanical System Modeling}
\input{07_modeling_mixed}

\chapter{Fluidic and Thermal System Modeling}
\input{08_thermal_modeling}

\chapter{Linearization}
\input{09_linearization}

\chapter{Laplace Transformation}
\input{02_laplace}

\chapter{Transfer Functions}
\input{10_transfer_func}

\chapter{Block Diagrams}
\input{12_block_diagrams}

\chapter{Impact of Pole Locations}
\input{11_pole-zero_location}

\chapter{Bode Plots}
\input{13_bode_plot}

\chapter{Feedback}
\input{14_feedback}

\chapter{PID Control}
\input{15_pid}

\chapter{Error Tracking and\ Stability}
\input{16_tracking_stability}

\chapter{Root-Locus}
\input{19_root_locus}

\chapter{Stability Margins}
\input{17_stability_margin}

\chapter{Compensation}
\input{18_compensation}

\chapter{Compensation Using Root-Locus}
\input{20_rlocus_compensator}

\appendix
\chapter{Exam Review}

I am including all the exam review topics, it is up to you to find out from the professor which topics will be covered by each exam.
\input{AP_exams}

\chapter{\texttt{ODE45} Primer}
\input{AP_ode45}

\chapter{\simulink\ Primer}
\input{AP_simulink}

\chapter{Fourier Analysis}
\input{01_fourier}

\chapter{Signal Sampling}
% Try to break this down into sections.
\input{03_aliasing}

\chapter{Math Reference}
\input{AP_math}

\backmatter
\addcontentsline{toc}{chapter}{Index}
\printindex

\chapter{Colophon}
This manuscript was generated using \LaTeX, a free and open-source typesetting suite.  It easily accommodates math and tracks equation, figure, and table numbers to facilitate revision.  I modified (slightly) the standard book document class to achieve the formatting you see.  The figures were generated using xFig (also free and open source) which is a vector graphics program, GNUPlot was used as a supplement to xFig in generating plots. 

\end{document}


Changelog (initial release was the Preliminary Edition):

First Edition:
Edited Grammar, incorporated suggestions from Megan McIlroy
Changed all instances of 'you' to 'we'...
Fixed repeated refs from the addition of the Appendix

Second Edition:
Edited grammar and usage.
Added paragraph about Euler's Relation in Fourier topic.
Added example to Fourier topic
Added example to Laplace topic
Fixed a section heading in the Aliasing topic (/section -> \section)
Fixed an error in the last equation of the Aliasing topic
Fixed an error in the example section of the Mechanical modeling section.
Added equation numbers to the definition of the state space form.
Added 'ODE45' to the index in the State Space topic.
Added an appendix: "ODE45 Primer."
Added an appendix: "Simulink Primer."
Fixed figures (moved \end{center}s to after the \labels)
Fixed the electrical figures.  Used library schematics for elements.
Fixed taylor series expansion equation for clarity.  rather than use |_{x_0,y_0}, I used |_{x_0 \atop y_0}.
Revised the Linearize section of the Linearization topic for clarity.
Expanded the preface to include a brief discussion of what controls are and what they require.
Fixed an error in the compensation topic (pole and zero were reversed)
Added the eigenvalues subsection to the stability topic.
Re-arranged the feedback topic.
Talked about the 'unit circle' in the bode plot topic.
Expanded and rotated pole locations figure.

Third Edition:
Fixed mathematical typos in exam review (and Bode plots topic).
Added a colophon
Revised wording in many places.
Fixed grammar and usage in many places.
Revised the modeling strategy section of the mechanical system modeling topic.
Fixed the math in the example of the state space representation topic.
Fixed the math in the E-M system modeling example.
Fixed the math in the Block diagrams example.
Removed \tau from Bode plots topic.
Expanded theory section of the bode plot topic.
Fixed an error regarding system type in the error tracking topic.
Fixed punctuation errors in a few of the maths overall.
Corrected some index syntax.
Expanded index to include references from appendix and other places in text.

Fourth Edition: 
General grammar and usage fixes.

Fifth Edition:
Grammar and usage
Fixed several figures:
	Fixed sign conventions in electrical systems topic
	Added coordinate to figure in bode plots topic
	Fixed the font on the linearization topic plot
	Fixed the x-axis label on the continuous-discrete figure in first topic
Added verbiage for sign conventions in electrical systems topic
Added the matrix dimensions to the state space topic
Added the mathematics appendix including proofs of laplace transform properties
Re-arranged entire note-set to reflect new order of topics in the lecture
	Moved the deprecated material to appendix

Sixth Release:
Added Rolling Without Slipping to Mechanical Systems modeling topic
Expanded and corrected the definition of settling time
Added figure for finding percent overshoot as a function of damping
Changed pole location plot to polar, added two response plot samples.

Seventh Release:
Added discussion of initial conditions to the transfer function topic.
Added resolution to the step response plot in the pole locations topic.
Added mass properties to the math appendix.

Eight Release:
Changed figure in introduction to accurately reflect the notion of sampling.
Fixed the rwos example to place the center point at the center.
Fixed some grammar in elec_modelling.
Changed the presentation of the Taylor series expansion.

Ninth Release: 
Fixed the figure in the Introduction.
Added a subsection to the Transfer Functions topic to describe the derivation of the state-space to transfer function equation.
Added an example to the pole placement topic
Expanded the explanation of states.

Tenth Release:
Changed subjects from 'you' to 'we'
Fixed typographical errors.
Fixed the Derivation section of the Transfer Function topic
Updated the polar pole location plot
Updated the Error Tracking and Stability topic to better reflect that the open loop transfer function is to be used.
Fixed the tenses in "Error Tracking and Stability" topic.
Added Lagrangian Mechanics to the Mechanical Modelling topic.
Added subsection to address the system type for disturbance rejection
Added a figure to the Bode Plotting topic to describe the graphical method of finding gain and phase
Changed state space nomenclature throughout: Q to \mathbf{X}
Added an example to the root-locus compensation topic.


Eleventh Release:
Added an equation to the thermo chapter.
Added copyright information!

12th Release
* Added voltage relation for transformers
* Added complex/derivative method for partial fraction decomposition

Todo: 
Add example to electrical modeling section. (maybe)
