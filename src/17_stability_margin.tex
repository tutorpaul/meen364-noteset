\section{Motivation}
We've recently been discussing stability and its implications, and now we will discuss another way to determine stability.  More important than just stability, however, is the \emph{degree} of stability---which is what \newterm{gain margin} and \newterm{phase margin} will tell you.  The two aforementioned terms are collectively known as \newterm{stability margins}.  Conveniently, the closed loop stability can be determined by finding the gain and phase margin of the open loop system, this can save the effort of actually determining the closed loop system transfer function.

\section{Gain Margin}
In a system with a variable gain, $K$, it is useful to know how much you can increase the gain without making the system unstable.  This is precisely what the gain margin (GM) of a system tells you.  Bear in mind that the open loop system's gain margin determines the closed loop system stability.  To find the gain margin, find the frequency where the phase crosses the $-180^\circ$ mark.  The gain margin is the difference between $1$ and the gain at this frequency (\eqn{gain_margin}).  If the gain margin is negative then the system is already unstable.

\begin{equation}
20\log |1| - 20\log |G(j\omega _0)| = \text{GM (in \unit{dB}); when } \angle G(j\omega _0)=-180^\circ
\label{eqn:gain_margin}
\end{equation}

Should the system's phase never cross the $-180^\circ$ line then the gain margin is $\infty$. 

\section{Phase Margin}
The phase margin is measured at the \newterm{crossover frequency}, or the frequency where the gain is equal to one (magnitude equals zero), and it is equal to the phase plus $180^\circ$ (\eqn{phase_margin}).  The phase margin is (approximately) linearly related to the damping ratio for phase margins of less than $70^\circ$ (\eqn{phase_damping}).  If the phase margin is negative then the system has negative damping and is unstable.

\begin{equation}
\angle G(j\omega _c)+180^\circ =\text{PM; when } 20\log |G(j\omega _c)| = 0
\label{eqn:phase_margin}
\end{equation}

\begin{equation}
\zeta = \frac{\text{PM}}{100}\text{; when PM}<70^\circ
\label{eqn:phase_damping}
\end{equation}

\section{Example}
Let's find the stability margins of the system defined by \eqn{marg_ex_sys}.

\begin{equation}
G(s)=\frac{3}{s(s^2+2s+5)}
\label{eqn:marg_ex_sys}
\end{equation}

Let's start by finding the crossover frequency, $\omega _c$, which is the frequency where the transfer function gain is equal to one.  This can be done by applying the techniques used for Bode plotting.

\begin{eqnarray*}
|G(j\omega _c)|=&1 &= \frac{\sqrt{3^2}}{\sqrt{\omega _c^2}\sqrt{\left(5-\omega _c^2\right)^2+\left( 2\omega _c \right)^2}}   \\
& 3^2 &= \omega _c^2\left(\left(5-\omega _c^2\right)^2+\left( 2\omega _c \right)^2\right) \\
& 9 &= \omega _c^2\left( 25-10\omega _c^2+\omega _c^4+4\omega _c^2 \right) \\
& 0 &=\omega _c^6 - 6\omega _c^4 +25\omega _c^2-9 \\
\therefore & \omega _c &= \pm 0.6285 \text{\quad (only the real solutions are shown)}
\end{eqnarray*}

Now, there is no such thing as a negative frequency so $\omega _c = \unitfrac[0.6285]{rad}{sec}$.  The next thing to be done is to find the frequency where the phase is equal to $-180^\circ$, or $\omega _0$.

\begin{eqnarray*}
\angle G(j\omega _0 )=& -180 &= \cancelto{0^\circ}{\tan ^{-1}\left(\frac{0}{3}\right)}-\cancelto{90^\circ}{\tan ^{-1}\left(\frac{\omega _0}{0}\right)} -\tan ^{-1}\left(\frac{2\omega _0}{5-\omega _0^2}\right) \\
& -180^\circ+90^\circ &= -\tan ^{-1}\left(\frac{2\omega _0}{5-\omega _0^2}\right) \\
& \tan \left(90^\circ\right) &= \frac{2\omega _0}{5-\omega _0^2} \\
& \infty &= \frac{2\omega _0}{5-\omega _0^2} \\
\therefore & 5-\omega _0^2 &= 0 \\
& \omega _0 &=\pm \sqrt{5} = \pm 2.2361
\end{eqnarray*}

Once again, there cannot be a negative frequency so $\omega _0 = \unitfrac[2.2361]{rad}{sec}$.  Next, we can find the gain margin by finding the magnitude of the transfer function (in \unit{dB}) at $\omega _0$.  

\begin{eqnarray*}
&|G(j\omega _0)| &= \frac{\sqrt{3^2}}{\sqrt{\omega _0^2}\sqrt{\left(5-\omega _0^2\right)^2+\left( 2\omega _0 \right)^2}} \\
& &= \frac{3}{2.2361\sqrt{(5-5)^2+(2(2.2361))^2}} \\
& &= \frac{3}{2.2361(2)(2.2361)} \\
& &= \frac{3}{10} = 0.3 \\
\text{\& GM} =& -20\log |G(j\omega _0)| &= \unit[10.458]{dB} 
\end{eqnarray*}

Now we are ready to find the phase margin of the system, which is the phase angle at $\omega _c$.

\begin{eqnarray*}
&\angle G(j\omega _c ) &= \cancelto{0^\circ}{\tan ^{-1}\left(\frac{0}{3}\right)}-\cancelto{90^\circ}{\tan ^{-1}\left(\frac{\omega _c}{0}\right)} -\tan ^{-1}\left(\frac{2\omega _c}{5-\omega _c^2}\right) \\
& &= -90^\circ -\tan^{-1}\left(\frac{2(0.6285)}{5-(0.6285)^2}\right) \\
& &=-90^\circ -\tan ^{-1}\left(\frac{1.257}{4.605}\right) \\
& &=-105.27^\circ \\
\text{\& PM}=&\angle G(j\omega _c ) +180^\circ &= 74.73^\circ
\end{eqnarray*}

\fig{margins} graphically shows the stability margins of the system.

\begin{figure}[ht]
\begin{center}
\includegraphics{17_stability_margin_figs/margin.pdf}
\caption{Stability Margins for Example System}
\label{fig:margins}
\end{center}
\end{figure}


In the event that there is more than one valid value for either $\omega _c$ or $\omega _0$ we would need to find the stability margins for each associated frequency, then choose the worst of those values as the actual system gain and phase margins.  

Alternately, we could use the \texttt{margin}\marginpar{\texttt{margin}}\index{margin@\texttt{margin}} command in \matlab, which will give us the Bode plot with the stability margins given, it will look similar to \fig{margins}.
