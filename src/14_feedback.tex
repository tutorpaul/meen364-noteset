\section{Motivation}
We have discussed how to find the transfer function of a system with \newterm{feedback}, but we have not talked about the implications of feedback, which is what will be done presently. 

If you have a system you want to control you have two options: \newterm{open-loop}, or \newterm{closed-loop} controls.\footnote{The terms `closed-loop control' and `feedback control' are used interchangeably}  To clarify this I will use the analogy of a lathe (think: machine shop).  In the shop you have two choices: manual or computer controlled.  When using the manual lathe you personally observe the state of the system and adjust the input accordingly to complete the operation satisfactorily.  Conversely, you can input a specific operation (specify a \newterm{setpoint}) and let the machine take care of actually moving the cutting tool.  Now, at a glance it seems like using the CNC lathe is always the best option because you can get a better, more repeatable result, but not all parts merit the extra cost associated with using the more complicated system.  In this lecture I hope to teach you the advantages to feedback control, and when it is appropriate to use feedback. 

\section{Advantages to Feedback Control}
Up until now the word ``stability'' has meant little to you in the scope of this course, as it will remain for another topic or so, nevertheless I feel comfortable saying that implementing closed-loop control on a system can improve its stability.  Stability aside, feedback provides four key improvements over open-loop control:\footnote{There are limitations and drawbacks to feedback control, but they are beyond the scope of this class.}
\begin{enumerate}
\item Disturbance Rejection,
\item Noise Reduction,
\item Reference Tracking, and
\item Reduced Sensitivity.
\end{enumerate}

\begin{figure}[ht]
\begin{center}
\includegraphics{14_feedback_figs/desc_sys.pdf}
\caption{Several Imperfect Systems}
\label{fig:desc_sys}
\end{center}
\end{figure}

\subsection{Disturbance Rejection}
In an actual system there is always the possibility for some measurable disturbance, be it from some external vibration, a change in load, or anything else that might perturb the system.  When you add a disturbance to the feedback loop you will get a system that looks like \fig{desc_sys}a, and a (closed-loop) transfer function like the one given as \eqn{desc_sys_dis}.  To reject disturbances---which, by and large, occur at low frequencies---you will want the closed-loop system (\eqn{desc_sys_dis}) gain to be much less than 1, thus the open-loop system ($C(s)G(s)$) should have a large gain at low frequency.

\begin{equation}
\frac{Y(s)}{D(s)}=\frac{1}{1+C(s)G(s)}
\label{eqn:desc_sys_dis}
\end{equation}

\subsection{Noise Reduction}
All (real) systems have noise, usually from sensors or electrical interference, and it usually occurs at high frequency.  A representation of a noisy system is given as \fig{desc_sys}c, which has the transfer function given as \eqn{desc_sys_noise}.  To eliminate noise you want the closed-loop transfer function to have a low gain at high frequencies.  Thus, you want the gain of $C(s)G(s)$ to be approximately 0 as $\omega \rightarrow \infty$.

\begin{equation}
\frac{Y(s)}{N(s)}=\frac{-C(s)G(s)}{1+C(s)G(s)}
\label{eqn:desc_sys_noise}
\end{equation}

\subsection{Reference Tracking}
In a great deal of cases you will want a system's output to follow a specific function so you introduce a \newterm{reference input}, $R(s)$, as was done in \fig{desc_sys}b.  This will give you the closed-loop transfer function given as \eqn{desc_sys_ref}, and since you want the input to track the output ($Y(s)\approx R(s)$) you will want the gain of $C(s)G(s)$ to be much larger than 1 at any frequency at which the system might operate.

\begin{equation}
\frac{Y(s)}{R(s)}=\frac{C(s)G(s)}{1+C(s)G(s)}
\label{eqn:desc_sys_ref}
\end{equation}

\subsection{Reduced Sensitivity}
When you are dealing with a real system you won't always know the system parameters exactly, so you make an educated guess and hopefully get close.  When you don't get close enough to use open-loop controls you need to use feedback.  \fig{desc_sys}d shows a feedback loop with an inexactly known plant, $G^*(s)$, and \eqn{desc_sys_sens} is the transfer function that relates the current value of the output, $Y(s)$, to the incremental change in the output, $\delta Y(s)$.  Now I'm not going to go into the mechanics of it all, but you want your open-loop transfer function, $C(s)G^*(s)$, to be close to zero at high frequencies.

\begin{equation}
\frac{\delta Y(s)}{Y(s)}=\frac{1}{1+C(s)G^*(s)}
\label{eqn:desc_sys_sens}
\end{equation}

To find the sensitivity of some system, $G(s)$, with respect to some variable, $k$, you can use the following equation.

\begin{equation}
S_k^{G(s)} = \frac{k}{G(s)}\frac{dG(s)}{dk}
\label{eqn:sensitivity}
\end{equation}
 
An ideal system has all of the previous qualities; its Bode plot would look something like the one given in \fig{ideal_sys}.

\begin{figure}[ht]
\begin{center}
\includegraphics{14_feedback_figs/ideal_sys.pdf}
\caption{Bode Plot for an Ideal System}
\label{fig:ideal_sys}
\end{center}
\end{figure}
