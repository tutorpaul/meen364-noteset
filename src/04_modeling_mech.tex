\section{Translational Systems}

Translational systems consist of three basic building blocks: masses, springs, and dashpots.\footnote{This statement assumes you are dealing with a lumped parameter system, which will be the only kind you will deal with in this course}  These elements should be familiar to you from \course{Dynamics and Vibrations}, however this document will briefly review what you need to know about them in case you have forgotten.  \eqn{newtons_second} shows Newton's second law which relates acceleration and force.  This relationship relies on \newterm{inertia} which is a property of bodies with \newterm{mass}.  

\begin{equation}
\sum F = m \frac{dv}{dt} = ma = m \ddot x
\label{eqn:newtons_second}
\end{equation}

In \eqn{newtons_second}, $m$ is the mass of the body in question, $v$ is the body's velocity, and $F$ refers to a force acting on the body---all the external forces must be summed.  Thus, the sum of the forces is equal to the mass of a body times its acceleration.  You must remember that Newton's second law is only valid in an \newterm{inertial reference frame}---or with respect to a stationary datum.  I repeat, you must have a coordinate system that is fixed in space.

Masses can also store energy in two ways, the kinetic energy, $T$, of a mass in motion is shown in \eqn{trans_kinetic_energy}. The gravitation potential energy of a mass, given as \eqn{grav_pot_energy}, has to do with its vertical position, $h$, relative to the \newterm{datum}.

\begin{equation}
T = \frac{1}{2}m\dot x^2
\label{eqn:trans_kinetic_energy}
\end{equation}

\begin{equation}
V_g = mgh
\label{eqn:grav_pot_energy}
\end{equation}

The role of a \newterm{spring} is to store energy.  Simply put, if you push on a spring it will push you back; if you pull on a spring it will pull back on you.  This concept is embodied by Hooke's law which is shown in \eqn{hookes_law}, where $k$ is the spring's \newterm{stiffness} coefficient, and $F$ is the amount of force required to deform the spring a distance $x$.

\begin{equation}
F=kx
\label{eqn:hookes_law}
\end{equation}

Springs store energy when they stretch or compress, this mechanical energy is called spring potential energy which is shown in \eqn{trans_spring_pot_energy}. Recall that $x$ is the deflection of the spring.

\begin{equation}
V_s = \frac{1}{2}kx^2
\label{eqn:trans_spring_pot_energy}
\end{equation}

\newterms{Dampers}{damper}, or \newterms{dashpots}{dashpot}, act as energy dissipaters.  Now you might ask ``Why would I want to dissipate energy?'' The answer is so your car will stop bouncing, and that heavy door won't slam into your face.  A damper's response is proportional to the velocity at which it is compressed or expanded, as is shown in \eqn{dampers}, $c$ is the damping coefficient, and $F$ is the force required to deform the damper at a velocity $v$.

\begin{equation}
F=cv=c\frac{dx}{dt}=c\dot x
\label{eqn:dampers}
\end{equation}

Dampers dissipate energy when they compress or stretch, this dissipative energy looks like \eqn{trans_dissipative_energy}.

\begin{equation}
D = \frac{1}{2}c\dot x^2
\label{eqn:trans_dissipative_energy}
\end{equation}

\section{Rotational Systems}

Just like translational systems, rotational systems have springs, dampers, and inertias; the real difference is that you model using the summation of moments rather than forces.  Newton's second law for rotational systems is shown in \eqn{newtons_second_rot}; $J$ represents the moment of inertia of the rotating body, and $\theta$ is the angular position of the body. \eqn{rot_kinetic_energy} shows the kinetic energy of a rotational system.

\begin{equation}
\sum M = J \frac{d^2\theta}{dt^2} = J \ddot \theta
\label{eqn:newtons_second_rot}
\end{equation}

\begin{equation}
T = \frac{1}{2}J\dot\theta^2
\label{eqn:rot_kinetic_energy}
\end{equation}

Hooke's law in a rotational system is applied to a flexible shaft, and is shown in \eqn{hookes_rot}, $k_{\theta}$ is the torsional stiffnes, and $M_k$ is the induced moment when the shaft is deformed by $\theta$. The rotational potential energy of a spring is given in \eqn{rot_spring_energy}.

\begin{equation}
M_k=k_{\theta}\theta
\label{eqn:hookes_rot}
\end{equation}

\begin{equation}
V_s = \frac{1}{2}k_\theta \theta^2
\label{eqn:rot_spring_energy}
\end{equation}

Viscous damping is present in bearings, and when significant it will be modeled using \eqn{damping_rot}.  Just as before the damping is proportional to the velocity of the system, so in the rotational case we will use rotational velocity, $\dot \theta$ (the damping coefficient is usually referred to as $B$ in rotational systems). Finally we see the dissipative energy of a rotational damper in \eqn{rot_dissipative_energy}.

\begin{equation}
M_B=B\frac{d\theta}{dt}=B\dot \theta
\label{eqn:damping_rot}
\end{equation}

\begin{equation}
D = \frac{1}{2}B\dot \theta^2
\label{eqn:rot_dissipative_energy}
\end{equation}

Bringing everything together to find the equation of motion works the same as in the translational case, just remember that you are dealing in moments, rather than forces.

\section{Rolling Without Slipping}
\begin{figure}[h!]
\begin{center}
\includegraphics{04_modeling_mech_figs/rwos_example.pdf}
\caption{A System Exhibiting Rolling Without Slipping Behavior}
\label{fig:rwos_example}
\end{center}
\end{figure}

In the process of mechanical system modeling we may encounter systems that exhibit \newterms{Rolling Without Slipping}{rolling without slipping} (RWOS) behavior much like the system illustrated in \fig{rwos_example}.  In actual fact the only systems that roll perfectly without slipping are cogged wheels and rack-and-pinion systems like the one shown in \fig{cogged_wheel}.  Any real, non-geared, system will experience \emph{some} slipping, but usually the slipping is quite minute and can safely neglected.



\begin{figure}[ht]
\begin{center}
\includegraphics{04_modeling_mech_figs/cogged_wheel.pdf}
\caption{A Rack-And-Pinion System Which Rolls Perfectly Without Slipping}
\label{fig:cogged_wheel}
\end{center}
\end{figure}

The approach we will use to analyze a rolling without slipping system will be to determine the kinematic constraint relating the angular rotation of the cylinder, $\theta$, to the distance the cylinder travels, $x$.  See \fig{rolling_without_slipping}, as the cylinder moves from Position (a) to Position (b) it paints the ground black while a matching black streak appears on the surface of the cylinder.  We can measure the length of the black streak on the ground and call that distance $x$, likewise we can measure the angular sweep covered by the black streak and call that quantity $\theta$.  If we think back to geometry we will remember that the length of a swept circular arc is $r\theta$.  Since the length of the swept arc and the distance travelled must be the same we can deduce the general kinematic constraint for rolling without slipping situations which is given in \eqn{rwos_kinematics}.
\begin{figure}[ht]
\begin{center}
\includegraphics{04_modeling_mech_figs/rolling_without_slipping.pdf}
\caption{Graphical Deduction of Rolling Without Slipping Kinematics}
\label{fig:rolling_without_slipping}
\end{center}
\end{figure}
\begin{equation}
x = r \theta
\label{eqn:rwos_kinematics}
\end{equation}

\section{Mixed Systems}

Oftentimes you will not be dealing with a purely translational or rotational system, but rather you will have a system that is mostly of one type with one or two components of the other type.  For instance, a pulley (with inertia) in a translational system. We can consider systems of this type as a special case of the rolling without slipping problem where the ground (in this case the cable) is moving, and the cylinder is stationary.  Thus the kinematic constraint developed above (\eqn{rwos_kinematics}) is valid.  Don't forget that moments, not forces, induce motion in a pulley, and that you will need to multiply a force by its moment-arm to get a moment.

\section{Newtonian Modeling Strategy}
Now that we know how to model all the elements in mechanical systems we can focus on how to model an entire system.

\begin{enumerate}
\item Define a positive direction of motion for each body.  Use this direction as the positive direction for all vectors relating to the body. \label{assigncoord}
\item Determine any required \newterm{kinematic constraint} relationships; relationships that define the motion for independent bodies that are connected physically (via contact or an inextensible cord). \label{getkineconstr}
\item Draw a \newterms{Free Body Diagram}{free body diagram} (FBD) for each body.
\begin{enumerate}
\item Isolate the body by cutting any cords, springs, or dampers connecting the body to any other body.
\item Draw all the forces acting on the body.  Explain why any forces were omitted---the primary reason will be that they don't effect the dynamics because they are out of the plane of motion.  Remember that a force is a vector and it should be depicted as such.
\item Re-draw the positive direction of motion for your reference.  This must match the direction chosen in Item~\ref{assigncoord}.
\item When appropriate, indicate the angle at which the body is moving relative to the defined reference system.
\end{enumerate}
\item Apply Newton's Second Law: $\sum F_x= m \ddot x$ or $\sum M_\theta= J \ddot \theta$.
\item Apply kinematic constraints to get a single equation of motion for each degree of freedom.
\end{enumerate}

\section{Lagrangian Mechanics}
\newterm{Lagrangian mechanics} is an alternative technique for finding the dynamics (equations of motion) of a mechanical system. Much like the Conservation of Energy and Work-Energy methods this method utilizes the energy states of a system to determine its dynamics. To find the equations of motion for a conservative system we start by finding the general kinetic and potential energies of the system ($T$ and $V$, respectively), then formulate the Lagrangian given by \eqn{lagrangian}. 
\begin{equation}
L = T-V
\label{eqn:lagrangian}
\end{equation}
Once the Lagrangian is known the dynamics for each generalized coordinate\footnote{Generalized coordinate is a fancy way of saying degree of freedom}, $q_i$, can be expressed using the functional derivative:
\begin{equation}
\frac{d}{dt}\left(\frac{\partial L}{\partial \dot q_i}\right) - \frac{\partial L}{\partial q_i} = 0
\label{eqn:lagrange_functional_derivative}
\end{equation}

Note that the above equation \emph{is} the equation of motion, and it must be written once for each degree of freedom ($i=1\dots n$). The above is very useful for systems that lack damping and are not acted upon by an external force, however the method can be extended to handle those as well.

\subsection{Damping}
To deal with a dissipative system we define the Rayleigh dissipation function, $R$, which is the sum of the dissipative energies of all the dampers in the system. The equations of motion for the system then look like this:
\begin{equation}
\frac{d}{dt}\left(\frac{\partial L}{\partial \dot q_i}\right) - \frac{\partial L}{\partial q_i} + \frac{\partial R}{\partial \dot q_i}= 0
\label{eqn:lagrange_dissipative}
\end{equation}

\subsection{External Forces}
External forces do work on a system, they can be quantified (for the types of systems we deal with most easily) as: \[W = \int_C F\, dq_i\] The generalized force, $Q_i$, is given as the partial derivative of the work: \[Q_i = \frac{\partial W}{\partial q_i} =  \frac{\partial}{\partial q_i} \int_C F \, dq_i = F \]

External moments are handled in much the same way, we can identify the work that is done by the moment: \[W = \int_C T\, dq_i\] Notice that $q_i$ should be a rotational coordinate when dealing with an external moment. When the partial derivative is taken with respect to $q_i$ we are left with $Q_i = T$.

When included in the equation of motion we see the following:
\begin{equation}
\frac{d}{dt}\left(\frac{\partial L}{\partial \dot q_i}\right) - \frac{\partial L}{\partial q_i} + \frac{\partial R}{\partial \dot q_i}= Q_i
\label{eqn:lagrange_dissipative_forced}
\end{equation}


\section{Example} \label{sec:mech_example}

Let's find the equations of motion for the system in \fig{example_sys}.  We will assume it starts from equilibrium, thus the effects of gravity have been mitigated.
\begin{figure}[ht]
\begin{center}
\includegraphics{04_modeling_mech_figs/example_sys.pdf}
\caption{Example Rotational and Translational System}
\label{fig:example_sys}
\end{center}
\end{figure}

\subsection{Newtonian Approach}
We will start by defining the coordinates, and making a motion assumption,  $x > r_2 \theta _2 > r_1 \theta _1$. Now we can draw the free body diagrams, as shown in \fig{example_marked}.
\begin{figure}[ht]
\begin{center}
\includegraphics{04_modeling_mech_figs/example_marked.pdf}
\caption{Marked Up Rotational and Translational System}
\label{fig:example_marked}
\end{center}
\end{figure}
Then we can define each of the forces based on the motion assumption.
$$F_{k_1}=k_1r_1\theta _1 $$
$$F_{k_2}=k_2(r_2\theta _2-r_1\theta _1) $$
$$F_c=c(r_2\dot \theta _2-r_1\dot \theta _1) $$
$$F_{k_3}=k_3(x-r_2\theta _2) $$
Now we are ready to apply Newton's second law (Remember to multiply by the moment-arm).
$$\sum M_{\theta _1}=J_1 \ddot \theta _1 = -r_1F_{k_1} + r_1(F_{k_2}+F_c) $$
$$\sum M_{\theta _2}=J_2 \ddot \theta _2 = -r_2(F_{k_2}+F_c)  +r_2F_{k_3} $$
$$\sum F_x = m\ddot x = f(t) - F_{k_3} $$
Lastly, we can re-arrange the equations of motion into standard form.
\begin{align}
J_1 \ddot \theta _1 + cr_1^2 \dot \theta _1 -cr_1r_2 \dot \theta _2 + (k_1+k_2) r_1^2 \theta _1 - k_2r_1r_2\theta_2 =0 \label{eqn:mechanical_system_eom_theta1} \\
J_2 \ddot \theta _2 - cr_1r_2 \dot \theta _1 + cr_2^2 \dot \theta _2 -k_2r_1r_2\theta _1 +(k_2+k_3)r_2^2 \theta _2 -k_3 r_2 x =0 \label{eqn:mechanical_system_eom_theta2} \\
m\ddot x -k_3r_2\theta _2 +k_3x = f(t)\label{eqn:mechanical_system_eom_x} 
\end{align}

\subsection{Lagrangian Approach}
We begin by defining the generalized coordinates: $\theta_1$, $\theta_2$, and $x$. For simplicity we will keep the same definitions and conventions as above. Then we will explore the general kinetic, $T$, and potential energy, $V$, of the system in terms of the generalized coordinates.
\begin{align*}
T &= \frac{1}{2} J_1\dot\theta^2 + \frac{1}{2}J_2\dot\theta^2+ \frac{1}{2}m\dot x^2 \\
V &= \frac{1}{2}k_1(r_1\theta_1)^2 + \frac{1}{2}k_2(r_2\theta_2-r_1\theta_1)^2 + \frac{1}{2}k_3(x-r_2\theta_2)^2
\end{align*}
Notice that the potential energy of springs $k_2$ and $k_3$ depends on the deflection---that is the difference in displacement of each end. Notice also that there is no gravitational potential energy term, this was omitted because the coordinates are measured from equilibrium; the initial stretch in the spring is compensating for the weight of the dangling body.

Now we can define the Lagrangian as the difference between the kinetic and potential energies (\eqn{lagrangian}).
\begin{align*}
L &= T - V \\
&=\frac{1}{2} J_1\dot\theta^2 + \frac{1}{2}J_2\dot\theta^2+ \frac{1}{2}m\dot x^2 - \left(\frac{1}{2}k_1(r_1\theta_1)^2 + \frac{1}{2}k_2(r_2\theta_2-r_1\theta_1)^2 + \frac{1}{2}k_3(x-r_2\theta_2)^2\right) \\
&=\frac{1}{2} J_1\dot\theta^2 + \frac{1}{2}J_2\dot\theta^2+ \frac{1}{2}m\dot x^2 - \frac{1}{2}k_1(r_1\theta_1)^2 - \frac{1}{2}k_2(r_2\theta_2-r_1\theta_1)^2 - \frac{1}{2}k_3(x-r_2\theta_2)^2
\end{align*}

Since there is a damper in the problem we can go ahead and define the Rayleigh dissipation function as well. \[R = \frac{1}{2}c(r_2\dot\theta_2-r_1\dot\theta_1)^2\] Noting that the external force is attached to a point which moves like $x$ we can define the external work done as well. \[W = \int_C f(t)\, dx\]

We will have to apply \eqn{lagrange_dissipative_forced} for each generalized coordinate, starting with $\theta_1$ we can express our equation of motion as:
\[\frac{d}{dt}\left(\frac{\partial L}{\partial \dot \theta_1}\right) - \frac{\partial L}{\partial \theta_1} + \frac{\partial R}{\partial \dot \theta_1}= \frac{\partial W}{\partial \theta_1}\]
So, we set about differentiating. 
\[\frac{\partial L}{\partial \dot \theta_1} = J_1\dot\theta_1 \quad\Rightarrow\quad \frac{d}{dt}\left(\frac{\partial L}{\partial \dot \theta_1}\right) = J_1\ddot\theta_1\]
\[\frac{\partial L}{\partial \theta_1} = -k_1r_1\theta_1(r_1) - k_2(r_2\theta_2-r_1\theta_1)(-r_1)\]
(Remembering chain rule.)
\[\frac{\partial R}{\partial \dot \theta_1} = c(r_2\dot\theta_2-r_1\dot\theta_1)(-r_1)\]
\[\frac{\partial W}{\partial \theta_1} = 0 \]

Putting all of these together in the prescribed manner (being careful of signs) will give us an identical result to \eqn{mechanical_system_eom_theta1}:
\[J_1 \ddot \theta _1 + cr_1^2 \dot \theta _1 -cr_1r_2 \dot \theta _2 + (k_1+k_2) r_1^2 \theta _1 - k_2r_1r_2\theta_2 =0 \]

Now for the second generalized coordinate, $\theta_2$.
\[\frac{d}{dt}\left(\frac{\partial L}{\partial \dot \theta_2}\right) - \frac{\partial L}{\partial \theta_2} + \frac{\partial R}{\partial \dot \theta_2}= \frac{\partial W}{\partial \theta_2}\]
\[\frac{\partial L}{\partial \dot \theta_2} = J_2\dot\theta_2 \quad\Rightarrow\quad \frac{d}{dt}\left(\frac{\partial L}{\partial \dot \theta_2}\right) = J_2\ddot\theta_2\]
\[\frac{\partial L}{\partial \theta_2} = - k_2(r_2\theta_2-r_1\theta_1)(r_2) - k_3(x-r_2\theta_2)(-r_2)\]
\[\frac{\partial R}{\partial \dot \theta_2} = c(r_2\dot\theta_2-r_1\dot\theta_1)(r_2)\]
\[\frac{\partial W}{\partial \theta_2} = 0 \]

Pull this together into the equation of motion we find the same result as from the Newtonian method (\eqn{mechanical_system_eom_theta2}):
\[J_2 \ddot \theta _2 - cr_1r_2 \dot \theta _1 + cr_2^2 \dot \theta _2 -k_2r_1r_2\theta _1 +(k_2+k_3)r_2^2 \theta _2 -k_3 r_2 x =0\]

Finally we come to the third generalized coordinate: $x$.
\[\frac{d}{dt}\left(\frac{\partial L}{\partial \dot x}\right) - \frac{\partial L}{\partial x} + \frac{\partial R}{\partial \dot x}= \frac{\partial W}{\partial x}\]
\[\frac{\partial L}{\partial \dot x} = m\dot x \quad\Rightarrow\quad \frac{d}{dt}\left(\frac{\partial L}{\partial \dot x}\right) = m\ddot x\]
\[\frac{\partial L}{\partial x} = - k_3(x-r_2\theta_2)(1)\]
\[\frac{\partial R}{\partial \dot x} = 0\]
\[\frac{\partial W}{\partial x} = f(t) \]

This easily comes together to match \eqn{mechanical_system_eom_x}.
\[m\ddot x -k_3r_2\theta _2 +k_3x = f(t)\]

Lagrange's method may seem like an overly-difficult contrivance on a system like the one depicted above, but once you try the two methods on something more complex (like a double pendulum) you will see its benefit. 
