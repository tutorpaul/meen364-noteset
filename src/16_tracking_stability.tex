\section{Steady-State Error}
The steady-state error, $e_\text{ss}$, of a system is defined as the difference between the setpoint and the system output as time approaches infinity, and the type of system given defines how the steady-state error should be determined.  Note that the term steady-state error is only meaningful when we are discussing stable, non-oscillating systems.  If the system in question is unstable then the system will never reach steady-state.

\subsection{General System}
Given a system like the one in \fig{desc_err}, we can calculate the steady-state error by defining the general \index{control error}control error: $E(s)=R(s)-Y(s)$, and applying the \index{final value theorem}final value theorem (\eqn{fin_val_thm}).

\begin{figure}[ht]
\begin{center}
\includegraphics{16_tracking_stability_figs/desc_err.pdf}
\caption{Generic Feedback System}
\label{fig:desc_err}
\end{center}
\end{figure}

\begin{equation}
e_\text{ss}=e(\infty)=\lim _{s \to 0} sE(s)=\lim _{s \to 0} s\left(R(s)-Y(s)\right)
\label{eqn:fin_val_thm}
\end{equation}

If we define our closed loop transfer function as $T_{CL}(s)$:
$$T_{CL}(s)=\frac{Y(s)}{R(s)}= \frac{D(s)G(s)}{1+D(s)G(s)}\text{,}$$
then we can simplify \eqn{fin_val_thm} to:
$$e_\text{ss}=\lim _{s \to 0} s\left(1-T_{CL}(s)\right)R(s)\text{.}$$


\subsection{Unity Feedback System} 

In the event that we are dealing with a unity feedback system (like the one in \fig{desc_err}), and we can determine the \newterm{system type}, then we can use \tbl{ss_err} (and Equations~\ref{eqn:pos_const}--\ref{eqn:acc_const}) to determine the steady state error.  The system type is characterized by the number of free integrators (or $\nicefrac{1}{s}$ terms that can be factored out), equivalently the system type is the number of poles at the origin of the $s$-plane.  Thus the system defined by \eqn{desc_sys_type} would be a type $k$ system.

\begin{equation}
T_{OL}(s) = D(s)G(s)=\frac{s+\alpha}{s^k(s^2+2\zeta\omega _ns+ \omega _n^2)}
\label{eqn:desc_sys_type}
\end{equation}


\begin{table}[ht]
\begin{center}
\caption{Steady State Errors for Given System Types and Inputs}
\label{tbl:ss_err}
\begin{tabular}{lccc}
\hline
& \multicolumn{3}{c}{Input} \\
\cline{2-4}
Type & Step ($R(s)=\nicefrac{1}{s}$) & Ramp ($R(s)=\nicefrac{1}{s^2}$) & Parabola ($R(s)=\nicefrac{1}{s^3}$) \\
\hline
Type 0 & $\nicefrac{1}{(1+K_p)}$ & $\infty$ & $\infty$ \\
Type 1 & 0 & $\nicefrac{1}{K_v}$ & $\infty$ \\
Type 2 & 0 & 0 & $\nicefrac{1}{K_a}$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{equation}
K_p=\lim_{s\to 0}T_{OL}(s)
\label{eqn:pos_const}
\end{equation}
\begin{equation}
K_v=\lim_{s\to 0}sT_{OL}(s)
\label{eqn:vel_const}
\end{equation}
\begin{equation}
K_a=\lim_{s\to 0}s^2T_{OL}(s)
\label{eqn:acc_const}
\end{equation}

\subsection{Disturbance Rejection}
Another metric we could be interested in would be how well a given system can reject disturbances (such as a wind load on a pendulum). Consider \fig{desc_err} again, this time consider the disturbance, $W(s)$, as our input. \[T_d = \frac{Y(s)}{W(s)} = \frac{G(s)}{1+D(s)G(s)}\] If the steady state output for this transfer function is a constant (non-zero) for a \emph{unit step input} then it can be said that the system is \emph{Type 0} for disturbance rejection. If the output is constant for a \emph{unit ramp input} then it is \emph{Type 1}, \&c.


\section{Determining Stability}
For some reason the term \newterm{stability} has remained undefined until this point, so here goes.  Depending on the application or course there are many ways to define stability, but in this course we will be using \newterm{bounded input-bounded output} or \newterm{BIBO} stability, which means that if a system is excited with a bounded (non-infinite) input then the output will be bounded.  The method we will use to determine stability is described below. 

\subsection{Pole Locations}
As was referenced earlier in the course, we can determine the stability of a system by examining where the poles fall on the $s$-plane.  If a pole falls on the RHP then the system is strictly unstable, and if a pole falls on the $\Im$ axis then the system is \newterm{marginally stable}.  Now, determining if the poles are all on the LHP can be difficult if the denominator polynomial is large (and there is no computer handy), but thankfully some smart old guys figured out a way to find these things (relatively) easily.


\subsection{Routh Array}
Given a complicated transfer function like \eqn{routh_tf}.

\begin{equation}
T(s)=\frac{a_0s^m+a_1s^{m-1}+a_2s^{m-2}+\cdots+a_{m-1}s+a_m}{b_0s^n+b_1s^{n-1}+b_2s^{n-2}+\cdots+b_{n-1}s+b_n}
\label{eqn:routh_tf}
\end{equation}
We can determine stability by forming the \newterm{Routh array} (\fig{routh_array}).
\begin{figure}[ht]
\begin{center}
\begin{tabular}{ccccc}
$s^n$: & $b_0$ & $b_2$ & $b_4$ & $\cdots$ \\
$s^{n-1}$: & $b_1$ & $b_3$ & $b_5$ & $\cdots$ \\
$s^{n-2}$: & $\frac{b_1b_2-b_0b_3}{b_1}=c_1$ & $\frac{b_1b_4-b_0b_5}{b_1}=c_2$ & $c_3$ & $\cdots$ \\
$s^{n-3}$: & $\frac{c_1b_3-b_1c_2}{c_1}=d_1$ & $\frac{c_1b_5-b_1c_3}{c_1}=d_2$ & $d_3$ & $\cdots$ \\
$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\ 
$s^0$: & $q_1$ & 0 & 0 & 0 \\ 
\end{tabular}
\caption{Example Routh Array for \eqn{routh_tf}}
\label{fig:routh_array}
\end{center}
\end{figure}

Now, for each sign change that occurs in the first column\footnote{The column with the $b_0$ term is the first} of the Routh array there is a single pole in the RHP of the $s$-plane.  So as long as each term in the first column is strictly positive the system is stable.

\subsection{Eigenvalues}
If we are given a system in state space form we can determine its stability by finding the eigenvalues of the $\mat{A}$ matrix, this is done by applying \eqn{eigenvals} and solving for all the values of $\lambda$.

\begin{equation}
\left|\lambda \mat{I} - \mat{A}\right| = 0
\label{eqn:eigenvals}
\end{equation}

The system is stable if all the values for $\lambda$ are negative.