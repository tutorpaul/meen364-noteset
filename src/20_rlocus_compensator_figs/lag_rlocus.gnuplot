set terminal fig
set output 'lag_rlocus.fig'
set size 1,1
set lmargin 6
set rmargin 1
#set ylabel 'Magnitude(dB)'
set ylabel 'Imaginary Axis'
set xlabel 'Real Axis'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set yrange [-30:30]
set xrange [-5:1]
#set xzeroaxis
set yzeroaxis

plot "lag_rlocus.dat" u 1:2 w lines 1,\
     "lag_rlocus.dat" u 3:4 w lines 1,\
     "lag_rlocus.dat" u 5:6 w lines 1,\
     "lag_rlocus.dat" u 7:8 w lines 1
  
set output 'lag_rlocus2.fig'
set yrange [-20:40]

plot "lag_rlocus.dat" u 9:10 w lines 1,\
     "lag_rlocus.dat" u 11:12 w lines 1,\
     "lag_rlocus.dat" u 13:14 w lines 1,\
     "lag_rlocus.dat" u 15:16 w lines 1,\
     "lag_rlocus.dat" u 17:18 w lines 1

set output 'lag_rlocus3.fig'
set size 0.5,0.5
set xrange [-0.25:0.05]
set yrange [-0.2:0.2]
     
plot "lag_rlocus.dat" u 9:10 w lines 1,\
     "lag_rlocus.dat" u 11:12 w lines 1,\
     "lag_rlocus.dat" u 13:14 w lines 1,\
     "lag_rlocus.dat" u 15:16 w lines 1,\
     "lag_rlocus.dat" u 17:18 w lines 1