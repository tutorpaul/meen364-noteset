set terminal fig
set output 'lead_rlocus.fig'
set size 1,1
set lmargin 6
set rmargin 1
#set ylabel 'Magnitude(dB)'
set ylabel 'Imaginary Axis'
set xlabel 'Real Axis'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set yrange [-30:30]
set xrange [-5:1]
#set xzeroaxis
set yzeroaxis

plot "lead_rlocus.dat" u 2:3 w lines 1,\
     "lead_rlocus.dat" u 4:5 w lines 1,\
     "lead_rlocus.dat" u 6:7 w lines 1
   
set size 0.47,0.47
set yrange [-20:20]
set xrange [-15:1]
  
set output 'lead_rlocus2.fig'
     
plot "lead_rlocus.dat" u 8:9 w lines 1,\
     "lead_rlocus.dat" u 10:11 w lines 1,\
     "lead_rlocus.dat" u 12:13 w lines 1,\
     "lead_rlocus.dat" u 14:15 w lines 1

set output 'lead_rlocus3.fig'
     
plot "lead_rlocus.dat" u 16:17 w lines 1,\
     "lead_rlocus.dat" u 18:19 w lines 1,\
     "lead_rlocus.dat" u 20:21 w lines 1,\
     "lead_rlocus.dat" u 22:23 w lines 1
     
set output 'lead_rlocus4.fig'
     
plot "lead_rlocus.dat" u 24:25 w lines 1,\
     "lead_rlocus.dat" u 26:27 w lines 1,\
     "lead_rlocus.dat" u 28:29 w lines 1,\
     "lead_rlocus.dat" u 30:31 w lines 1
     
set output 'lead_rlocus5.fig'
     
plot "lead_rlocus.dat" u 32:33 w lines 1,\
     "lead_rlocus.dat" u 34:35 w lines 1,\
     "lead_rlocus.dat" u 36:37 w lines 1,\
     "lead_rlocus.dat" u 38:39 w lines 1     
