G=tf([1 3],[1 2 5 0]);
D1=tf([1 1],[1 10]);
D2=tf([1 1],[1 20]);
D3=tf([1 1],[1 30]);
D4=tf([1 2],[1 30]);

[R4,K1]=rlocus(D4*G);
[R3,K2]=rlocus(D3*G);
K3=sort([K1 K2]);
K=K3(2:end-1);

R=rlocus(G,K);
R1=rlocus(D1*G,K);
R2=rlocus(D2*G,K);
R3=rlocus(D3*G,K);
R4=rlocus(D4*G,K);



fid= fopen('ex_rlocus.dat','wt');

fprintf(fid,'#    gain  a bunch of other crap\n');
for i=1:length(K)
    fprintf(fid,'%12.4E  ', K(i));
    for j=1:3
        fprintf(fid,'%12.4E  %12.4E  ', real(R(j,i)), imag(R(j,i)) );
    end
    for j=1:4
        fprintf(fid,'%12.4E  %12.4E  ', real(R1(j,i)), imag(R1(j,i)) );
    end
    for j=1:4
        fprintf(fid,'%12.4E  %12.4E  ', real(R2(j,i)), imag(R2(j,i)) );
    end
    for j=1:4
        fprintf(fid,'%12.4E  %12.4E  ', real(R3(j,i)), imag(R3(j,i)) );
    end
    for j=1:4
        fprintf(fid,'%12.4E  %12.4E  ', real(R4(j,i)), imag(R4(j,i)) );
    end
    
    fprintf(fid,'\n');
end
fclose(fid);