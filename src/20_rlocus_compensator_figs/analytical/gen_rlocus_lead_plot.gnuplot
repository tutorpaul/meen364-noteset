set terminal fig

#set size 1,1
set size ratio -1
set lmargin 6
set rmargin 1
#set ylabel 'Magnitude(dB)'
set y2label 'Imaginary Axis'
set xlabel 'Real Axis'
#set logscale x
#set format x "10^%L"
#set format x ""
set nokey
set autoscale
set samples 1000

set y2range [-1:9]
set yrange [-1:9]
set xrange [-10:0]
#set arrow from -2.25,-1 to -2.25,9 nohead
#set arrow from -0.5,0 to -4,0 nohead
set xzeroaxis
#set yzeroaxis


set noytics
set y2tics mirror

set output 'ex_rlocus_lead_uncomp.fig'
plot "ex_rlocus_lead_uncomp.dat" u 2:3 w lines lt 5 ,\
     "ex_rlocus_lead_uncomp.dat" u 4:5 w lines lt 5 ,\
     "ex_rlocus_lead_uncomp.dat" u ( ((int($0))==0)? $2 :1/0):3 w points pt 2 ,\
     "ex_rlocus_lead_uncomp.dat" u ( ((int($0))==0)? $4 :1/0):5 w points pt 2


set output 'ex_rlocus_lead_const.fig'

plot "ex_rlocus_lead_uncomp.dat" u 2:3 w lines lt 5 ,\
     "ex_rlocus_lead_uncomp.dat" u 4:5 w lines lt 5 ,\
     "ex_rlocus_lead_uncomp.dat" u ( ((int($0))==0)? $2 :1/0):3 w points pt 2,\
     "ex_rlocus_lead_uncomp.dat" u ( ((int($0))==0)? $4 :1/0):5 w points pt 2

replot sqrt(8**2-x**2) w lines lt 3, -sqrt(8**2-x**2) w lines lt 3, -1/(tan(asin(.4)))*x w lines lt 3
replot '-' w points pt 4
-3.2 7.3321
E

set output 'ex_rlocus_lead_subtend.fig'

plot "ex_rlocus_lead.dat" u ( ((int($0))==0)? $2 :1/0):3 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==0)? $4 :1/0):5 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==0)? $6 :1/0):7 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==15)? $2 :1/0):3 w points pt 6

#replot sqrt(8**2-x**2) w lines lt 3, -sqrt(8**2-x**2) w lines lt 3, -1/(tan(asin(.4)))*x w lines lt 3
replot '-' w points pt 4
-3.2 7.3321
E

set output 'ex_rlocus_lead_comp.fig'
plot "ex_rlocus_lead.dat" u 2:3 w lines lt 1 ,\
     "ex_rlocus_lead.dat" u 4:5 w lines lt 1 ,\
     "ex_rlocus_lead.dat" u 6:7 w lines lt 1 ,\
     "ex_rlocus_lead.dat" u ( ((int($0))==0)? $2 :1/0):3 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==0)? $4 :1/0):5 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==0)? $6 :1/0):7 w points pt 2,\
     "ex_rlocus_lead.dat" u ( ((int($0))==15)? $2 :1/0):3 w points pt 6
     
replot sqrt(8**2-x**2) w lines lt 3, -sqrt(8**2-x**2) w lines lt 3, -1/(tan(asin(.4)))*x w lines lt 3
replot '-' w points pt 4
-3.2 7.3321
E