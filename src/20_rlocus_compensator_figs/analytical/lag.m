clear all;
G = tf(1,[1 3 0]);
figure(1);
rlocus(G);

s = (-2.5+2j);

p = 0.01;

kz = 20*p;
syms z k;
k_zero =k+(s*(s+3)*(s+p))/(s+z);

S = solve(subs(k_zero),'k*z = .1');

z = eval(real(S.z));
%z = .0532;
%gain = real(k_zero)^2+imag(k_zero)^2;

%some magic happens

%k = 192;
%z = kz/k;

D = tf([1 z],[1 p]);

figure(2);
rlocus(D*G);