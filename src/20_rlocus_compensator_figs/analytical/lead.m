G = tf(1,[2 9 4]);
%figure(1);
[R,K] = rlocus(G);
fid= fopen('ex_rlocus_lead_uncomp.dat','wt');

fprintf(fid,'#    gain    real1    imag1   real2     imag2\n');
for i=1:length(K)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E\n',...
        K(i),real(R(1,i)),imag(R(1,i)),real(R(2,i)),imag(R(2,i)));
end
fclose(fid);

zeta = .4;
omega_n = 8;

re = zeta*omega_n;
im = omega_n*sqrt(1-zeta^2);

z = 5;

theta_1=180 - atand((re-0.5)/im);
theta_2= atand((4-re)/im);
phi_z=atand((z-re)/im);

theta_p=180-theta_1-theta_2+phi_z;

p = tand(theta_p)*im + re;

C = tf([1 z],[1 p]);

fid= fopen('ex_rlocus_lead.dat','wt');

%figure(2);
[R,K]=rlocus(C*G);

fprintf(fid,'#    gain    real1    imag1   real2     imag2  real3   imag3\n');
for i=1:length(K)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E\n',...
        K(i),real(R(1,i)),imag(R(1,i)),real(R(2,i)),imag(R(2,i)),real(R(3,i)),imag(R(3,i)));
end
fclose(fid);

s=re*1j*im;

%k=fzero(@(k) (s+z)*k +(2*s+1)*(s+4)*(s+p),74);
figure(1);
rlocus(C*G,linspace(75,170,500));