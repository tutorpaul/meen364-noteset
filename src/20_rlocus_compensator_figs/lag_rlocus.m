G=tf([1 3],[1 2 5 0]);
% Lead Compensators
D1=tf([1 1],[1 10]);
D2=tf([1 1],[1 20]);
D3=tf([1 1],[1 30]);
D4=tf([1 2],[1 30]);
% Lag Compensators
D5=tf([1 0.1],[1 0.01]);
D6=tf([1 0.01],[1 0.001]);

K=[linspace(0,3,30) linspace(5,11,30) linspace(47,900,50) 3.5557e05 inf];

[R5,K1]=rlocus(D5*D1*G);

K=sort([K K1]);
K=K(2:end-1);
R5=rlocus(D5*D1*G,K);
rlocus(D5*D1*G,K);
R1=rlocus(D1*G,K);


fid= fopen('lag_rlocus.dat','wt');

fprintf(fid,'#    gain  a bunch of other crap\n');
for i=1:length(K)
    for j=1:4
        fprintf(fid,'%12.4E  %12.4E  ', real(R1(j,i)), imag(R1(j,i)) );
    end
    for j=1:5
        fprintf(fid,'%12.4E  %12.4E  ', real(R5(j,i)), imag(R5(j,i)) );
    end
    
    fprintf(fid,'\n');
end
fclose(fid);