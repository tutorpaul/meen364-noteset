\section{Introduction}
Using \simulink\ requires a bit of a perspective change from dealing with \texttt{ODE45}.  Rather than thinking about the model as a whole you need to think about the individual states.  In \simulink\ you will be creating a block diagram that will represent the state space model.  In this primer I will be referencing the example in \S\ref{sec:mech_example} (pg.\ \pageref{sec:mech_example}).  

\section{Building the Model}
Go ahead and open \matlab, in the command prompt issue the command \verb!simulink!; this will initialize \simulink\ for use.  When the \simulink\ library opens open a new model by clicking $\texttt{File} \rightarrow \texttt{New} \rightarrow \texttt{Model}$.  

The first thing we will do is explore the \simulink\ library; depending on your system and version it will look different, but the guts are mostly the same.  Find the heading titled \texttt{Continuous}, and open it (click or double-click), the sub-library should have a number of blocks: integrator, differentiator, transfer function, etc. Now open the \texttt{Math Operations} sub-library and have a look around.

Now that we are all comfortable seeing these blocks we will start messing with them.   Look back in the \texttt{Continuous} sub-library and drag an \texttt{Integrator} to your model.  This integrator will integrate the $\ddot \theta_1$ signal to $\dot \theta_1$ (it would be best to name it appropriately by double-clicking and typing\dots\ with the keyboard).  Drag another \texttt{Integrator} to your model, this will integrate $\dot \theta_1$ signal to $\theta_1$ (don't forget to rename it), you could also ctrl-drag the one you already added to the model to duplicate it.  Connect the two blocks by dragging from the right end (outlet) of the first integrator to the left end (inlet) of the second integrator.  Your model should look like \fig{model_1}.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_1.pdf}}
\caption{New \simulink\ Model, Step 1}
\label{fig:model_1}
\end{center}
\end{figure}

Now you need to define what makes up the $\ddot\theta_1$ signal:
$$\ddot \theta _1 =\frac{- cr_1^2 \dot \theta _1 +cr_1r_2 \dot \theta _2 - (k_1+k_2) r_1^2 \theta _1 + k_2r_1r_2\theta_2}{J_1}. $$
Before you can put this in \simulink\ you will need to define integrators to allow access to the $\dot\theta_2$ and $\theta_2$ signals (just like what we did above).  After you have gained access to the $\theta_2$ signals you should add a \texttt{Gain} block to your model, these can be found in the \texttt{Math Operations} sub-library. 

Try double-clicking on the \texttt{Gain} block you dragged over, a dialog should appear with a few text fields on it.  One of the fields should be titled `Gain', in that text field you can enter the numerical value of the desired gain, a variable name, or an equation of several variables.\footnote{The model can access any global variable from the workspace}.  So, go ahead and enter \verb!(k1+k2)*r1^2! then click `OK'.  Drag the \texttt{Gain} block toward the location of the $\theta_1$ signal, and connect it up.  Now go back to the \texttt{Math Operations} sub-library and drag an \texttt{Add} block to your model, and double-click it.  You can change the number of inlets by adding more plus or minus signs to the `List of signs' text box, go ahead and change it to \verb!--++! then click `OK' (you can resize the block by dragging one of the corners).  Drag the \texttt{Add} block so it is positioned before the $\ddot \theta_1$ signal to $\dot \theta_1$ integrator.  Now connect the 
gain block to one of the minus signs on the \texttt{Add} block.  Your model should now look like \fig{model_2}.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_2.pdf}}
\caption{New \simulink\ Model, Step 2}
\label{fig:model_2}
\end{center}
\end{figure}

From the \texttt{Math Operations} sub-library, drag another \texttt{Gain} block to your model, place it between the \texttt{Add} block and the $\ddot \theta_1$ \texttt{Integrator} block.  This will be where you divide by $J_1$, so enter the variable where necessary (you should put \verb!1/J1! in the `Gain' field) and connect it up.  \fig{model_3} shows how your model should look.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_3.pdf}}
\caption{New \simulink\ Model, Step 3}
\label{fig:model_3}
\end{center}
\end{figure}

Drag over another \texttt{Gain} block to the area between and above the top two \texttt{Integrator} blocks, while it is selected press ctrl+R to rotate the block until it is pointing up.  Put the appropriate equation in the block and connect it to a minus sign on the \texttt{Add} block.  While holding control drag a wire from the existing wire between the two \texttt{Integrator} blocks to the new \texttt{Gain} blocks.  Using the techniques discussed so far finish the $\ddot\theta_1$ signal and check your model against \fig{model_4}.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_4.pdf}}
\caption{New \simulink\ Model, Step 4}
\label{fig:model_4}
\end{center}
\end{figure}

Now, create the $\ddot\theta_2$ signal, which requires access to $x$.  While you're at it go ahead and make the $\ddot x$ signal.  Note that the input, $f$, will need to be created.  Go ahead and open the \texttt{Sources} sub-library and find a \texttt{Constant} block, drag it over, double-click, change the value to $0.5$, and hook it up to the \texttt{Add} block.  Your model should now resemble \fig{model_5}, if it looks good go ahead and run the simulation by clicking \texttt{Start} in the \texttt{Simulation} menu (find it in the menu bar alongside \texttt{File} and \texttt{Edit}).  Should your model have initial conditions they can be added by double clicking on the \texttt{Integrator} and filling in the \texttt{Initial Condition} field.  Make sure you provide the appropriate initial conditions for each \texttt{Integrator} in your model.

\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_5.pdf}}
\caption{New \simulink\ Model, Step 5}
\label{fig:model_5}
\end{center}
\end{figure}

In the \texttt{Sinks} sub-library you will find a block called \texttt{Scope}, you can use this block to observe any signal in the model by connecting it and double clicking after running the simulation.  If the model calls for it you can also have a time-varying force, in the \texttt{Sources} sub-library find the \texttt{Clock} block and drag one to the same area you put the \texttt{Constant} block.  Delete the \texttt{Constant} block and replace it with a \texttt{Gain}, then connect the output of the \texttt{Clock} to the input of the \texttt{Gain}.  Now, from the \texttt{Sinks} sub-library drag over a \texttt{To Workspace} block and connect it to a signal you want to capture, $\dot x$ for instance. Double-click on it and change the `Variable Name' field to something appropriate, also change the `Save Format' pull-down menu selection to `Array'.  Copy the \texttt{To Workspace} to a location below the model and change its `Variable Name' to ``time,'' then copy the \texttt{Clock} to an area near the new \texttt{To Workspace} block and connect the two as was done in \fig{model_7}.  Once you run the simulation again you will have access to two new arrays corresponding to the variables names you put in the \texttt{To Workspace} blocks.


\begin{figure}[ht]
\begin{center}
\fbox{\includegraphics{AP_simulink_figs/model_7.pdf}}
\caption{New \simulink\ Model, Step 6}
\label{fig:model_7}
\end{center}
\end{figure}
 
Save your model as ``mymodel.mdl'' and return to \matlab, by executing the command \verb!sim('mymodel.mdl')! you can run the model without opening \simulink.  This makes using a \simulink\ model as part of custom function possible.


