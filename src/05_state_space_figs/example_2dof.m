tspan=0:.001:2;

global k1 k2 c1 c2 m1 m2;

k1=10000;
k2=20000;
c1=3;
c2=5;
m1=50;
m2=30;

x0=[0 0 0 0]';

[t,x]=ode45('example_2dof_ode',tspan,x0);

output=x(:,1);

plot(t,output);