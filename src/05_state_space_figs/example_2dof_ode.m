function xnew=example_2dof_ode(t,x)

global k1 k2 c1 c2 m1 m2;

f=sin(10*2*pi*t);

xnew=zeros(4,1);

xnew(1)=x(2);
xnew(2)=-x(1)*(k1+k2)/m1-x(2)*(c1+c2)/m1+x(3)*k2/m1+x(4)*c2/m1;
xnew(3)=x(4);
xnew(4)=x(1)*k2/m2 + x(2)*c2/m2 - x(3)*k2/m2 - x(4)*c2/m2 +f;
