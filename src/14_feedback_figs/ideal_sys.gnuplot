set terminal fig

set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Magnitude (dB)'
set xlabel 'Frequency (rad/sec)'
set logscale x
set format x "10^%L"
set zeroaxis x

set output 'ideal_sys.fig'
plot "ideal_sys.dat" u 1:2 title '|C(s)G(s)|' w lines 1

