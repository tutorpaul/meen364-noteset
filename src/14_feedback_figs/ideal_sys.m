A=tf(1,[1 0]);
B=tf(1,[1 5]);
C=tf([1 100],1);

[mag,phase,w]=bode(10*A*B*C);


fid= fopen('ideal_sys.dat','wt');

fprintf(fid,'#  frequency  magnitude phase \n');
for i=1:length(w)
    fprintf(fid,'%12.4E  %12.4E  %12.4E  ', w(i), 20*log10(mag(i)), phase(i) );
    
    fprintf(fid,'\n');
end
fclose(fid);

