%!TEX root = 00_complete.tex

\section{Motivation}
Engineers (and Mathematicians) use \newterm{state space} representation to simplify the simulation of differential equation systems, and you have un-knowingly already used this representation.  Anytime you used the \texttt{ODE45}\marginpar{\texttt{ODE45}}\index{ODE45@\texttt{ODE45}} function in \matlab\ (\course{Dynamics and Vibrations}, \course{Numerical Analysis}) you were using the state space form.
\section{Formulation}
The state space form consists of a system of first order differential equations, but---as you know---when modeling mechanical (and some electrical) systems you will encounter second order differential equations.  To rectify this problem you need to identify the \newterm{state variables}, and generate a set of first order differential equations in terms of the states which are exactly equivalent to the original system.  The states are variables which represent the essential data for reproducing the current state of a real system.  The number of state variables required depends on the number (and type) of degrees of freedom in a system.  For each mechanical degree of freedom there will be two state  variables, namely the position and velocity of each body.\footnote{Electrical degrees of freedom will be discussed in a later topic}  For instance, if we were to model a simple mass, spring, and damper system we would get the following equation of motion:
\begin{equation}
\ddot x(t) +2 \zeta \omega_n \dot x(t)+ \omega_n^2 x(t)=f(t)\text{,}
\label{eqn:example_system}
\end{equation}
and our states would be: the position of the mass, $x(t)$, and the velocity of the mass, $\dot x(t)$.  Now that the state variables are known we can form the \newterm{state vector}:
$$\mathbf{X}(t)=\left\{\begin{array}{c} \dot x(t) \\ x(t) \\ \end{array}\right\}=\left\{\begin{array}{c} \mathbf{x}_1(t) \\ \mathbf{x}_2(t) \\ \end{array}\right\}\text{,}$$
which implies:
$$\dot {\mathbf{X}}(t)=\left\{\begin{array}{c} \ddot x(t) \\ \dot x(t) \\ \end{array}\right\}=\left\{\begin{array}{c} \dot{\mathbf{x}}_1(t) \\ \dot{\mathbf{x}}_2(t) \\ \end{array}\right\}\text{.}$$

Now that our states are defined we need to determine the input to the system.  The input will be the force that tries to drive the system out of equilibrium, in \eqn{example_system} the input is the time-varying force $f(t)$.  This input is usually referred to as $u(t)$, and in the event that there are multiple inputs we would arrange them into a vector.

The next step in finding the state space representation of a system is to define the \newterm{state matrix}, $\mat{A}$, and the \newterm{input matrix}, $\mat{B}$.  These two matrices
are defined such that \eqn{ss_input} is equivalent to the original system.

\begin{equation}
\frac{d}{dt} \mathbf{X}(t)=\mat{A}\mathbf{X}(t)+\mat{B}u(t)
\label{eqn:ss_input}
\end{equation}

For the system defined by \eqn{example_system}, the previous equation (the \newterm{input equation}) would be the following:
\begin{equation}
\left\{\begin{array}{c} \ddot x(t) \\ \dot x(t) \\ \end{array}\right\} = \left[ \begin{array}{cc} -2\zeta\omega_n & -\omega_n^2 \\ 1 & 0 \\ \end{array}\right] \left\{\begin{array}{c} \dot x(t) \\  x(t) \\ \end{array}\right\} + \left[\begin{array}{c} 1 \\ 0 \\ \end{array}\right]f(t)\text{.}
\label{eqn:input_eqn}
\end{equation}
The first row of \eqn{input_eqn} translates exactly to \eqn{example_system}, but the second row translates to $\dot x(t) = \dot x(t)$, which---at first glance---appears completely useless.  However, looking at the context shows us that it ties the system together and indicates the relationship between the two state variables. In reality the second row is saying $\dot q_2(t) = q_1(t)$.

The last thing we need is the \newterm{output equation}, which defines what our output from the state space system will be; that is, what we want to observe about the system.  To define the output equation we have to first figure out what our output should be. Let's assume that we only cared about the position of the mass in the system defined by \eqn{example_system}, that means that our desired output would be $x(t)$, which we called $q_2(t)$.  Now we can define our \newterm{output matrix}, $\mat{C}$, and our \newterm{feed-forward matrix},\footnote{The feed-forward term is used to feed the input directly to the output} $\mat{D}$, so that our output, $y(t)$, is equivalent to our desired output (the format of the output equation is given as \eqn{ss_output}).
\begin{equation}
y(t)=\mat{C}\mathbf{X}(t)+\mat{D}u(t)
\label{eqn:ss_output}
\end{equation}
Thus the output equation for our example would be:
\begin{equation}
y(t)=\left[\begin{array}{cc} 0 & 1 \\ \end{array}\right]\left\{\begin{array}{c} \dot x(t) \\  x(t) \\ \end{array}\right\}+ 0u(t)
\label{eqn:output_eqn}
\end{equation}

Equations~\ref{eqn:input_eqn}~and~\ref{eqn:output_eqn} form the complete state space model.

\subsection{Understanding States}
In the last section we defined the states of a mechanical system as the \emph{position} and \emph{velocity} of the independent degrees of freedom of the system, but why should they be defined as such?  According to the equation of motion of a generic system (like \eqn{example_system}) we can predict with certainty the system's acceleration if we know its position and velocity (taking the current time $t$ and the nature of the external force $f(t)$ as known), no additional information about the systems configuration is required to replicate the exact condition of the system.

Another way to think about states is in terms of the energy contained by the system at any instant, if we know the energy of the system they we can replicate its condition exactly.  As was discussed in \course{Dynamics and Vibrations} the total energy of a system can be understood in terms of \newterm{kinetic energy}, $T$, and \newterm{potential energy}, $V$.  Kinetic energy is the energy of motion, and it depends on physical constants of the system, and its \emph{velocity}.  Potential energy (for our purposes) is separated into two categories: gravitational potential, and spring potential.  Each of these is defined by the system \emph{position}.  So, to understand the energy of a system we need to know its position and velocity, thus this information is also sufficient to define the system's state.

\subsection{Matrix Dimensions}
The sizes of the matrices used to define the state space model are dictated by the numbers of inputs and outputs.  If there are $n$ states, $m$ inputs, and $p$ outputs to be defined then the sizes of the matrices are as follows:

$$\frac{d}{dt} \mathbf{X}(t)=\underbrace{\mat{A}}_{[n\times n]} \mathbf{X}(t)+\underbrace{\mat{B}}_{[n\times m]}u(t)$$
$$y(t)=\underbrace{\mat{C}}_{[p\times n]}\mathbf{X}(t)+\underbrace{\mat{D}}_{[p\times m]}u(t)$$


\section{Example}
Let's look at the example given as \fig{two_dof} whose equations of motion are given as \eqn{two_dof}.  For this example we will take the output to be the displacement of $k_3$.

\begin{figure}[ht]
\begin{center}
\includegraphics{05_state_space_figs/2dof.pdf}
\caption{Simple Two Degree-of-Freedom System}
\label{fig:two_dof}
\end{center}
\end{figure}

\begin{equation}
\begin{array}{c}
m_1\ddot x_1 + (c_1+c_3)\dot x_1 -c_3\dot x_2 +(k_1+k_3) x_1 -c_3x_2 = 0 \\
m_2\ddot x_2 - c_3\dot x_1 + (c_2+c_3)\dot x_2 -k_3x_1 + (k_2+k_3)x_2 = f(t)
\end{array}
\label{eqn:two_dof}
\end{equation}

First, we will want to define our states: $\dot x_1$, $x_1$, $\dot x_2$, and $x_2$.  While we're at it let's define our input: $f(t)$, and our desired output: $x_2-x_1$.  Now we can start to re-arrange our equations of motion into the form required by the input equation.

$$\begin{array}{c}
\ddot x_1 = \frac{-(c_1+c_3)}{m_1} \dot x_1 - \frac{(k_1+k_3)}{m_1} x_1+\frac{c_3}{m_1} \dot x_2  +\frac{k_3}{m_1} x_2 \\
\ddot x_2 = \frac{c_3}{m_2} \dot x_1  +\frac{k_3}{m_2} x_1 -\frac{(c_2+c_3)}{m_2} \dot x_2 -\frac{(k_2+k_3)}{m_2} x_2 + \frac{f(t)}{m_2}
\end{array}$$
These can be easily inserted into the input equation.
$$
\left\{\begin{array}{c} \ddot x_1 \\ \dot x_1 \\ \ddot x_2 \\ \dot x_2 \end{array}\right\} =
\left[\begin{array}{cccc} \frac{-(c_1+c_3)}{m_1} & \frac{-(k_1+k_3)}{m_1} & \frac{c_3}{m_1}& \frac{k_3}{m_1}\\
1 & 0 & 0 & 0 \\
\frac{c_3}{m_2} & \frac{k_3}{m_2} & \frac{-(c_2+c_3)}{m_2} & \frac{-(k_2+k_3)}{m_2} \\
0 & 0 & 1 & 0 \end{array}\right]\left\{\begin{array}{c} \dot x_1 \\ x_1 \\ \dot x_2 \\ x_2 \end{array}\right\} + \left[\begin{array}{c} 0 \\ 0 \\ \frac{1}{m_2} \\ 0 \end{array}\right]f(t)
$$
Lastly, we will make our output equation.  Remember: the problem is not complete without the output equation.
$$y(t)=\left[\begin{array}{cccc} 0 & -1 & 0 & 1 \end{array}\right] \left\{\begin{array}{c} \dot x_1 \\ x_1 \\ \dot x_2 \\ x_2 \end{array}\right\} +0f(t) $$
