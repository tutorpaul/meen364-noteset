set terminal fig
set output 'aliasing_example1.fig'
set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Time'
set xrange [0:0.02]

plot "aliasing1.dat" i 0 u 1:2 t "Original Signal (200Hz)" w lines 1,\
     "aliasing1.dat" i 1 u 1:2 t "Good Samples (600Hz)" w points 1,\
     "aliasing1.dat" i 2 u 1:2 t "Aliased Samples (375Hz)" w points 4,\
     "aliasing1.dat" i 0 u 3:4 t "Aliased Signal (175Hz)" w lines 2
