%% Fig 1

T=0.02;

t1=(0:1/(10000):T);
t2=(0:1/(600):T);
t3=(0:1/(375):T);
t4=(0:1/(10000):T);

y1=sin(200*2*pi*t1);
y2=sin(200*2*pi*t2);
y3=sin(200*2*pi*t3);
y4=-sin(175*2*pi*t4);

figure(1)
plot(t1,y1,'b',t2,y2,'.');
hold on;
axis tight;

plot(t3,y3,'ko',t4,y4,'k','MarkerSize',15);
legend('Original Signal (200Hz)','Good Samples (600Hz)','Aliased Samples (375Hz)','Aliased Signal (175Hz)');
set(legend,'FontSize',12);
xlabel('Time','FontSize',12);
ylabel('Amplitude','FontSize',12);
hold off;

fid= fopen('aliasing1.dat','wt');

fprintf(fid,'#  Time1 signal1  time4  signal2   \n');
for i=1:length(t1)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E \n',...
        t1(i),y1(i),t4(i),y4(i));
end

fprintf(fid,'\n\n\n\n#  Time2 points2 \n');
for i=1:length(t2)
    fprintf(fid,'%12.4E %12.4E \n',...
        t2(i),y2(i));
end

fprintf(fid,'\n\n\n\n#  Time3  points3 \n');
for i=1:length(t3)
    fprintf(fid,'%12.4E %12.4E \n',...
        t3(i),y3(i));
end

fclose(fid);

%% Fig 2

T=0.02;

t1=(0:1/(100000):T);
t2=(0:1/(2000):T);
t3=(0:1/(500):T);
t4=(0:1/(100000):T);

y1=sin(100*2*pi*t1)+sin(450*2*pi*t1);
y2=sin(100*2*pi*t2)+sin(450*2*pi*t2);
y3=sin(100*2*pi*t3)+sin(450*2*pi*t3);
y4=sin(100*2*pi*t4)-sin(50*2*pi*t4);

figure(2)
plot(t1,y1,'b',t2,y2,'.');
hold on;
axis tight;

plot(t3,y3,'ko',t4,y4,'k','MarkerSize',15);
legend('Original Signal (200Hz)','Good Samples (600Hz)','Aliased Samples (375Hz)','Aliased Signal (175Hz)');
set(legend,'FontSize',12);
xlabel('Time','FontSize',12);
ylabel('Amplitude','FontSize',12);
hold off;



fid= fopen('aliasing2.dat','wt');

fprintf(fid,'#  Time1 signal1  time4  signal2   \n');
for i=1:length(t1)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E \n',...
        t1(i),y1(i),t4(i),y4(i));
end

fprintf(fid,'\n\n\n\n#  Time2 points2 \n');
for i=1:length(t2)
    fprintf(fid,'%12.4E %12.4E \n',...
        t2(i),y2(i));
end

fprintf(fid,'\n\n\n\n#  Time3  points3 \n');
for i=1:length(t3)
    fprintf(fid,'%12.4E %12.4E \n',...
        t3(i),y3(i));
end

fclose(fid);