function Qdot=primer_ode45(t,Q)
global r1 r2 c k1 k2 k3 J1 J2 m;

f=0.5*t;

Qdot=zeros(6,1);

Qdot(1)=Q(2);
Qdot(2)=-(k1+k2)*r1^2/J1*Q(1)-c*r1^2/J1*Q(2)...
    +k2*r1*r2/J1*Q(3)+c*r1*r2/J1*Q(4);
Qdot(3)=Q(4);
Qdot(4)=k2*r1*r2/J2*Q(1)+c*r1*r2/J2*Q(2)...
    -(k2+k3)*r2^2/J2*Q(3)-c*r2^2/J2*Q(4)+k3*r2/J2*Q(5);
Qdot(5)=Q(6);
Qdot(6)=k3*r2/m*Q(3)-k3/m*Q(5)+1/m*f;

end
