global r1 r2 c k1 k2 k3 J1 J2 m;

r1=1;
r2=1.3;
c=.005;
k1=10;
k2=12;
k3=15;
J1=1;
J2=1.4;
m=10;

tspan=0:.01:2;
Q0=[0 0 0 0 -1 0];

[t,Q]=ode45('primer_ode45',tspan,Q0);

plot(t,Q(:,6));