% Fluidic Systems
\section{Fluidic Systems}
Fluidic systems are directly parallel to electrical systems, and they are modeled in the same way.  The only change is some nomenclature, namely: the current (from electrical systems) is known as the \newterm{flowrate} ($Q$), and the voltage is called the \newterm{pressure} ($P$).
 % Capacitors
\subsection{Fluid Capacitors}
A \newterm{fluid capacitor} is also known as a tank, and fluid flows into the capacitor based on the pressure difference between the fluid in the tank and the inlet.  The flowrate in a tank can be modeled using \eqn{element_cap}.
\begin{equation}
Q_C=C_f\frac{dP_C}{dt}
\label{eqn:element_cap}
\end{equation}

In the previous equation $Q_C$ is the flowrate into (or out of) the capacitor, $C_f$ is the fluid capacitance, and $P_C$ is the pressure difference across the inlet.
 % Inertors
\subsection{Fluid Inertors}
An \newterm{inertor} is a device that resists the change in flowrate within a system.  Think of a paddlewheel (with rotational inertia) that is capable of blocking a flowing stream, now imagine that it is spinning with the same rate the water is flowing.  If you were to suddenly increase the flowrate of the water it would be met with resistance from the paddlewheel until the wheel could speed up, likewise if you decreased the flowrate the paddlewheel would try to keep pushing the water along at the original rate until the wheel can slow down.  \eqn{element_inert} shows you how to model this behavior mathematically.
\begin{equation}
P_I=I_f\frac{dQ_I}{dt}
\label{eqn:element_inert}
\end{equation}

The $P_I$ represents the pressure difference across the element, $Q_I$ represents the flowrate through the element, and $I_f$ is the fluid inertance of the element.
 % Resistors
\subsection{Fluid Resistors}
\newterms{Fluidic resistance}{fluidic resistance} is present in every long span of pipe due to friction with the walls.  The elemental equation is given as \eqn{element_resist}, where $R_f$ is the fluidic resistance.
\begin{equation}
P_R=R_fQ_R
\label{eqn:element_resist}
\end{equation}
 % Sources
\subsection{Fluid Sources}
There are two types of sources that can be modeled in fluidic systems: \emph{Pressure regulated}\marginpar{pressure regulated source}\index{sources!pressure regulated}, and \emph{Flowrate regulated}\marginpar{flowrate regulated source}\index{sources!flowrate regulated}.  A pressure regulated source will provide a specific fluid pressure regardless of flowrate, the flowrate regulated source does exactly the opposite.  These are directly related to the voltage and current sources (respectively) in electrical systems.
 % Modeling
\subsection{Modeling Strategy}
The modeling strategy for a fluidic system is the same as a comparable electrical system.  The Kirchoff's laws apply in the same manner (voltage is pressure and current is flowrate) although in a fluidic system they are called the interconnection laws.

% Thermal Systems
\section{Thermal Systems}
Thermal systems are dissimilar from the systems studied thus far because they aren't modeled as lumped parameter systems.  There aren't resistors, inductors, and capacitors that facilitate modeling, instead you use energy balance equations to model the heat transfer.  In the scope of this course you will assume each systems is lossless (energy is conserved).  There are three modes of heat transfer which should be familiar to you, they are: conduction, convection, and radiation.
 % Conduction
\subsection{Conduction}
\newterms{Conduction}{conduction} occurs between two solid bodies in direct contact which are at different temperatures.  You should remember from \course{Principles of Thermodynamics} that the two bodies will eventually equalize in temperature.  Conduction can also define the energy transfer within a single body that results from some temperature difference.  This mode of heat transfer is modeled using \eqn{therm_cond}.
\begin{equation}
Q_{hk}=\left(\frac{kA}{L}\right)\left(T_1-T_2\right)
\label{eqn:therm_cond}
\end{equation}

In the previous equation $Q_{hk}$ is the heat flow rate due to conduction, $k$ is the thermal conductivity of the material, $A$ is the cross-sectional area, the two $T$ terms are temperatures at some point in the body, and $L$ is the distance between the points where $T_1$ and $T_2$ are measured.
 % Convection
\subsection{Convection}
\newterms{Convection}{convection} occurs between a solid and a flowing fluid (typically air or water). The heat transfer rate equation for convection is given as \eqn{therm_conv}, where $Q_{hc}$ is the heat flow rate, $h_c$ is the convective heat transfer coefficient, $A$ is the contact area, $T_s$ is the temperature of the solid, and $T_f$ is the fluid temperature.
\begin{equation}
Q_{hc}=h_cA\left(T_s-T_f\right)
\label{eqn:therm_conv}
\end{equation}
 % Radiation
\subsection{Radiation}
\newterms{Radiative}{radiation} heat transfer is best defined by that warm feeling you get when you sit near a space heater, where your skin feels quite a bit warmer than the air around you.  The relationship that defines the rate of heat transfer is shown in \eqn{therm_rad}, where $Q_{hr}$ is the radiative heat transfer rate, $\sigma $ is the Stefan-Boltzmann constant ($\unitfrac[5.667\times 10^{-8}]{W}{m^2k^4}$), $F_E$ is the emissivity of the radiating body, $F_A$ is a fraction that defines the amount of radiation that reaches the receiving body, $A$ is the area of the emitting body, and the two $T$ terms are the two bodies' temperatures (these must be in absolute units).
\begin{equation}
Q_{hr}=\sigma F_EF_AA\left(T_1^4-T_2^4\right)
\label{eqn:therm_rad}
\end{equation}

 
 % Modeling strategy
\subsection{Modeling Strategy}
As was said earlier, you aren't working with a lumped parameter system, so the strategy when modeling is different.  What you are going to do is define an \newterm{energy balance}, which is where you sum the heat flow rates, the heat generation, and the work done by the system (be mindful of signs), and set it equal to the rate of energy storage (which is usually an unknown quantity).

\begin{equation}
	\frac{dT}{dt} = \frac{1}{m C_P}\sum Q
\end{equation}


\section{Example}
Let's consider the problem given in \fig{fluid_example}, which has two inputs ($q_\text{in,1}$ and $q_\text{in,2}$).  The first thing we will want to do is define an absolute pressure, $P_0$, and the pressures at the bottom of each tank: $P_1$, $P_2$, and $P_3$.  Also, we will define some flowrates: $q_1$ through $R_1$, and $Q$ through the inertor; \fig{fluid_example_marked} shows the definitions.
\begin{figure}[ht]
\begin{center}
\includegraphics{08_thermal_modeling_figs/fluid_example.pdf}
\caption{Example Fluidic System}
\label{fig:fluid_example}
\end{center}
\end{figure}

To continue this problem we need to define our states: $P_{10}$, $P_{20}$, $P_{30}$, and $Q$ ($P_{10}\equiv P_1 - P_0$).  These represent the pressures in the capacitors (voltage across a capacitor), and the flowrate through the inertor (current through an inductor).
\begin{figure}[ht]
\begin{center}
\includegraphics{08_thermal_modeling_figs/fluid_example_marked.pdf}
\caption{Marked Up Example Fluidic System}
\label{fig:fluid_example_marked}
\end{center}
\end{figure}

Now we will write the elemental equations (Equations~\ref{eqn:ex_cap1}--\ref{eqn:ex_cap3}), just as we would do for an electrical system.  However, defining the inertor's elemental equation requires us to separate the resistor from the inertor, as is done in \fig{example_inertor}.

\begin{equation}
C_1\dot P_{10} =q_{c_1}= q_\text{in,1}-q_1
\label{eqn:ex_cap1}
\end{equation}

\begin{equation}
C_2\dot P_{20}=q_{c_2}=q_1-Q
\label{eqn:ex_cap2}
\end{equation}

\begin{equation}
C_3\dot P_{30}=q_{c_3}=q_\text{in,2}+Q-q_\text{out}
\label{eqn:ex_cap3}
\end{equation}

\begin{figure}[ht]
\begin{center}
\includegraphics{08_thermal_modeling_figs/fluid_example_zoom.pdf}
\caption{Exploded View of Fluid Resistor and Inertor}
\label{fig:example_inertor}
\end{center}
\end{figure}
Now that we have seen what is actually happening, we can define two elemental equations as such:

$$ P_2 - P_2^\prime =R_2 Q\text{, and } $$

$$ P_2^\prime - P_0 =I \dot Q\text{.} $$
Solving these will give us a relationship for $\dot Q$, which is the elemental equation for the resistor and inertor (\eqn{ex_inert}).
\begin{equation}
P_2-\left( I\dot Q +P_0 \right) = P_{20}-I\dot Q = R_2Q \quad\Rightarrow\quad I\dot Q=P_{20}-R_2Q 
\label{eqn:ex_inert}
\end{equation}

So, we now have four equations which are mostly in terms of states and inputs, but we need to get rid of $q_1$ and $q_\text{out}$.  So let's define some elemental equations for the resistors that those flows pass through (Equations~\ref{eqn:ex_res1}~and~\ref{eqn:ex_res3}).

\begin{equation}
P_{10}-P_{20}=R_1q_1 \quad\Rightarrow\quad q_1=\frac{P_{10}}{R_1}-\frac{P_{20}}{R_1}
\label{eqn:ex_res1}
\end{equation}

\begin{equation}
P_{30}=R_3q_\text{out} \quad\Rightarrow\quad q_\text{out}=\frac{P_{30}}{R_3} 
\label{eqn:ex_res3}
\end{equation}

From these equations we can do some algebra and find the governing equations for the system, which are given as Equations~\ref{eqn:fluid_eom1}--\ref{eqn:fluid_eom4}.
\begin{equation}
\dot P_{10} = \frac{-1}{R_1C_1}P_{10}+\frac{1}{R_1C_1}P_{20}+\frac{1}{C_1}q_\text{in,1}
\label{eqn:fluid_eom1}
\end{equation}

\begin{equation}
\dot P_{20}=\frac{1}{R_1C_2}P_{10}-\frac{1}{R_1C_2}P_{20}-\frac{1}{C_2}Q
\label{eqn:fluid_eom2}
\end{equation}

\begin{equation}
\dot P_{30}=\frac{-1}{C_3R_3}P_{30}+\frac{1}{C_3}Q+\frac{1}{C_3}q_\text{in,2}
\label{eqn:fluid_eom3}
\end{equation}

\begin{equation}
\dot Q = \frac{1}{I}P_{20}-\frac{R_2}{I}Q
\label{eqn:fluid_eom4}
\end{equation}

These can easily be placed in state space form as such (taking $q_\text{out}$ to be the desired output):

$$\left\{\begin{array}{c} \dot P_{10} \\ \dot P_{20} \\ \dot P_{30} \\ \dot Q \end{array}\right\} = \left[ \begin{array}{cccc} \frac{-1}{R_1C_1} & \frac{1}{R_1C_1} & 0 & 0 \\ \frac{1}{R_1C_2} & \frac{-1}{R_1C_2} & 0 & \frac{-1}{C_2} \\ 0 & 0 & \frac{-1}{R_3C_3} &  \frac{1}{C_3} \\ 0 & \frac{1}{I} & 0 & \frac{-R_2}{I} \\ \end{array}\right] \left\{\begin{array}{c} P_{10} \\ P_{20} \\ P_{30} \\ Q \end{array}\right\} +\left[\begin{array}{cc} \frac{1}{C_1} & 0 \\ 0 & 0 \\ 0 & \frac{1}{C_3} \\ 0 & 0 \end{array}\right] \left\{\begin{array}{c} q_\text{in,1} \\ q_\text{in,2} \end{array}\right\} $$

$$q_\text{out} = \left[\begin{array}{cccc} 0 & 0 & \frac{1}{R_3} & 0 \end{array}\right]\left\{\begin{array}{c} P_{10} \\ P_{20} \\ P_{30} \\ Q \end{array}\right\} + \left[\begin{array}{cc} 0 & 0 \end{array}\right]\left\{\begin{array}{c} q_\text{in,1} \\ q_\text{in,2} \end{array}\right\}$$
