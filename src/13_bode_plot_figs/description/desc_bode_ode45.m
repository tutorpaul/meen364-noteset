function ydot=example_bode_ode45(t,y)

global omega c m k;

ydot=zeros(2,1);

ydot(1)=y(2);

ydot(2)=(-k*y(1)-c*y(2)+sin(omega*t))/m;

end