clc;
global omega k m c;

omega=2*pi;
m=1;
k=100;
zeta=.25;
c=2*zeta*sqrt(k/m)*m;

tspan=linspace(0,7,500);

Y0=[0 0]';

[t,y]=ode45('desc_bode_ode45',tspan,Y0);

u=.035*sin(omega*t);

plot(t,u,t,y(:,1));

fid= fopen('desc_bode.dat','wt');

fprintf(fid,'# time    input    output\n');
for i=1:length(t)
    fprintf(fid,'%12.4E   %12.4E   %12.4E\n',t(i),u(i),y(i));
end
fclose(fid);