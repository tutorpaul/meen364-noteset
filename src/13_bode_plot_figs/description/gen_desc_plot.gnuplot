set terminal fig
set output 'desc_plot.fig'
set ylabel 'Amplitude'
set xlabel 'Time (sec)'

plot "desc_bode.dat" using 1:2 title 'Input (scaled 0.35=1)' with lines 1,\
     "desc_bode.dat" using 1:3 title 'Output' with lines 3