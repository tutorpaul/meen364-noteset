K_0=tf(100^2,1000*50);

A=tf([1/10000 2/100 1],1);
B=tf(1,[1 0 0]);
C=tf(1,[1/50 6/50 1]);

tot=K_0*A*B*C;

%rlocus(tot)
[magT,phaseT,w]=bode(tot);

bode(tot);

% bode(C);
[magK,phaseK]=bode(K_0,w);
[magA,phaseA]=bode(A,w);
[magB,phaseB]=bode(B,w);
[magC,phaseC]=bode(C,w);

magK=squeeze(magK);
magA=squeeze(magA);
magB=squeeze(magB);
magC=squeeze(magC);
magT=squeeze(magT);

phaseK=squeeze(phaseK);
phaseA=squeeze(phaseA);
phaseB=squeeze(phaseB);
phaseC=squeeze(phaseC);
phaseT=squeeze(phaseT);

% fid= fopen('ex_bode.dat','wt');
% 
% fprintf(fid,'#    freq     gain(const)  phase(const)  gain(num)    phase(num)   gain(s^2)    phase(s^2)   gain(den)    phase(den)   gain(tot)   phase(tot)\n');
% for i=1:length(w)
%     fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E %12.4E\n',...
%         w(i),20*log10(magK(i)),phaseK(i),20*log10(magA(i)),phaseA(i),20*log10(magB(i)),phaseB(i),20*log10(magC(i)),phaseC(i),20*log10(magT(i)),phaseT(i));
% end
% fclose(fid);