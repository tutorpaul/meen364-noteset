set terminal fig

set size 0.465,0.5
set lmargin 6
set rmargin 1
set ylabel 'Phase (degree)'
set xlabel 'Frequency (rad/sec)'
set logscale x
set format x "10^%L"
set nokey
set yrange [-185:-175]

set output 'ex_plot_phase3.fig'
plot "ex_bode.dat" u 1:7 w lines 1

set yrange [-10:10]
set output 'ex_plot_phase1.fig'
plot "ex_bode.dat" u 1:3 w lines 1

set yrange [-5:185]
set output 'ex_plot_phase2.fig'
plot "ex_bode.dat" u 1:5 w lines 1

set yrange [-185:5]
set output 'ex_plot_phase4.fig'
plot "ex_bode.dat" u 1:9 w lines 1

set size 1,1

set yrange [-320:-180]
set output 'ex_plot_phasetot.fig'
plot "ex_bode.dat" u 1:11 w lines 1

set ylabel 'Mag. (dB)'
set format x ""
unset xlabel

set yrange [-250:50]
set output 'ex_plot_gaintot.fig'
plot "ex_bode.dat" u 1:10 w lines 1, "vertline.dat" u 1:2 w lines 1

set size 0.465,0.5

set yrange [-20:-10]
set output 'ex_plot_gain1.fig'
plot "ex_bode.dat" u 1:2 w lines 1

set yrange [-10:80]
set output 'ex_plot_gain2.fig'
plot "ex_bode.dat" u 1:4 w lines 1

set yrange [-200:50]
set output 'ex_plot_gain3.fig'
plot "ex_bode.dat" u 1:6 w lines 1

set yrange [-140:20]
set output 'ex_plot_gain4.fig'
plot "ex_bode.dat" u 1:8 w lines 1