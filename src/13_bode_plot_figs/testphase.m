omega=logspace(-1,4,100);

phase=180/pi*atan((194*omega.^5+50000*omega.^3)./...
   (-omega.^6+8850*omega.^4-500000*omega.^2));


semilogx(omega,phase);
