\section{Motivation}
Dealing with non-linear equations is tough, much tougher than linear equations. It is impossible to generate a transfer function for a non-linear equation, thankfully there is a method to approximate the linear equation of a non-linear function in a small neighborhood.  \fig{lin_approx} shows the idea of linearization graphically, by considering the slope of the non-linear region you wish to linearize it is possible to gain insight into the system you are exploring.  However, as you move further from the known point the accuracy of the approximation diminishes, thus it is vital to choose a meaningful known point (typically the equilibrium point of the system is most useful).  

\begin{figure}
\begin{center}
\includegraphics{09_linearization_figs/lin_approx}
\caption{A Non-Linear Function and its Linearized Approximation}
\label{fig:lin_approx}
\end{center}
\end{figure}

% Equilibrium points
\section{Finding Equilibrium Points}
An \newterm{equilibrium point} is defined as a position that a system will remain in indefinitely.  Likewise, a system with damping that is in motion will come to rest at an equilibrium point and remain there.  By definition a system at rest is not accelerating and has no velocity, thus to find the equilibrium point (or points) of a system you should set all the acceleration and velocity terms (terms with dots) to zero, and solve for the position terms (non-dottted terms).  

When there are more unknown terms than equations you will find that it is impossible to find a specific equilibrium position, thus you must choose an \newterm{operating point} by setting one or more of the variables and solving for the others.

% identifying non-linear terms
\section{Identifying Non-Linear Terms}
After finding the equilibrium points you will want to identify which terms are non-linear, this is not particularly necessary to the process of linearization but it is vital to know \emph{why} an equation is considered non-linear.  These are easy to pick out once you understand what to look for. All of the following are examples of non-linear terms:
\begin{itemize}
\item Sinusoidal terms ($\sin (x(t))$ or $\cos (x(t))$);
\item Squared or higher order terms ($x(t)^2$);
\item Two time varying terms multiplied by one another ($x(t)y(t)$); and
\item Exponential or Logarithm terms ($e^{x(t)}$ or $\ln|x(t)|$).
\end{itemize}  

% Linearizing non-linear terms
\section{Linearize}
To linearize, apply first order \newterm{Taylor series expansion} to each term in a non-linear equation.  But what is Taylor series expansion?  \eqn{taylor_series} shows the strict mathematical definition.
\begin{equation}
f(x(t),y(t)) \approx f(x_0,y_0)+ \frac{\partial f}{\partial x}(x_0,y_0) \Delta x(t) + \frac{\partial f}{\partial y}(x_0,y_0) \Delta y(t)
\label{eqn:taylor_series} 
\end{equation}

The idea behind the Taylor series is to estimate the value of a function by examining its derivatives.  Since we know the function's value at the equilibrium point we can get a reasonable guess of the nearby point's values using only the first derivative.  So, take a partial derivative of each term with respect to whatever independent variables are present in that term.  

We can speed up the linearization process by generalizing the Taylor series expansion of linear terms.  Let's look at a generic linear term, $\alpha q(t)$ for example, when we apply \eqn{taylor_series} we get $\alpha(q_0+\Delta q(t))$.  This relation is called a \newterm{perturbation equation} (\eqn{perturb_eqn}) and it is perfectly acceptable to substitute it into a linear term of a non-linear equation rather than explicitly performing Taylor series expansion.  Likewise you can substitute the time derivative of the perturbation equation place of higher order linear terms (Equations~\ref{eqn:d_perturb_eqn}~and~\ref{eqn:dd_perturb_eqn}, the $q_0$ drops out because it is a constant).

\begin{equation}
q(t)=q_0+\Delta q(t) 
\label{eqn:perturb_eqn}
\end{equation}
\begin{equation}
\dot q(t)=\Delta \dot q(t)  
\label{eqn:d_perturb_eqn}
\end{equation}
\begin{equation}
\ddot q(t)=\Delta \ddot q(t) 
\label{eqn:dd_perturb_eqn}
\end{equation}

% Example 
\section{Example}
Let's linearize Equations~\ref{eqn:example_prob1}~and~\ref{eqn:example_prob2}.

\begin{equation}
\ddot x(t)= x(t)^2+x(t)u(t)-\sin (x(t))+\dot x(t)y(t) +y(t)
\label{eqn:example_prob1}
\end{equation}
\begin{equation}
\ddot y(t)= x(t)+\dot y(t)+ u(t)^2
\label{eqn:example_prob2}
\end{equation}

First we'll plug in zeros for each of the dotted terms, and change the non-dotted terms to equilibrium notation ($x(t)\Rightarrow x_0$, etc.).
$$0=x_0^2+x_0u_0-\sin (x_0) +y_0 $$
$$0=x_0 +u_0^2 $$

Then solve for the equilibrium point.
$$y_0=-x_0^2 -x_0u_0 +\sin (x_0) $$
$$x_0=-u_0^2 $$

As you can see there is no way for us to find a unique equilibrium point, so we must choose an operating point---in this case I would choose $u_0=0$ which would make $x_0=0$ and $y_0=0$.  However it is most useful to linearize about a general point, $x_0$, $y_0$, and $u_0$.  So\dots\ that's what we'll do.  Now we'll find and linearize the non-linear terms using \eqn{taylor_series}, and with the linear terms, we'll do nothing (yet).
\begin{eqnarray*}
\ddot x(t) &=& x_0^2+2x_0\Delta x(t)+x_0u_0+u_0\Delta x(t)+x_0\Delta u(t) \\ 
& & -\sin (x_0) -\cos (x_0)\Delta x(t)+y_0\Delta \dot x(t) +y(t)
\end{eqnarray*}
$$\ddot y(t)=x(t) +\dot y(t) +u_0^2 +2u_0\Delta u(t)$$

We will quickly define the perturbation equations.
$$x(t)=x_0+\Delta x(t) $$
$$y(t)=y_0+\Delta y(t) $$
$$u(t)=u_0+\Delta u(t) $$

Then we substitute the perturbation equations (and their derivatives) into the linearized equations of motion to get the following.
\begin{eqnarray*}
\Delta \ddot x(t)&=&x_0^2+2x_0\Delta x(t)+x_0u_0+u_0\Delta x(t)+x_0\Delta u(t) \\
&& - \sin (x_0) -\cos (x_0)\Delta x(t)+y_0\Delta \dot x(t) +y_0+\Delta y(t) 
\end{eqnarray*}
$$\Delta \ddot y(t)=x_0+\Delta x(t) +\Delta \dot y(t) +u_0^2 +2u_0\Delta u(t)$$

If we consider the equilibrium equations we came up with earlier, we find that we can cancel all the non-time-varying terms in the previous equations.  These cancellations will \emph{always} be possible if your work is correct.  The linearized equations of motion are given as Equations~\ref{eqn:example_prob1_lin}~and~\ref{eqn:example_prob2_lin}.

\begin{equation}
\Delta \ddot x(t)=2x_0\Delta x(t)+u_0\Delta x(t)+x_0\Delta u(t) -\cos (x_0)\Delta x(t)+y_0\Delta \dot x(t) +\Delta y(t)
\label{eqn:example_prob1_lin}
\end{equation}
\begin{equation}
\Delta \ddot y(t)=\Delta x(t) +\Delta \dot y(t) +2u_0\Delta u(t)
\label{eqn:example_prob2_lin}
\end{equation}
