% Electric Motors (Generators).
\section{Electric Motors}
\newterms{Electric motors}{electric motor} either convert electrical energy to mechanical energy, or can be used as generators, which convert mechanical energy to electrical energy; thus motors are commonly referred to as \newterm{electric machine}\emph{s}.  Typically you will see rotary electric motors, however linear electric motors also exist.  The following sections describe both forms of electric machines and how they are modeled.

 % Linear 
\section{Linear Electric Machines}
You won't often see a linear electric motor, but there is a prevalent example---electric trains which are used in the Japanese subway system are essentially huge linear electric motors.  \fig{lin_motor} shows a simplified model of a linear electric motor. As you can see, there is a magnetic field going into the page (the crosses) and a conductive bar that is free to roll on a track.  You might recall from \course{Electricity and Optics} that a magnetic field exerts a force on charged particles in motion; the relation that defines this force is known as \newterm{Lorentz law}.  \eqn{lorentz}, which shows Lorentz law, uses $\vec{F}$ as the force vector, $\vec{I}$ as the vector current flowing through the bar, $l$ as the length of the bar, and $\vec{B}$ as the magnetic field vector. 

\begin{figure}[ht]
\begin{center}
\includegraphics{07_modeling_mixed_figs/lin_motor.pdf}
\caption{Simplified Linear Motor}
\label{fig:lin_motor}
\end{center}
\end{figure}

\begin{equation}
\vec{F}=l\vec{I} \times \vec{B}
\label{eqn:lorentz}
\end{equation}

If the bar has some velocity then there will be a measurable voltage difference through the bar, this resultant voltage is known as the \newterm{back emf}, or $V_\text{emf}$.  When operating as a generator the back emf, $V_\text{emf}$, is equal to the velocity of the bar, $v$, times the magnetic field intensity, $B$, and the length of the bar, $l$ (\eqn{generators}).

\begin{equation}
V_\text{emf}=Blv
\label{eqn:generators}
\end{equation}


 % Rotational
\section{Rotational Electric Machines}
You will likely need to model a rotary electric motor in the near future, so it is important that you are familiar with what is happening.  A motor has a whole bunch of wire inside it, which is wrapped up in coils.  This arrangement produces a magnetic field which spins a rotor.  These coils closely resemble an inductor, however to simplify the model the inductor is shown separate from the motor, as is done in \fig{rot_motor}.\footnote{If you are interested in the phenomena that make motors work you should look into taking \course{Mechatronic Systems, Dynamics and Modeling}}  Just like the linear motor, the mechanical force (motor torque) is proportional to the current running through the motor, this is embodied by \eqn{motor_torque}.  In \eqn{motor_torque}, $T$ represents the output torque of the motor, $I$ is the current flowing through the motor, and $k_t$ is the motor's torque coefficient.

\begin{figure}[ht]
\begin{center}
\includegraphics{07_modeling_mixed_figs/rot_motor.pdf}
\caption{Simplified Rotational Motor}
\label{fig:rot_motor}
\end{center}
\end{figure}

\begin{equation}
T=k_tI
\label{eqn:motor_torque}
\end{equation}

Also---like the linear machine---when the shaft is rotating there will be some resultant voltage difference between the terminals of the motor.  The equation that defines the back emf is shown in \eqn{back_emf}, where $V_\text{emf}$ is the back emf, $\dot \theta$ is the rotational velocity of the shaft, and $k_e$ is the motor's EMF constant.

\begin{equation}
V_\text{emf}=k_e\dot \theta
\label{eqn:back_emf}
\end{equation}

% Modeling Strategy.
\section{Modeling Strategy}
Let's examine the electro-mechanical system shown in \fig{rot_motor} taking the desired output to be the back emf of the motor.  We start the problem by defining our state variables as $\theta$, $\dot \theta$, and $I$ (the current through the inductor). Now we begin modeling the mechanical portion of the problem by drawing the free body diagram of the disk.  Once that's done we can state the equation of motion by applying Newton's second law.  The equation of motion for the disk is shown in \eqn{ex_disk_eom}.  

\begin{equation}
J\ddot \theta = T
\label{eqn:ex_disk_eom}
\end{equation}

To model the torque, $T$, on the disk we will use \eqn{motor_torque}, which, when substituted and re-arranged, will yield \eqn{ex_disk_eom_fin}.

\begin{equation}
\ddot \theta = \frac{k_t}{J}I
\label{eqn:ex_disk_eom_fin}
\end{equation}

Next, we will analyze the electrical portion of the system, which should begin with definitions of the elemental equations for each of the elements in the circuit.  For this example Equations~\ref{eqn:ex_resistor}~and~\ref{eqn:ex_inductor} represent the resistor and inductor (respectively), and \eqn{back_emf} is for the motor.  

\begin{equation}
V_R=RI
\label{eqn:ex_resistor}
\end{equation}

\begin{equation}
V_L=L\dot I
\label{eqn:ex_inductor}
\end{equation}
 
Now we can use Kirchoff's voltage law to find the governing equation for the circuit, as is done in \eqn{ex_kvl}.
\begin{equation}
V_\text{in}-V_R-V_L-V_\text{emf}=0
\label{eqn:ex_kvl}
\end{equation}

\eqn{ex_kvl} only becomes useful after substituting in the elemental equations (Equations~\ref{eqn:back_emf},~\ref{eqn:ex_resistor},~and~\ref{eqn:ex_inductor}).  Making the substitutions and re-arranging will give us \eqn{ex_kvl_fin}.

\begin{equation}
\dot I=\frac{-k_e}{L}\dot \theta -\frac{R}{L}I+\frac{1}{L}V_\text{in}
\label{eqn:ex_kvl_fin}
\end{equation}

We can now put the equations into state space form, don't forget the output equation.  The complete state space governing equations are shown as Equations~\ref{eqn:ex_state_input}~and~\ref{eqn:ex_state_output}.

\begin{equation}
\left\{ \begin{array}{c} \dot \theta \\ \ddot \theta \\ \dot I\\ \end{array} \right\} = \left[ \begin{array}{ccc} 0 & 1 & 0 \\ 0 & 0 & \nicefrac{k_t}{J} \\ 0 & \nicefrac{-k_e}{L} & \nicefrac{-R}{L} \\ \end{array} \right] \left\{ \begin{array}{c} \theta \\ \dot \theta \\ I\\ \end{array} \right\} + \left[ \begin{array}{c} 0 \\ 0 \\ \nicefrac{1}{L} \end{array} \right]V_\text{in}
\label{eqn:ex_state_input}
\end{equation}

\begin{equation}
y(t) = \left[ \begin{array}{ccc} 0 & k_e & 0 \\ \end{array} \right] \left\{ \begin{array}{c} \theta \\ \dot \theta \\ I\\ \end{array} \right\} + 0 V_\text{in}
\label{eqn:ex_state_output}
\end{equation}
