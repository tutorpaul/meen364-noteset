set terminal fig
set output 'fse_problem_soln.fig'
set size .5,.5
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Sample'
set xrange [0:5]
set yrange [-6:6]
set nokey
set xzeroaxis
plot 1.732*sin(3.14*x/3)-.577*sin(3.14*2*x/3)+.577*sin(3.14*4*x/3)-1.732*sin(3.14*5*x/3) w lines 1,\
     "fse_problem.dat" u 1:2 w points 5, "" notitle w impulses