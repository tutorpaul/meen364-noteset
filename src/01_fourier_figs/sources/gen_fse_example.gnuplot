set terminal fig
set output 'fse_example.fig'
set size 1,1
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Time'
set xrange [0:6.28]
plot "fse_example.dat" u 1:2 t "Exact" w lines 1,\
     "fse_example.dat" u 1:3 t "First Order" w lines 1,\
     "fse_example.dat" u 1:4 t "Third Order" w lines 1,\
     "fse_example.dat" u 1:5 t "Fifth Order" w lines 1,\
     "fse_example.dat" u 1:6 t "Tenth Order" w lines 1