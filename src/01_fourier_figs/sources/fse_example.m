N=800; %number of samples
order=10; %highest order
T=2*pi; %fundamental period

square=zeros(1,N);
square(1:floor(N/2))=1;
square(floor(N/2)+1:N)=-1;
t=linspace(0,T,N);



a=zeros(1,order);
approx=zeros(order,N);

for k=1:order
    temp=0;
    
    for n=1:N-1
        temp=temp+(square(n)*exp(-2*j*pi*k*n/N));
    end
    
    a(k)=temp/N;
    
end

approx(1,:)=real(a(1))*cos(t)-imag(a(1))*sin(t);
    


for k=2:order
    
    approx(k,:)=approx(k-1,:)+real(a(k))*cos(k*t)-imag(a(k))*sin(k*t);
    
    
end


plot(t,square,'b',t,2*approx(1,:),'b',t,2*approx(3,:),'b',t,2*approx(5,:),'b',t,2*approx(10,:),'b');
axis([0,T,-1.4,1.5]);

fid= fopen('fse_example.dat','wt');

fprintf(fid,'#  Time   Square_wave   approx1    approx3    approx 5   approx10 \n');
for i=1:length(t)
    fprintf(fid,'%12.4E %12.4E %12.4E %12.4E %12.4E %12.4E \n',...
        t(i),square(i),2*approx(1,i),2*approx(3,i),2*approx(5,i),2*approx(10,i));
end
fclose(fid);