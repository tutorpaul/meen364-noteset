fignum=1;
sampletime=.05;

x = [1 2 3 4 5 6]';
t = (0:sampletime:pi)';
a = [sin(t) sin(4*t) sin(9*t) sin(16*t) sin(25*t) sin(36*t)];
e = (-4+15*rand(length(a),1));

y = [sin(10*t), sin(20*t)+2*sin(40*t), a*x+e];

%y=sin(20*t);

power =cell(3,1);
freq =cell(3,1);

for i=1:size(y,2)
    subplot(3,2,i*2-1);
    plot(t,y(:,i));
    axis tight;

    fourierdata=fft(y(:,i));
    fourierdata(1)=[];


    n=length(fourierdata);
    power{i}=abs(fourierdata(1:floor(n/2))).^2;
    nyquist = (pi)/sampletime;
    freq{i}=(1:n/2)/(n/2)*nyquist;

    subplot(3,2,i*2);
    plot(freq{i},power{i})
    axis tight;
    xlabel('frequency');
    ylabel('power');
    title('Periodogram');

end

sampletime=.001;

t2 = (0:sampletime:pi)';
a2 = [sin(t2) sin(4*t2) sin(9*t2) sin(16*t2) sin(25*t2) sin(36*t2)];
e2 = (-4+15*rand(length(a2),1));
y2 = [sin(10*t2), sin(20*t2)+2*sin(40*t2), a2*x+e2];

fid= fopen('fft_example_signal.dat','wt');

fprintf(fid,'#   Time    singlesin  doublesin   6sin+noise  freq1  power1  freq2  power2  freq3  power3');
for i=1:length(t2)
    fprintf(fid,'%13.5E %13.5E %13.5E %13.5E \n',...
        t2(i),y2(i,1),y2(i,2),y2(i,3));
end
fclose(fid);

fid= fopen('fft_example_fft.dat','wt');

fprintf(fid,'# freq1  power1  freq2  power2  freq3  power3');
for i=1:length(freq{1})
    fprintf(fid,'%13.5E %13.5E %13.5E %13.5E %13.5E %13.5E \n',...
        freq{1}(i),power{1}(i),freq{2}(i),power{2}(i),freq{3}(i),power{3}(i));
end
fclose(fid);