set terminal fig
set output 'fse_problem.fig'
set size .5,.5
set lmargin 6
set rmargin 1
set ylabel 'Amplitude'
set xlabel 'Sample'
set xrange [0:9]
set yrange [-6:6]
set nokey
set xzeroaxis
plot "fse_problem.dat" u 1:2 w points 5, "" notitle w impulses